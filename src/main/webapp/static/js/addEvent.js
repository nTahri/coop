var selected_members = [];

$(document).ready(function() {
	
	// Workaround
	$("#members-wrapper").height($("#main-wrapper").height() + 200);
	
	
	// Build members list
	members = {};
	$("#members-items-root > tr").each(function() {
		var id = $(this).data('memberid');
		var member = {
			'id' : $(this).data('memberid'),
			'displayName' : $(this).data('displayname'),
			'firstName' : $(this).data('firstname'),
			'lastName' : $(this).data('lastname'),
			'status' : $(this).data('status'),
			'mail' : $(this).data('mail')
		};
		members[id] = member;
	});
	
	// Build initial participants list
	$("#participants-items-root > tr").each(function() {
		var id = $(this).data("memberid");
		addMember(members[id]);
		console.log(id);
	});
	
	
	$("#datepickerStart").datetimepicker({
      format: "yyyy-MM-dd hh:mm:ss",
      pickTime: true,
      startDate: new Date()
    }).on('changeDate', function(ev){
    	var testdate = new Date(ev.date).toISOString().replace('T',' ').split('.')[0];
    	$( "#endDate" ).val(testdate);
    	$( "#startDate" ).val(testdate);
//    	if (ev.date.valueOf() < date-start-display.valueOf()){	
//    	}
    });
    
	$("#datepickerEnd").datetimepicker({
      format: 'yyyy-MM-dd hh:mm:ss',
      pickTime: true,
      startDate: new Date()
    });

	
	$("#open-members").click(function() {
		openMembers();
	});
	$("#close-members").click(function() {
		closeMembers();
	});
	
	$("#members-items > tbody > tr").click(function() {
		var id = $(this).data('memberid');
		var member = members[id];

		addMember(member);

		// closeMembers();
	});
	
	$("input#member-filter").keyup(function() {
		filterMembers($(this).val().toLowerCase());
	});
});

function addMember(m) {
	if (selected_members.indexOf(m) >= 0) {
		
	} else {
		selected_members.push(m);
		renderMemberList();
	}
}

function removeMember(m) {
	var index = selected_members.indexOf(m);
	if (index > -1) {
		selected_members.splice(index, 1);
		renderMemberList();
	}

}

function openMembers() {
	$("#members-wrapper").show();
}
function closeMembers() {
	$("#members-wrapper").hide();
}

function renderMemberList() {
	$("#selected-members-root").empty();
	
	var val = [];
	selected_members.forEach(function(mem) {
		val.push(mem.id);
		
		$("#selected-members-root").append(
				"<tr id='" + mem.id + "'>" +
					"<td class='align-left'>" + mem.displayName + "</td>" +
					"<td class='align-right'>" + mem.mail + "</td>" +
					"<td class='event-member-remove' id='" 
					+ mem.id
					+ "'><img src='/laderaille/static/images/delete.png'></td>" +
				"</tr>");
		
	});

	$("select#members").val(val);
	
	$(".event-member-remove").click(function() {
		var id = $(this).attr("id");
		removeMember(members[id]);
	});
	
	
	filterMembersRefresh();
}

function filterMembersRefresh() {
	filterMembers($("input#member-filter").val());
}
function filterMembers(filterString) {
	for (var m_key in members) {
		var member = members[m_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		if (filterString.length > 0 && member.displayName.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		if (filterString.length > 0 && member.mail.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		if (selected_members.indexOf(member) >= 0) {
			show = false;
		}
		
		if (show) {
			$("tr#member-" + member.id).show();
		} else {
			$("tr#member-" + member.id).hide();
		}
	}
}