$(document).ready(function() {
	$("#delete-transactions").click(function(){
		var r = confirm("Voulez-vous vraiment supprimer toutes les transactions");
		if (r == true) {
			cleanUp("transactions");
		}
		
	});
	$("#delete-products").click(function(){
		var r = confirm("Voulez-vous vraiment supprimer tous les produits");
		if (r == true) {
			cleanUp("inventory");
		}
		
	});
	$("#delete-members").click(function(){
		var r = confirm("Voulez-vous vraiment supprimer tous les membres");
		if (r == true) {
			cleanUp("members");
		}
		
	});
});

function cleanUp(route){
	$.ajax({
		method : "GET",
		url : "/laderaille/clear/all/" + route,
		success : function(result) {
			
		},
		error : function(result) {
			
		}
	});
}