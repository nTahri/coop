var selected_category = null;
var selected_member = null;
var selected_event = null;
var filter_tags = [];

var selected_item = null;

var items = {};
var members = {};
var events = {};
var bill = {};

var TSQ;
var TVQ;

var umpaiedinvoices;
$(document)
		.ready(
				function() {
					umpaiedinvoices = JSON.parse(notPaiedinvoices);
					//alert(umpaiedinvoices.length);
					TSQ = $('#tax').data('tps');
					TVQ = $('#tax').data('tvq');

					// Prepare bill
					bill.subtotal = 0;
					bill.total = 0;
					bill.items = [];

					// Build item dictionary
					items = {};
					$("#inventory-items-root > tr").each(function() {
						var id = $(this).data('productcode');
						var item = {
							'id' : $(this).data('productcode'),
							'name' : $(this).data('name'),
							'price' : $(this).data('price'),
							'instock' : $(this).data('stock'),
							'category' : $(this).data('cat'),
							'taxcategory' : $(this).data('description'),
							'ref' : $(this).data('ref') + "",
							'minstock' : $(this).data('minstock') + ""
						};
						items[id] = item;
					});

					// Build members list
					members = {};
					$("#members-items-root > tr").each(function() {
						var id = $(this).data('memberid');
						var member = {
							'id' : $(this).data('memberid'),
							'displayName' : $(this).data('displayname'),
							'firstName' : $(this).data('firstname'),
							'lastName' : $(this).data('lastname'),
							'status' : $(this).data('status'),
							'mail' : $(this).data('mail'),
							'expiringDate' : $(this).data('d')
						};
						members[id] = member;
					});

					// Build events list
					events = {};
					$("#events-items-root > tr").each(function() {
						var id = $(this).data('eventid');
						var event = {
							'id' : $(this).data('eventid'),
							'name' : $(this).data('name'),
							'startDate' : $(this).data('startDate'),
							'endDate' : $(this).data('endDate'),
							'owner' : $(this).data('owner')
						};
						events[id] = event;
					});

					// OPEN + CLOSE CONTROLS
					// ====================================================================
					$("#open-members").click(function() {
						openMembers();
					});
					$("#close-members").click(function() {
						closeMembers();
					});
					$("#open-events").click(function() {
						openEvents();
					});
					$("#close-events").click(function() {
						closeEvents();
					});
					$("#bill-add-item").click(function() {
						openInventory();
					});
					$("#close-inventory").click(function() {
						closeInventory();
					});
					$("#close-info").click(function() {
						closeInfo();
					});


					// SELECTING MEMBER
					// ==========================================================================
					$("#members-items > tbody > tr").click(function() {
						var id = $(this).data('memberid');
						var member = members[id];

						selectMember(member);

						closeMembers();
					});

					// SELECTING EVENT
					// ==========================================================================
					$("#events-items > tbody > tr").click(function() {
						var id = $(this).data('eventid');
						var event = events[id];

						selectEvent(event);

						closeEvents();
					});

					// SELECTING CATEGORIES
					// ==========================================================================
					$(".category-select").click(function() {
						var cat = $(this).children("td").text();
						selectCategory(cat);
					});

					$(
							".filter-tag#category-filter-tag > .filter-tag-remove-btn")
							.click(function() {
								clearCategory();
							});

					// SELECTING ITEMS
					// ==========================================================================
					$("#inventory-items-root > tr").click(function() {
						var id = $(this).attr("id").replace("item-", "");
						// var taxcategory = $(this).data("description");
						
						// var minStock = $(this).attr("data-minStock");
						// items[taxcategory] = taxcategory;

						selectItem(items[id]);
					});
					$("#deselect-item").click(function() {
						clearSelectedItem();
					});

					// FILTERING
					// ==========================================================================
					$("input#inventory-filter").keyup(function() {
						filterItems($(this).val().toLowerCase());
					});
					$("input#member-filter").keyup(function() {
						filterMembers($(this).val().toLowerCase());
					});
					$("input#event-filter").keyup(function() {
						filterEvents($(this).val().toLowerCase());
					});

					// ADDING ITEMS TO BILL
					// ==========================================================================
					// rest l affichage du stock min atteind
					$("#inventory-add-button")
							.click(
									function() {
//										alert(selected_item.instock);
										var quantity = $(
												"input#inventory-add-quantity")
												.val();

										if (selected_item.ref.startsWith('M')) {
											addItem(selected_item,
													parseInt(quantity));
											closeInventory();
											buildInvoice();
										} else {
											// alert(selected_item.instock + ' ==== ' + quantity);
											if (selected_item.instock >= quantity) {
												addItem(selected_item,
														parseInt(quantity));
												closeInventory();
												buildInvoice();
												if ((selected_item.instock - parseInt(quantity)) <= selected_item.stockMin) {
													alert("Le stock min est atteint pour ce produit");
												}
											} 
											else {
												alert("La quantite existante en stock est "
														+ selected_item.instock);
											}
										}
									});

					// SAVING INVOICE
					// ==========================================================================
					$("#save-invoice").click(function() {
						$("#paiment-choose-wrapper").show();
					});
					$("#paiment-btn").click(function() {
						saveInvoice();
						$("#paiment-choose-wrapper").hide();
					});
					$("#close-paiment").click(function() {
						$("#paiment-choose-wrapper").hide();
					});

					// INITAL RENDER
					buildInvoice();
				});

function saveInvoice() {
	
	if (!selected_member) {
		alert("Le nom du client ne doit pas etre vide.");
	} else if (!selected_event){
		alert("L'evenement ne doit pas etre vide.");
	} else {
		
		var valid_discounts = true;
		bill.items.forEach(function(item) {
			if (item.discount < 0)
				valid_discounts = false;
			if (item.discountType == "$") {
				if (item.discount > (item.price * quantity))
					valid_discounts = false;
			} else if (item.discountType == "%") {
				if (item.discount > 100)
					valid_discounts = false;
			}
		});
		
		if (!valid_discounts) {
			alert("Un rabais ne peut pas etre negatif ou depasser 100% ou la valeure totale.");
		} else {
			bill['sellerName'] = $("div#username").text();
			bill['buyerName'] = selected_member.mail;
			bill['eventId'] = selected_event.id;
			bill['id'] = null;
			bill['items_str'] = JSON.stringify(bill.items);
			bill['invoiceStatus'] = $('input[name="paiment-mode"]:checked').val();
			bill['sendEmail'] = $("#destination-email").is(':checked');// $('#destination-email').val();
			bill['print'] = $("#destination-print").is(':checked');// $('#destination-print').val();
			var print = $("#destination-print").is(':checked');
			var emailIt = $("#destination-email").is(':checked');
			//alert("emailIt :" + emailIt);
			//console.log("emailIt :" + emailIt);

			bill[csrfParameter] = csrfToken;
			// req['invoice'] = billstr;

			$.ajax({
				method : "POST",
				url : "saveInvoice?show=" + print + "&emailIt=" + emailIt,
				data : bill,
				dataType : 'application/json;charset=utf8',
				success : function(result) {
					if(print === true){
						console.log(result);
						window.open(result, '_blank');
						
					}else{
						//window.open(result);
						
						console.log(result);
						window.location.href = result;
						
						return false;
					}
					window.location.replace("/laderaille/invoice/invoiceList");
				},
				error : function(result) {
					
					if(print){
						console.log(result);
						window.open(result["responseText"], '_blank');
						
					}else{
//						window.open(result["responseText"]);
						console.log(result);
						window.location.href = result["responseText"];
						
						return false;
					}
					window.location.replace("/laderaille/invoice/invoiceList");
					
				}
			});
		}
	}
}
function openInventory() {
	closeEvents();
	closeMembers();

	$("input#inventory-filter").val("");
	clearCategory();
	clearSelectedItem();

	$("#inventory-wrapper").show();
}
function openMembers() {
	closeInventory();
	closeEvents();

	$("#members-wrapper").show();
}
function openEvents() {
	closeInventory();
	closeMembers();

	$("#events-wrapper").show();
}

function closeInventory() {
	$("#inventory-wrapper").hide();
}
function closeMembers() {
	$("#members-wrapper").hide();
}
function closeEvents() {
	$("#events-wrapper").hide();
}
function closeInfo(){
	$("#info-wrapper").hide();
}

function selectCategory(cat) {
	if (cat == null) {
		clearCategory();
	} else {
		selected_category = cat;

		$("table#categories").hide();
		$("table#inventory-items").show();

		$("table#inventory-items td.category-column").hide();
		$(".filter-tag#category-filter-tag").show();
		$("#category-filter-tag-label").text("Categorie: " + selected_category);

		// $("table#inventory-items > tbody > tr").hide();
		// $("tbody > tr.category-" + selected_category).show();

		var className = $("#inventory-items tbody tr").map(function() {
			if ($(this).data('cat') !== selected_category) {
				$(this).hide();
			}
		});
	}
}
function clearCategory() {
	selected_category = null;

	$("table#inventory-items").hide();
	$("table#categories").show();

	$("table#inventory-items td.category-column").show();
	$(".filter-tag#category-filter-tag").hide();
	var className = $("#inventory-items tbody tr").map(function() {
		$(this).show();
	});
}

function selectItem(item) {
	if (item == null) {
		clearSelectedItem();
	} else {
		selected_item = item;
		// selected_item.taxcategory = taxcategory;
		// selected_item.stockMin = stockMin;
		// data-stockMin

		$("#tables-row").hide();
		$("#search-row").hide();
		$("#selected-item-row").show();

		$("#selected-item-name-label").text(selected_item.name);
		$("#selected-item-description").text(
				"Type de Taxes: " + selected_item.taxcategory
						+ " Restant en stock: " + selected_item.instock);
	}
}
function clearSelectedItem() {
	selected_item = null;
	selectCategory(selected_category);

	$("#tables-row").show();
	$("#search-row").show();
	$("#selected-item-row").hide();
}

function filterItems(filterString) {
	if (filterString == "") {
		// CLEAR FILTER
		selectCategory(selected_category);
		// filterItems($("input#inventory-filter").val().toLowerCase());
	} else {
		$("table#categories").hide();
		$("table#inventory-items").show();

		// items.forEach(function(item) {
		for ( var item_key in items) {
			var item = items[item_key];
			var show = false;
			if (filterString.length > 0
					&& item.name.toLowerCase().indexOf(filterString) >= 0) {
				show = true;
			}
			// if (item.description.toLowerCase().indexOf(filterString) >= 0) {
			// show = true;
			// }
			if (item.category.toLowerCase().indexOf(filterString) >= 0) {
				show = true;
			}

			if (selected_category != null && item.category != selected_category) {
				show = false;
			}

			if (show) {
				$("tr#item-" + item.id).show();
			} else {
				$("tr#item-" + item.id).hide();
			}
		}
		// });
	}
}
function filterMembers(filterString) {
	for ( var m_key in members) {
		var member = members[m_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		if (filterString.length > 0
				&& member.displayName.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		if (filterString.length > 0
				&& member.mail.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}

		if (show) {
			$("tr#member-" + member.id).show();
		} else {
			$("tr#member-" + member.id).hide();
		}
	}
}
function filterEvents(filterString) {
	for ( var e_key in events) {
		var event = events[e_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		if (filterString.length > 0
				&& event.name.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}

		if (show) {
			$("tr#event-" + event.id).show();
		} else {
			$("tr#event-" + event.id).hide();
		}
	}
}

function addItem(item, quantity) {
	// console.log("Adding");
	// alert(item.taxcategory === 'NON TAXABLE' ? 0.00 : TVQ*item.price);
	var entry = {
		productId : item.id,
		quantity : quantity,
		tsq : (item.taxcategory === 'NON TAXABLE' ? 0.00 : TSQ * item.price),
		tvq : (item.taxcategory === 'NON TAXABLE' ? 0.00 : TVQ * item.price),
		price : item.price,
		discount : 0,
		discountType : '%'
	};
	// console.log(entry);
	bill.items.push(entry);
}
function removeItem(index) {
	bill.items.splice(index, 1);

	buildInvoice();
}

function selectMember(member) {
	$("#selected-client-value").text(member.mail);
	
	var msg = "";
	
	var hasNonPaiedInvoice = jQuery.grep(umpaiedinvoices, function(value) {
	    return value.hasOwnProperty("buyerName");
	}).map(function(value) {
	    return value["buyerName"]; // Extract the values only
	}).includes(member.mail);
	if(hasNonPaiedInvoice){
		msg = "<p>Ce membre a au moins une facture non payee </p>";
	}
	//If there is an expiring date and this date is less than 10 days from now
	if(member.expiringDate && Math.round(((new Date(member.expiringDate))-(new Date()))/(1000*60*60*24)) <= 10){
		if(msg){
			msg += "<p> et son abonnement est presque expire, date d'expiration est le: " + member.expiringDate +"</p>";
		}else{
			msg = "<p>L'abonnement de ce membre est presque expire, date d'expiration est le: " + member.expiringDate +"</p>";
		}
	}
	if (msg){
		//alert(msg);
		$("#info-container").empty();
		$("#info-container").append(msg);
		$("#info-wrapper").show();
	}
	selected_member = member;
}
function selectEvent(event) {
	$("#selected-event-value").text(event.name);

	selected_event = event;
}

function buildInvoice() {
	$("#bill-items-root").empty();
	$("#discount-items-root").empty();

	var count = 0;
	bill.items
			.forEach(function(entry) {

				var entry_value = (entry.quantity * entry.price).toFixed(2);
				var entry_item = items[entry.productId];

				bill.subtotal += entry_item.price * entry.quantity;

				// Add a row to the items
				$("#bill-items-root")
						.append(
								"<tr class='item-row' id='item-"
										+ entry_item.id
										+ "'>"
										+ "<td class='align-left' style='width:500px;'>"
										+ "<input type='text' class='input-locked' value='"
										+ entry_item.name
										+ "' name='name' readonly />"
										+ "</td>"

										+ "<td class='align-right'>"
										+ "<input class='align-right input-locked' type='text' value='"
										+ entry.quantity
										+ "' name='qty' readonly />"
										+ "</td>"

										+ "<td class='align-right'>"
										+ "<input id='item-discount-"
										+ entry.productId
										+ "' class='align-right item-discount' type='number' value='"
										+ entry.discount
										+ "' name='discountValue' />"
										+ "</td>"

										+ "<td class='align-right'>"
										+ "<select class='item-discount-type' id='item-discount-type-"
										+ entry.productId
										+ "'><option value='%' selected>%</option><option value='$'>$</option></select>"
										+ "</td>"

										+ "<td class='align-right'>"
										+ "<input id='item-bill-price-"
										+ entry_item.id
										+ "' class='align-right input-locked item-value' type='text' value='"
										+ entry_value
										+ "' name='price' readonly />"
										+ "$</td>"

										+ "<td class='bill-item-delete' data-id='item-bill-price-"
										+ entry_item.id
										+ "' id='delete-item-"
										+ count
										+ "'><img src='/laderaille/static/images/delete.png'></td>"
										+ "</tr>");

				count += 1;
			});
	var bill_subtotal_str = bill.subtotal.toFixed(2);

	// Add back the "Add" button as the last row
	$("#bill-items-root")
			.append(
					"<tr id='bill-add-row'>"
							+ "<td class='align-center'><div id='bill-add-item'>Ajouter</div></td>"
							+ "<td></td>" + "<td></td>" + "</tr>");
	$("#bill-add-item").click(function() {
		openInventory();
	});

	// Add individual handlers
	$(".bill-item-delete").click(function() {
		var id = $(this).attr("id").replace("delete-item-", "");

		removeItem(id);
	});
	$(".item-discount").keyup(function() {
		refreshInvoice();
	});
	$(".item-discount").change(function() {
		refreshInvoice();
	});
	$(".item-discount-type").change(function() {
		refreshInvoice();
	});

	refreshInvoice();
}

function refreshInvoice() {
	// Go through each row
	// Update their discount to the input's values
	// Update their final price (but only the display)
	$("table#bill-items .item-row")
			.each(
					function(row_index, row) {
						// console.log("row", row);
						// console.log("index", row_index);

						var entry = bill.items[row_index];
						// console.log("entry", entry);
						if (entry) {
							// console.log("discount type",
							// $(this).find(".item-discount-type").val());
							// console.log($(this).find(".item-discount").val());

							entry.discount = $(this).find(".item-discount")
									.val();
							entry.discountType = $(this).find(
									".item-discount-type").val();

							// Change the displayed values for price
							var entry_value = entry.price * entry.quantity;
							if (entry.discountType == "%") {
								var percent_discount = entry.discount / 100.0
										* entry_value;
								var final_display_value = (entry_value - percent_discount);
								// console.log(entry_value, percent_discount,
								// final_display_value);
								$(this).find(".item-value").val(
										final_display_value.toFixed(2));
							} else if (entry.discountType == "$") {
								var fixed_discount = entry.discount;
								var final_display_value = (entry_value - fixed_discount);
								$(this).find(".item-value").val(
										final_display_value.toFixed(2));
							}
						}
					});

	// Update final values
	bill.subtotal = 0;
	var billTvq = 0;
	var billTsq = 0;
	bill.items.forEach(function(entry) {

		billTsq += entry.tsq;
		billTvq += entry.tvq;
		var entry_value = entry.quantity * entry.price;
		var entry_discount_value = 0;

		// Change the displayed values for price
		if (entry.discountType == "%") {
			var entry_value = entry.price * entry.quantity;
			var percent_discount = entry.discount / 100.0 * entry_value;

			entry_discount_value = percent_discount;
		} else if (entry.discountType == "$") {
			entry_discount_value = entry.discount;
		}

		bill.subtotal += entry_value - entry_discount_value;
	});

	var bill_subtotal_str = bill.subtotal.toFixed(2);

	var bill_tsq = billTsq;// bill.subtotal * TSQ;
	var bill_tsq_str = billTsq.toFixed(2);// bill_tsq.toFixed(2);
	var bill_tvq = billTvq;// bill.subtotal * TVQ;
	var bill_tvq_str = billTvq.toFixed(2);// bill_tvq.toFixed(2);

	bill.total = bill.subtotal + bill_tsq + bill_tvq;
	var bill_total_str = bill.total.toFixed(2);

	$("#bill-subtotal-value").text(bill_subtotal_str + "$");
	$("#bill-sales-tax-value").text(bill_tsq_str + "$");
	$("#bill-tax-value").text(bill_tvq_str + "$");
	$("#bill-total-value").text(bill_total_str + "$");
}
