var selected_member = null;
var members = {};

$(document).ready(function() {
	// Build members list
	members = {};
	$("#members-items-root > tr").each(function() {
		var id = $(this).data('memberid');
		var member = {
			'id' : $(this).data('memberid'),
			'displayName' : $(this).data('displayname'),
			'firstName' : $(this).data('firstname'),
			'lastName' : $(this).data('lastname'),
			'status' : $(this).data('status'),
			'mail' : $(this).data('mail'),
			'expiringDate' : $(this).data('d')
		};
		members[id] = member;
	});
	
	$("#members-items > tbody > tr").click(function() {
		var id = $(this).data('memberid');
		var member = members[id];

		selectMember(member);

		closeMembers();
	});
	
	// OPEN + CLOSE CONTROLS
	// ====================================================================
	$("#open-members").click(function() {
		openMembers();
	});
	$("#close-members").click(function() {
		closeMembers();
	});
});

function selectMember(member) {
	$("#selected-client-value").text(member.mail);
	selected_member = member;
}

function openMembers() {
	$("#members-wrapper").show();
}
function closeMembers() {
	$("#members-wrapper").hide();
}

function sendMyMail() {
	var memberMail = document.getElementById("selected-client-value");
	document.getElementById("memberMyMail").value = memberMail.innerHTML;		 
}
function validateWorkHoursForm() {
	var memberMail = document.getElementById("selected-client-value");
	var startDate = document.getElementById("startingDate");
	var endDate = document.getElementById("endingDate");
	var errorMessage = ""; 
	if(memberMail.innerHTML == "")
	{				
        errorMessage += "Vous devez selectionner un membre.\n";		   
	}					
	if(startDate.value==""||endDate.value==""){
		errorMessage += "Vous devez entrez une periode de recherche.\n";
    }
	if (errorMessage != "") {
		alert(errorMessage);
	}	
	else {
		document.getElementById("submitB").click();
	}
}