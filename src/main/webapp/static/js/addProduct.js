$(document).ready(function() {
	$("#productSellPrice").keyup(function() {
		var sell_val = $(this).val();
		var buy_val = $("#productPrice").val();
		
		$("#productPrice").attr("min", sell_val);
		$("#productPrice").val(Math.max(sell_val, buy_val));
	});
	
	$("#productSellPrice").change(function() {
		var sell_val = $(this).val();
		var buy_val = $("#productPrice").val();
		
		$("#productPrice").attr("min", sell_val);
		$("#productPrice").val(Math.max(sell_val, buy_val));
	});
	
	$("#productSellPrice").click(function() {
		var sell_val = $(this).val();
		var buy_val = $("#productPrice").val();
		
		$("#productPrice").attr("min", sell_val);
		$("#productPrice").val(Math.max(sell_val, buy_val));
	});
	
	$("#productSellPrice").submit(function() {
		var sell_val = $(this).val();
		var buy_val = $("#productPrice").val();
		
		$("#productPrice").attr("min", sell_val);
		$("#productPrice").val(Math.max(sell_val, buy_val));
	});

});