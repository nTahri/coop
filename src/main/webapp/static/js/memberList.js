$(document).ready(function() {
	// Build members list
	members = {};
	$("#members-items-root > tr").each(function() {
		var id = $(this).data('memberid');
		var member = {
			'id' : $(this).data('memberid'),
			'displayName' : $(this).data('displayname'),
			'firstName' : $(this).data('firstname'),
			'lastName' : $(this).data('lastname'),
			'status' : $(this).data('status'),
			'mail' : $(this).data('mail')
		};
		members[id] = member;
	});
	
	$("input#filter").keyup(function() {
		filterMembers($(this).val().toLowerCase());
	});
});

function filterMembers(filterString) {
	for (var m_key in members) {
		var member = members[m_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		if (filterString.length > 0 && member.displayName.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		if (filterString.length > 0 && member.mail.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		
		if (show) {
			$("tr#member-" + member.id).show();
		} else {
			$("tr#member-" + member.id).hide();
		}
	}
}