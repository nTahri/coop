var selected_member = null;
var members = {};

$(document).ready(function() {
	// Build members list
	members = {};
	$("#members-items-root > tr").each(function() {
		var id = $(this).data('memberid');
		var member = {
			'id' : $(this).data('memberid'),
			'displayName' : $(this).data('displayname'),
			'firstName' : $(this).data('firstname'),
			'lastName' : $(this).data('lastname'),
			'mail' : $(this).data('mail'),
			'expiringDate' : $(this).data('d')
		};
		members[id] = member;
	});
	
	$("#members-items > tbody > tr").click(function() {
		var id = $(this).data('memberid');
		var member = members[id];

		selectMember(member);

		closeMembers();
	});
	
	// OPEN + CLOSE CONTROLS
	// ====================================================================
	$("#open-members").click(function() {
		openMembers();
	});
	$("#close-members").click(function() {
		closeMembers();
	});
});

function selectMember(member) {
	$("#selected-client-value").text(member.mail);
	selected_member = member;
}

function openMembers() {
	$("#members-wrapper").show();
}
function closeMembers() {
	$("#members-wrapper").hide();
}

function change(obj) {		
    var button = document.getElementById("getAllEventMember");
    button.click();
}

function sendMail() {
	document.getElementById("myCheck").checked = true;
	var memberMail = document.getElementById("selected-client-value");
	document.getElementById("memberMail").value = memberMail.innerHTML;		 
}

function changeFunctionBenevole(obj) {
	document.getElementById("benevoleWorkingTimes").style.visibility = 'visible';
	document.getElementById("submitB").style.visibility = 'visible';
	document.getElementById("submitM").style.visibility = 'hidden';	
	document.getElementById("memberWorkingTimes").style.visibility = 'hidden';		
}

function changeFunctionMember(obj) {
	document.getElementById("memberWorkingTimes").style.visibility = 'visible';
	document.getElementById("submitM").style.visibility = 'visible';			
	document.getElementById("benevoleWorkingTimes").style.visibility = 'hidden';
	document.getElementById("submitB").style.visibility = 'hidden';	
			
}	

function validationFunction(){
	document.getElementById("memberWorkingTimes").style.visibility = 'visible';
	document.getElementById("submitM").style.visibility = 'visible';			
	document.getElementById("benevoleWorkingTimes").style.visibility = 'hidden';
	document.getElementById("submitB").style.visibility = 'hidden';					
}	

function validateForm() {
	var memberMail = document.getElementById("selected-client-value");
	var event = document.getElementById("selectedMyEvent");
	var startDate = document.getElementById("startDate");
	var endDate = document.getElementById("endDate");
	var check = document.getElementById("myCheckMember").checked;
	var simpmembre = document.getElementById("simpmembre").checked;
	var benevole = document.getElementById("benevole").checked;

	var errorMessage = ""; 
	if(memberMail.innerHTML == "")
	{				
        errorMessage += "Vous devez selectionner un membre.\n";		   
	}
	if(event.value == "NONE")
	{			
		var getValue = document.getElementById("optionSecledValue");			
		if(getValue != null && getValue.label!="")
		{
			selectedMyEvent[selectedMyEvent.selectedIndex].value = getValue.label;
		}else{
			errorMessage += "Vous devez selectionner un evenement.\n";	
		}	        	   
	}		
	if (simpmembre&&!check) {
		
        errorMessage += 'Vous devez cocher la case "Validez la presence du membre."\n';
    }

	if (benevole) {
		if(startDate.value==""||endDate.value=="")
			errorMessage += "Vous devez entrez les heures de presence.\n";
    }		

	if (errorMessage != "") {
		alert(errorMessage);
	}
	else{
		if (benevole){
			document.getElementById("submitBB").click();
		}
		if (simpmembre){
			document.getElementById("submitMM").click();
		}			
	}		
}