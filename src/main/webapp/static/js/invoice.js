$(document).ready(function() {
	
	invoices = {};
	$("#invoicesList-items-root > tr").each(function() {
		var id = $(this).data('id');
		var invoice = {
			'id' : $(this).data('id'),
			'buyer' : $(this).data('buyer'),
			'seller' : $(this).data('seller'),
			'trasactionDate' : $(this).data('trasactionDate'),
			'status' : $(this).data('status'),
			'total' : $(this).data('total')
		};
		invoices[id] = invoice;
	});
	
	// FILTERING
	$("input#invoice-filter").keyup(function() {
		filterItems($(this).val().toLowerCase());
	});
	$("#search-select").change(function(){
		filterItems($(this).val());
	});
});

function filterItems(filterString) {
	//alert(invoices[9].status);
	for (var e_key in invoices) {
		
		var invoice = invoices[e_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		
		if (filterString.length > 0) {
			if(filterString == 1){
				if(invoice.status == 1 || invoice.status == 3 || invoice.status == 4){
					show = true;
				}
			}else{
				if(invoice.status == filterString){
					show = true;
				}
			}
			
			
		}
		
		if (show) {
			$("tr#invoice-" + invoice.id).show();
		} else {
			$("tr#invoice-" + invoice.id).hide();
		}
	}
}