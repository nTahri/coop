$(document).ready(function() {
	// Build members list
	
	$("input#filter").keyup(function() {
		filterCategories($(this).val().toLowerCase());
	});
});

function filterCategories(filterString) {
	var trs = $("#categories-root > tr");
	$("#categories-root > tr").each(function(index) {

		var cat_name = $(this).find("td").text();
		var show = false;
		
		if (cat_name.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		
		if (filterString.length == 0) {
			show = true;
		}
		
		if (show) {
			$(this).show();
		} else {
			$(this).hide();
		}
	});
}