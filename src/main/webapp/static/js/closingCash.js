function unitTypeFunction(unitType) {	
	    	var cell = document.getElementById(unitType);
	    	if(cell.value =="")	
		cell.value = 0;	    	
}

function removeZeroFunction(unitType) {			
	var cell = document.getElementById(unitType);	
	if(cell.value =="0")	
		cell.value = "";
}	

function functionTotaldollars() {	
	var cellTotal = document.getElementById("somme");			
	var fiveCent= document.getElementById("fiveCent");
	var tenCent= document.getElementById("tenCent");
	var twentyFiveCent= document.getElementById("twentyFiveCent");
	var oneDollars= document.getElementById("oneDollars");
	var twoDollars= document.getElementById("twoDollars");
	var fiveDollars= document.getElementById("fiveDollars");
	var tenDollars= document.getElementById("tenDollars");
	var twentyDollars= document.getElementById("twentyDollars");
	var fiftyDollars= document.getElementById("fiftyDollars");
	var hundredDollars= document.getElementById("hundredDollars");	
	var result = 0;
	result = fiveCent.value*0.05+tenCent.value*0.1+twentyFiveCent.value*0.25+
	oneDollars.value*1+twoDollars.value*2+fiveDollars.value*5+tenDollars.value*10+
	twentyDollars.value*20+fiftyDollars.value*50+hundredDollars.value*100;	
	
    cellTotal.value = result;
}      

function confirmationFunction(lastTotal){
	var cellTotal = document.getElementById("somme");	
	var difference = parseFloat(cellTotal.value) - parseFloat(lastTotal);
	
	alreadySaveBlock:
	if(parseFloat(cellTotal.value)!=0){
		if((difference < -5) || (difference > 5))
		{
			if((document.getElementById("myCheck").checked == false))
			{
				document.getElementById("myCheck").checked = true;
				alert("Votre d\351copmte comporte des erreurs. Comptez de nouveau");	
				break alreadySaveBlock;
			}
			
			if((document.getElementById("myCheck").checked == true))
			{	
				var confirmation = confirm("D\351sirez vous vraiment changer le dernier d\351compte de caisse ?");				    
			    if (confirmation) {
			    	var justification = prompt("Ajoutez un commentaire pour justifier cette diff\351rence.");
			    	if (justification != "") {
			            document.getElementById("openComments").value = justification;
			            document.getElementById("openDifference").value = difference;
			            document.getElementById("myCheck").checked = false;
				    	document.getElementById("updateCash").click();				            
			            break alreadySaveBlock;
			        }else{
		        		alert("Vous ne pouvez sauvegarder ce nouveau d\351compte sans justification.");
		        		break alreadySaveBlock;
		        	} 
			    } else {
				    document.getElementById("myCheck").checked = false;
				    break alreadySaveBlock;
			    }
				
			}				
		} 
		else if ((difference >= -5) && (difference <= 5))
		{
			alert("Votre d\351copmte est correct. Merci!");
			break alreadySaveBlock;
		}	
	}else{
		alert("Votre devez d'abord compter.");
	}
}