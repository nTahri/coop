$(document).ready(function() {
	// Build events list
	events = {};
	$("#events-items-root > tr").each(function() {
		var id = $(this).data('eventid');
		var event = {
			'id' : $(this).data('eventid'),
			'name' : $(this).data('name'),
			'startDate' : $(this).data('startDate'),
			'endDate' : $(this).data('endDate'),
			'owner' : $(this).data('owner')
		};
		events[id] = event;
	});
	
	$("input#filter").keyup(function() {
		filterEvents($(this).val().toLowerCase());
	});
});

function filterEvents(filterString) {
	for (var e_key in events) {
		var event = events[e_key];
		var show = false;
		if (filterString.length == 0) {
			show = true;
		}
		if (filterString.length > 0 && event.name.toLowerCase().indexOf(filterString) >= 0) {
			show = true;
		}
		
		if (show) {
			$("tr#event-" + event.id).show();
		} else {
			$("tr#event-" + event.id).hide();
		}
	}
}