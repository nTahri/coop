function unitTypeFunction(unitType) {	
	var cell = document.getElementById(unitType);
	if(cell.value =="")	
		cell.value = 0;	    	
}

function removeZeroFunction(unitType) {			
	var cell = document.getElementById(unitType);	
	if(cell.value =="0")	
		cell.value = "";
}	

function functionTotaldollars() {	
	var cellTotal = document.getElementById("somme");			
	var fiveCent= document.getElementById("fiveCent");
	var tenCent= document.getElementById("tenCent");
	var twentyFiveCent= document.getElementById("twentyFiveCent");
	var oneDollars= document.getElementById("oneDollars");
	var twoDollars= document.getElementById("twoDollars");
	var fiveDollars= document.getElementById("fiveDollars");
	var tenDollars= document.getElementById("tenDollars");
	var twentyDollars= document.getElementById("twentyDollars");
	var fiftyDollars= document.getElementById("fiftyDollars");
	var hundredDollars= document.getElementById("hundredDollars");	
	var result = 0;
	result = fiveCent.value*0.05+tenCent.value*0.1+twentyFiveCent.value*0.25+
	oneDollars.value*1+twoDollars.value*2+fiveDollars.value*5+tenDollars.value*10+
	twentyDollars.value*20+fiftyDollars.value*50+hundredDollars.value*100;	
	
    cellTotal.value = result;
}      

function confirmationFunction(totalExpected, closingCashExist){
	var cellTotal = document.getElementById("somme");	
	var difference = parseFloat(cellTotal.value) - parseFloat(totalExpected);
	alreadySaveBlock:
	if(closingCashExist === 'false'){
		
		//No value in the table.
		if((parseFloat(cellTotal.value)==0))
		{
			alert("Votre devez d'abord compter.");
			break alreadySaveBlock;
		}
		
		newSaveBlock:
		if((difference < -5) || (difference > 5))
		{	
			
			if((document.getElementById("myCheck").checked == false))
			{
				document.getElementById("myCheck").checked = true;
				alert("Votre d\351copmte comporte des erreurs. Comptez de nouveau");
				break alreadySaveBlock;
			}
			if((document.getElementById("myCheck").checked == true))
			{				
				var confirmation = confirm("Votre d\351compte comporte des erreurs. Souhaitez-vous tout de m\352me l'enregistrer?");				    
			    if (confirmation) {	
			    	var justification = prompt("Ajoutez un commentaire pour justifier cette diff\351rence. " +
			    			"Le d\351compte enregist\351 n'est pas modifiable. ");
			    	if (justification != "") {
			            document.getElementById("commentaire").value = justification;
			            document.getElementById("closingDifference").value = difference;			            
			            document.getElementById("myCheck").checked = false;
			            document.getElementById("saveCash").click();	
			            break alreadySaveBlock;
			        }else{
		        		alert("Vous ne pouvez sauvegarder ce nouveau d\351compte sans justification.");
		        		break alreadySaveBlock;
		        	}   	
			    	
			    } else {
		    	
			    	document.getElementById("somme").value=0;			
					document.getElementById("fiveCent").value=0;
					document.getElementById("tenCent").value=0;
					document.getElementById("twentyFiveCent").value=0;
					document.getElementById("oneDollars").value=0;
					document.getElementById("twoDollars").value=0;
					document.getElementById("fiveDollars").value=0;
					document.getElementById("tenDollars").value=0;
					document.getElementById("twentyDollars").value=0;
					document.getElementById("fiftyDollars").value=0;
					document.getElementById("hundredDollars").value=0;	
	
					 var tr=document.getElementById('sum_table').rows;
					 var td = null;
				     for (var i = 1; i < tr.length-3; i++) {
				          td = tr[i].cells;
				          	td[2].innerHTML = 0;								       
				     }
				     document.getElementById("myCheck").checked = false;
				     break alreadySaveBlock;
			    }		
			}					
		} 
		
		if ((difference >= -5) && (difference <= 5))
		{
			var r = confirm("Votre d\351compte est correct et est sur le point d'\352tre sauvegard\351. " +
					"La caisse sera ferm\351e et un d\351compte enregist\351 n'est pas modifiable. ");
			if (r == true) {
				document.getElementById("saveCash").click();			
	    		document.getElementById("myCheck").checked = false;
	    		break alreadySaveBlock;
			} else {
				document.getElementById("myCheck").checked = false;
	    		break alreadySaveBlock;
			}
    		
		}	
	}else{
			alert("Le d\351compte d'aujourd'hui a d\351j\340 \351t\351 enregistr\351. Vous ne pouvez le modifier.");			
 	}
}

function unitFunction(unite, numLine, factor) {	
	var x = document.getElementById("sum_table").rows[numLine].cells;	
	var decompte = document.getElementById(unite);	    	
	var result = decompte.value*factor;	
	if (!isNaN(result)) {
		x[2].innerHTML = result;
	}    					
}		