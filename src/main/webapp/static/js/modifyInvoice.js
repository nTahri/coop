$(document).ready(function() {
	$("#pay-btn").click(function(){
		payment();
	});		
	$("#refound-btn").click(function(){
		refound();
	});
});

function payment(){
	var id  = $("#pay-btn").data('id');
	$.ajax({
		method : "GET",
		url : "payInvoice-" + id,
		success : function(result) {
//			alert("success pay" + result);
			window.open(result, '_blank');
			window.location.replace("/laderaille/invoice/invoiceList");
		},
		error : function(result) {
//			alert("error pay" + JSON.stringify(result["responseText"]));
			window.open(result["responseText"], '_blank');
			window.location.replace("/laderaille/invoice/invoiceList");
		}
	});
}
function refound(){
	var id  = $("#refound-btn").data('id');
//	alert(id);
	$.ajax({
		method : "GET",
		url : "refundInvoice-" + id,
		success : function(result) {
//			alert("success refound" + result);
			window.open(result, '_blank');
			window.location.replace("/laderaille/invoice/invoiceList");
		},
		error : function(result) {
//			alert("e2rror refound" + JSON.stringify(result["responseText"]));
			window.open(result["responseText"], '_blank');
			window.location.replace("/laderaille/invoice/invoiceList");
		}
	});
}