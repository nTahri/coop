var selected_category = null;
var filter_tags = [];

var items = [];


$(document).ready(function() {
	
	// Build item dictionary
	items = {};
	$("#inventory-items-root > tr").each(function() {
		var id = $(this).data('productcode');
		var item = {
			'id' : $(this).data('productcode'),
			'name' : $(this).data('name'),
			'price' : $(this).data('price'),
			'instock' : $(this).data('stock'),
			'category' : $(this).data('cat'),
			'taxcategory' : $(this).data('taxcategory')
		};
		items[id] = item;
	});
	

	// SELECTING CATEGORIES
	$(".category-select").click(function() {
		var cat = $(this).children("td").text();
		selectCategory(cat);
	});

	$(".filter-tag#category-filter-tag > .filter-tag-remove-btn").click(function() {
		clearCategory();
	});
	
	
	
	// FILTERING
	$("input#inventory-filter").keyup(function() {
		filterItems($(this).val().toLowerCase());
	});
	
});

function selectCategory(cat) {
	if (cat == null) {
		clearCategory();
	} else {
		selected_category = cat;

		$("table#categories").hide();
		$("table#inventory-items").show();

		$("table#inventory-items td.category-column").hide();
		$(".filter-tag#category-filter-tag").show();
		$("#category-filter-tag-label").text("Categorie: " + selected_category);

		// $("table#inventory-items > tbody > tr").hide();
		// $("tbody > tr.category-" + selected_category).show();

		var className = $("#inventory-items tbody tr").map(function() {
			if ($(this).data('cat') !== selected_category) {
				$(this).hide();
			}
		});
	}
}
function clearCategory() {
	selected_category = null;

	$("table#inventory-items").hide();
	$("table#categories").show();

	$("table#inventory-items td.category-column").show();
	$(".filter-tag#category-filter-tag").hide();
	var className = $("#inventory-items tbody tr").map(function() {
		$(this).show();
	});
}

function filterItems(filterString) {
	if (filterString == "") {
		// CLEAR FILTER
		selectCategory(selected_category);
		// filterItems($("input#inventory-filter").val().toLowerCase());
	} else {
		$("table#categories").hide();
		$("table#inventory-items").show();

		// items.forEach(function(item) {
		for (var item_key in items) {
			var item = items[item_key];
			var show = false;
			if (filterString.length > 0 && item.name.toLowerCase().indexOf(filterString) >= 0) {
				show = true;
			}
			if (filterString.length == 0) {
			 	show = true;
			}
			if (item.category.toLowerCase().indexOf(filterString) >= 0) {
				show = true;
			}

			if (selected_category != null && item.category != selected_category) {
				show = false;
			}

			if (show) {
				$("tr#item-" + item.id).show();
			} else {
				$("tr#item-" + item.id).hide();
			}
		}
		//});
	}
}

