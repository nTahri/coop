<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>La Deraille - Connection</title>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>
	<link href="<c:url value='/static/css/laderaille.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/login.css' />" rel="stylesheet"></link>
	
	<title>Connection</title>
</head>
<body>
	<div id="login-wrapper">
		<div id="login-logo">
			<img src="<c:url value='/static/images/logo_black.png' />">
		</div>
		<br>
<!-- 		<div id="login-label">
			La Deraille
		</div> -->
		<div id="login-info">
			<c:url var="loginUrl" value="/login" />
			<form action="${loginUrl}" method="post">
			<input id="username" type="text" name="mail" placeholder="Nom d'utilisateur" required><br>
			<input id="password" type="password" name="password" placeholder="Mot de passe" ><br>
			<input id="rememberme" type="checkbox" id="rememberme" name="remember-me">Rester Connecte</br>
			<input id="login-button" type="submit" value="Se Connecter">
			<input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
			</form>
		</div>
	</div>
</body>
</html>