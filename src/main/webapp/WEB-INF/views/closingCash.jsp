<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "events";</script>
	<title>Ouverture de la caisse</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/closingCash.js' />"></script>	
	
</head>
<body>	
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">	
	<h1>Ouverture de la caisse</h1>
	<form:form method="POST" commandName="closingCash">	
	<form:input id="openComments" path="openComments" style="display: none;"/>	
	<form:input id="openDifference" path="openDifference" style="display: none;"/>		
		<div>
			<div class="label">
				Date du dernier decompte de caisse
			</div>
			<input type="date" value="${yesterday}" readonly="true" /><br>
		</div>
		
		<div>
			<div class="label">
				Membre ayant ferme la caisse
			</div>
			<input type="mail" value="${lastMemberclosingCash}" path="member.mail" readonly="true" /><br>
		</div>
		<p>	<h3 style="color:#FF0000;">${errorAnonymousMessage}</h3></p>						
		<form:input path="closingDate" type="hidden"/>		
		<form:input path="id" type="hidden"/>	
		<input type="checkbox" id="myCheck" style="display: none;"/>
		<table id="sum_table" style="float: left;">
			<thead>
				<tr>
					<th class="align-left">Unite</th>
					<th class="align-left">Decompte</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="align-left">5�</td>
					<td class="align-left">
						<form:input id="fiveCent" onclick='removeZeroFunction("fiveCent")' 
						onfocusout='unitTypeFunction("fiveCent")'
						onkeyup='functionTotaldollars()' path="fiveCent"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">10�</td>
					<td class="align-left">
						<form:input id="tenCent" onclick='removeZeroFunction("tenCent")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("tenCent")' path="tenCent"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">25�</td>
					<td class="align-left">
						<form:input id="twentyFiveCent" onclick='removeZeroFunction("twentyFiveCent")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("twentyFiveCent")' path="twentyFiveCent"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">1$</td>
					<td class="align-left">
						<form:input id="oneDollars" onclick='removeZeroFunction("oneDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("oneDollars")' path="oneDollars"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">2$</td>
					<td class="align-left">
						<form:input id="twoDollars" onclick='removeZeroFunction("twoDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("twoDollars")' path="twoDollars"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">5$</td>
					<td class="align-left">
						<form:input id="fiveDollars" onclick='removeZeroFunction("fiveDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("fiveDollars")' path="fiveDollars"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">10$</td>
					<td class="align-left">
						<form:input id="tenDollars" onclick='removeZeroFunction("tenDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("tenDollars")' path="tenDollars"/>
					</td>	
				</tr>
				<tr>
					<td class="align-left">20$</td>
					<td class="align-left">
						<form:input id="twentyDollars" onclick='removeZeroFunction("twentyDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("twentyDollars")' path="twentyDollars"/>
					</td>	
				</tr>
				<tr>
					<td class="align-left">50$</td>
					<td class="align-left">
						<form:input id="fiftyDollars" onclick='removeZeroFunction("fiftyDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("fiftyDollars")' path="fiftyDollars"/>
					</td>
				</tr>
				<tr>
					<td class="align-left">100$</td>
					<td class="align-left">
						<form:input id="hundredDollars" onclick='removeZeroFunction("hundredDollars")' 
						onkeyup='functionTotaldollars()'
						onfocusout='unitTypeFunction("hundredDollars")' path="hundredDollars"/>
					</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="align-left"><b>Montant Total</b></td>
					<td class="align-left"><form:input id="somme" path="total" readonly="true"/>$</td>
				</tr>
				<tr>				
					<td>
						<input class="controls-btn" type="button" value="Confirmer le decompte" onclick="confirmationFunction('${lastTotal}')" />
					</td>
					<td style="display: none;">
						<input class="controls-btn" id="updateCash" type="submit" name="update"/>
					</td>							
				</tr>
			</tfoot>				
		</table>			
	</form:form>
	
</div>
</body>
</html>