<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "admin";</script>
	<title>Rapport de Fermeture de Caisse</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>	
	<script src="<c:url value='/static/js/login.js' />"></script>		
</head>
<body>
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">	
		<h1>Statistiques des fermetures de Caisse</h1>
		<form:form action="cashStatistics.do" method="POST" commandName="cashStatistics">
		<div id="search-row" class="content-row">	
			<div>
				<p>
					<div class="label">
						Entrez la date du rapport cherche
					</div>
					<input id="searchDate" name="searchDate" type="date" value="${cashDate}"/>
				</p>                  
			</div>	
			<div>	
					<input type="button" value="Rechercher" onclick="validateDateInput()"/>	
			       	<input id="validateDate" type="submit" name="search" value="Rechercher" style="visibility:hidden;"/>	
			       	<div class="float-left">
						<img src="<c:url value='/static/images/search.png' />">
					</div> 	
			</div>		
			<div>
				
				<c:if test="${cashStatistics.getClosingDifference() != 0.0}">	
					<div>	
						<p>
							<div class="label">	Difference � la fermeture de la caisse :</div>
							<form:input type="number" minFractionDigits="2" maxFractionDigits="2" class="value" path="closingDifference"
							readonly="true"/>
						</p>
						<p><div class="text"> Commentaires :</div></p>
						<p><form:textarea path="closingComments" rows="4" cols="50" readonly="true"/></p>
						<p><form:input type="mail" path="member.mail" readonly="true"/><p>
					</div>	
				</c:if>
				<c:if test="${cashStatistics.getOpenDifference() != 0.0}">	
					<div>	
						<p>
							<div class="label">	Difference � l'ouverture de la caisse :</div>
							<form:input type="number" minFractionDigits="2" maxFractionDigits="2" class="value" path="openDifference"
							readonly="true"/>
						</p>
						<p><div class="text"> Commentaires :</div></p>
						<p><form:textarea path="openComments" rows="4" cols="50" readonly="true"/></p>
						<p><form:input type="mail" path="openCashMember.mail" readonly="true"/><p>					
					</div>
				</c:if>		
			</div>	
		
	
					
			<table id="sum_table">
				<thead>
					<tr>
						<th class="align-left">Unite</th>
						<th class="align-left">Decompte</th>
						<th class="align-right">Montant</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-left">5�</td>
						<td class="align-left">
							<form:input id="fiveCent" onclick='removeZeroFunction("fiveCent")' 
							onfocusout='unitTypeFunction("fiveCent")' readonly="true"
							onkeyup='unitFunction("fiveCent", 1, 0.05),functionTotaldollars()' path="fiveCent"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.fiveCent*0.05}$</td>
					</tr>
					<tr>
						<td class="align-left">10�</td>
						<td class="align-left">
						<form:input id="tenCent" onclick='removeZeroFunction("tenCent")' 
						onfocusout='unitTypeFunction("tenCent")' readonly="true"
						onkeyup='unitFunction("tenCent", 2, 0.1), functionTotaldollars()' path="tenCent"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.tenCent*0.1}$</td>
					</tr>
					<tr>
						<td class="align-left">25�</td>
						<td class="align-left">
						<form:input id="twentyFiveCent" onclick='removeZeroFunction("twentyFiveCent")' 
						onkeyup='unitFunction("twentyFiveCent", 3, 0.25), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("twentyFiveCent")' path="twentyFiveCent"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.twentyFiveCent*0.25}$</td>
					</tr>
					<tr>
						<td class="align-left">1$</td>
						<td class="align-left">
						<form:input id="oneDollars" onclick='removeZeroFunction("oneDollars")' 
						onkeyup='unitFunction("oneDollars", 4, 1), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("oneDollars")' path="oneDollars"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.oneDollars*1.0}$</td>
					</tr>
					<tr>
						<td class="align-left">2$</td>
						<td class="align-left">
						<form:input id="twoDollars" onclick='removeZeroFunction("twoDollars")' 
						onkeyup='unitFunction("twoDollars", 5, 2), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("twoDollars")' path="twoDollars"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.twoDollars*2.0}$</td>
					</tr>
					<tr>
						<td class="align-left">5$</td>
						<td class="align-left">
						<form:input id="fiveDollars" onclick='removeZeroFunction("fiveDollars")' 
						onkeyup='unitFunction("fiveDollars", 6, 5), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("fiveDollars")' path="fiveDollars"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.fiveDollars*5.0}$</td>
					</tr>
					<tr>
						<td class="align-left">10$</td>
						<td class="align-left">
						<form:input id="tenDollars" onclick='removeZeroFunction("tenDollars")' 
						onkeyup='unitFunction("tenDollars", 7, 10), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("tenDollars")' path="tenDollars"/>
						</td>	
						<td class="rowDataSd align-right">${cashClosingRepport.tenDollars*10.0}$</td>
					</tr>
					<tr>
						<td class="align-left">20$</td>
						<td class="align-left">
						<form:input id="twentyDollars" onclick='removeZeroFunction("twentyDollars")' 
						onkeyup='unitFunction("twentyDollars", 8, 20), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("twentyDollars")' path="twentyDollars"/>
						</td>	
						<td class="rowDataSd align-right">${cashClosingRepport.twentyDollars*20.0}$</td>
					</tr>
					<tr>
						<td class="align-left">50$</td>
						<td class="align-left">
						<form:input id="fiftyDollars" onclick='removeZeroFunction("fiftyDollars")' 
						onkeyup='unitFunction("fiftyDollars", 9, 50), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("fiftyDollars")' path="fiftyDollars"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.fiftyDollars*50.0}$</td>
					</tr>
					<tr>
						<td class="align-left">100$</td>
						<td class="align-left">
						<form:input id="hundredDollars" onclick='removeZeroFunction("hundredDollars")' 
						onkeyup='unitFunction("hundredDollars", 10, 100), functionTotaldollars()' readonly="true"
						onfocusout='unitTypeFunction("hundredDollars")' path="hundredDollars"/>
						</td>
						<td class="rowDataSd align-right">${cashClosingRepport.hundredDollars*100.0}$</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td class="align-right"><b>Montant Total</b></td>
						<td class="align-right"><form:input id="somme" path="total" readonly="true"/></td>
					</tr>
				</tfoot>
			</table>
		</div>	
	</form:form>
	<script>
	function validateDateInput() {
		  
		var searchDate = document.getElementById("searchDate");				
		var errorMessage = "";				
		if(searchDate.value==""){
			errorMessage += "Vous devez entrez une date de recherche.\n";
	    }					
		
		if (errorMessage != "") {
			alert(errorMessage);
		}	
		else {
			document.getElementById("validateDate").click();				
		}
	}	    

	</script>
	</div>
</body>
</html>