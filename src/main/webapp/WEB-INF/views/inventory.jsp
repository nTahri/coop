<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "inventory";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/inventory.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/inventory.css' />">
	
	<title>Inventaire</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<div id="inventory-content">
				<div id="title-row" class="inventory-content-row">
					<div class="float-left"><h1>Inventaire</h1></div>
				</div>
				<div id="search-row" class="inventory-content-row">
					<div class="float-left"><img src="<c:url value='/static/images/search.png' />"></div>
					<div class="float-left"><input type="text" id="inventory-filter" placeholder="Filtrer l'inventaire"></div>
					<div class="float-right filter-tag" id="category-filter-tag" style="display:none;">
						<span id="category-filter-tag-label">Categorie: XXXXXXX</span>
						<span class="filter-tag-remove-btn">X</span>
					</div>
				</div>
				<div id="tables-row" class="inventory-content-row">
					<table id="categories">
						<thead>
							<tr>
								<td>Categorie</td>
							</tr>
						</thead>
						<tbody id="categories-root">
							<c:forEach items="${categories}" var="category">
								<tr class="category-select">
									<td>${category.getName()}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<table id="inventory-items" style="display:none;">
						<thead>
							<tr>
								<td class="align-left">Code</td>
								<td>Item</td>
								<td class="category-column">Categorie</td>
								<td class="align-right">En Stock</td>
								<td class="align-right">Stock Minimum</td>
								<td class="align-right">Prix</td>
								<td></td>
							</tr>
						</thead>
						<tbody id="inventory-items-root">
							<c:forEach items="${products}" var="product">
								<tr id='item-${product.getProductCode()}'
									class='category-${product.getProductCategory().getName()}'
									data-productcode='${product.getProductCode()}'
									data-cat='${product.getProductCategory().getName()}'
									data-name='${product.getProductName()}'
									data-price='${product.getProductSellPrice()}'
									data-description='${product.getProductTaxCat()}'
									data-stock='${product.getProductStock()}'
									data-price='${product.getProductSellPrice()}'>
									<td class='align-left'>${product.getProductCode()}</td>
									<td class='align-left'>${product.getProductName()}</td>
									<td class='align-left category-column'>${product.getProductCategory().getName()}</td>
									<td class='align-right'>${product.getProductStock()}</td>
									<td class='align-right'>${product.getProductStockMin()}</td>
									<td class='align-right'>${product.getProductSellPrice()}$</td>
									<td class="bill-controls icon-column">
										<a href="modifyProduct-${product.getId()}">
											<img class="memberlist-icon" src="<c:url value='/static/images/modify.png' />"></img>
										</a>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>
</div>
</body>
</html>