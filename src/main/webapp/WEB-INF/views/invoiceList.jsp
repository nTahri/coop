<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "invoices";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/invoice.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	
	<title>Liste de Factures</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<div id="search-row" class="content-row" ><br/>
				<%-- <div class="float-left"><img src="<c:url value='/static/images/search.png' />"></div>
				<div class="float-left">
					<input type="text" id="invoice-filter" placeholder="Filtrer Statuts (0/Non-Payee, 1/Payee, 2/Rembourse)" style="width: 300px !important;">
				</div> --%>
				<div class="input-choice">
					<label>Filtre</label>
					<select name="item" id="search-select" class="input-choice">
						<option value=""></option>
					    <option value="1">Payee</option>
					    <option value="0">Non-Payee</option>
					    <option value="2">Remboursee</option>
					</select>
				</div>
			</div>
			<h1>Factures</h1>
			<table>
				<thead>
					<tr>
						<th class="align-left">
							ID
						</th>
						<th class="align-left">
							Client
						</th>
						<th class="align-left">
							Responsable
						</th>
						<th class="align-left">
							Date
						</th>
						<th class="align-right">
							Statut
						</th>
						<th class="align-right">
							Valeur
						</th>
						<th class="billlist-controls">
						</th>
					</tr>
				</thead>
				<tbody id="invoicesList-items-root">
					<c:forEach items="${invoices}" var="invoice">
					<tr id='invoice-${invoice.getId()}'
						data-id='${invoice.getId()}'
						data-buyer='${invoice.getBuyerName()}'
						data-seller='${invoice.getSellerName()}'
						data-trasactionDate='${invoice.getTransactionDate()}'
						data-status='${invoice.getInvoiceStatus()}'
						data-total='${invoice.getTotal()}'
						>
						<td>${invoice.getId()}</td>
						<td>${invoice.getBuyerName()}</td>
						<td>${invoice.getSellerName()}</td>
						<td>
							<fmt:formatDate type="date" value="${invoice.getTransactionDate()}" />
						</td>
						<td class="align-right">
							<c:choose>
								<c:when test="${invoice.getInvoiceStatus() == 1 || invoice.getInvoiceStatus() == 3 || invoice.getInvoiceStatus() == 4}">Paye</c:when>
                                <c:when test="${invoice.getInvoiceStatus() == 0}">Non-paye</c:when>
                                <c:when test="${invoice.getInvoiceStatus() == 2}">Remboursement</c:when>
                                <c:otherwise>Non-paye</c:otherwise>
							</c:choose>
						</td>
						<td class="align-right">
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getTotal()}" />$
						</td>
						<td class="billlist-controls">
							<a href="view-${invoice.getId()}">
								<img class="billlist-icon" src="<c:url value='/static/images/modify.png' />"></img>
							</a>
						</td>
						<td class="billlist-controls">
							<a href="/laderaille/export/downloadPDF/${invoice.getId()}?show=false&emailIt=false">
								<img class="billlist-icon" src="<c:url value='/static/images/download-icon.png' />"></img>
							</a>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>
</body>
</html>