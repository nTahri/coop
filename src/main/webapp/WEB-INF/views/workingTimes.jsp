<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<script type="text/javascript">var context = "events";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Gestion des heures de travail</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/workingTimes.js' />"></script>
	<script type="text/javascript">
		var csrfParameter = '${_csrf.parameterName}';
		var csrfToken = '${_csrf.token}';
	</script>	
</head>
<body>
<div id="wrapper">	
	<%@ include file="navigation.jsp"%>		
	<div id="main-wrapper">
	<form:form action="workingTimes.do" method="POST" commandName="workingTimes">				
		<div id="main-content">
			<h1>Enregistrement de la presence</h1>
			<form:input path="id" type="hidden"/>
			<input type="checkbox" id="myCheck" style="visibility:hidden;"/>		
			<form:input type="hidden" id="memberMail" path="member.mail"/>			
			 	<p>		
					<select id="selectedMyEvent" name="eventId" onchange="change(this)">
						<c:if test="${eventIdStringSelected == null}">
							<option value="NONE" label="Veuillez selectionner l'evenement en cours"/>
						</c:if>	
						<c:if test="${eventIdStringSelected != null}">
							<option id="optionSecledValue" value="NONE" selected="${eventIdStringSelected}" label="${eventIdStringSelected}"/>
						</c:if>		
						<c:forEach items="${eventList}" var="item">
							<option>${item.getName()} @id=${item.getId()}</option>
						</c:forEach>					  		
					</select>  
		        </p>
		        	       
				<div id="bill-client-controls">
					<div id="selected-client-label">Recherchez le Membre</div>
					<div id="selected-client-value"></div>
					<div id="open-members">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
				</div>			
				<div id="sepecificWorkingTimes">
					<p>	
						<h4> Selectionnez ci-dessous le profil du membre</h4>								
						<input id="simpmembre" type="radio" name="profilMember" value="member" onchange="changeFunctionMember(this)"> Membre<br>
						<input id="benevole" type="radio" name="profilMember" value="benevole" onchange="changeFunctionBenevole(this)"> Benevole<br>
					</p> 
					<div style="visibility:hidden;" id="benevoleWorkingTimes">
						<p>
							<h4>Rentrez les heures de travail du benevole</h4>	
							<tr >
								<td>Date de debut :</td>
								<td><input id="startDate" type="datetime-local" name="startDateTime" /></td>
							</tr>
							<tr>
								<td>Date de Fin :</td>
								<td><input id="endDate" type="datetime-local" name="endDateTime" /></td>
							</tr>
						</p>
					</div>
					
					<div id="memberWorkingTimes" style="visibility:hidden;">						
						<p>
								Validez la presence du membre :
								<input type="checkbox" id="myCheckMember" name="checkMember"/>
						</p>
					</div>
					
					<div style="visibility:hidden;">		
						<tr>
							<td>ID Working Times :</td>
							<td><form:input path="id" /></td>
						</tr>
					</div>						
					<p>		
						<input id="submitB" type="button" name="add"  value="Soumettre"  onclick="validateForm()"
						style="visibility:hidden;"/>	
						<input id="submitBB" type="submit" name="add"  value="Soumettre" onclick="sendMail()"
						style="visibility:hidden;"/>																
					</p>	
					<p>		
						<input id="submitM" type="button" name="checkPresence"  value="Soumettre" onclick="validateForm()"
						style="visibility:hidden;"/> 
						<input id="submitMM" type="submit" name="checkPresence"  value="Soumettre" onclick="sendMail()"
						style="visibility:hidden;"/> 
						<input id="getAllEventMember" type="submit" name="showMembers" style="visibility:hidden;"/> 											
					</p>	
					
					<p>		
						<h3 style="color:#DAA520;">${sucessMessage}</h3>
						<h3 style="color:#FF0000;">${errorMessage}</h3>
					</p>	
			
				</div>
			
			<div id="members-wrapper" style="display: none">
					<div id="members-content">
						<div id="title-row" class="content-row">
							<div class="float-left">
								<h1>Membres</h1>
							</div>
							<div id="close-members" class="confirm-btn float-right">X</div>
						</div>
						<div id="search-row" class="content-row">
							<div class="float-left">
								<img src="<c:url value='/static/images/search.png' />">
							</div>
							<div class="float-left">
								<input type="text" id="member-filter" placeholder="Filtrer les membres">
							</div>
						</div>
						<div id="tables-row" class="content-row">
							<table id="members-items">
								<thead>
									<tr>
										<td class="align-left">ID</td>
										<td class="align-right">Nom</td>
										<td class="align-right">Courriel</td>
										<td class="align-right">Statut</td>
									</tr>
								</thead>
								<tbody id="members-items-root">
									<c:forEach items="${members}" var="member">
										<tr id='member-${member.getId()}'
											data-memberid='${member.getId()}'
											data-lastname='${member.getLastName()}'
											data-firstname='${member.getFirstName()}'
											data-displayname='${member.getDisplayName()}'
											data-status='${member.getStatus()}'
											data-mail='${member.getMail()}'>
											<td class='align-left'>${member.getId()}</td>
											<td class='align-right'>${member.getDisplayName()}</td>
											<td class='align-right'>${member.getMail()}</td>
											<td class='align-right'><c:choose><c:when test="${member.getStatus()}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				
			</div>

	</form:form>
	
	<table id="member_table" border="4" style="float: left; margin-top: 100px;
		margin-left: 100px; width:1000px;">
			<tr align="center">		
				<td colspan="2"><h3>Liste des participants � l'evenement</h3></td>			
			</tr>
			<th>Prenom</th>	
			<th>Nom</th>					
			<c:set var="total" value="${0}"/>
			<c:forEach items="${memberList}" var="member">
				<tr>
					<td>${member.firstName}</td>
					<td>${member.lastName}</td>
					<c:set var="total" value="${total + 1}" />
				</tr>
			</c:forEach>	
			<tr align="center">		
				<td>Total</td>	
				<td>${total}</td>
			</tr>
	</table>	
		</div>	
    
	</div>
</body>
</html>
