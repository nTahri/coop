<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "inventory";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/categoryList.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	
	<title>Categories</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<div id="title-row" class="content-row">
				<div class="float-left"><h1>Categories</h1></div>
			</div>
			<div id="search-row" class="content-row">
				<div class="float-left">
					<img src="<c:url value='/static/images/search.png' />">
				</div>
				<div class="float-left">
					<input type="text" id="filter" placeholder="Filtrer les categories">
				</div>
			</div>
			<div id="tables-row" class="content-row">

				<table>
					<thead>
						<tr>
							<th class="align-left">
								Name
							</th>
							<!-- <th class="bill-controls">
							</th> -->
						</tr>
					</thead>
					<tbody id="categories-root">
						<c:forEach items="${categories}" var="category">
						<tr>
							<td class="align-left">${category.getName()}</td>
							<!-- <td class="billlist-controls">
								<a href="deleteCategory-${category.getId()}">
									<img class="billlist-icon" src="<c:url value='/static/images/delete.png' />"></img>
								</a>
							</td> -->
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div id="new-category" class="content-row">
				<form:form method="POST" modelAttribute="category">
					<form:input type="hidden" path="id" id="id" />
					<h1>Nouvelle Categorie</h1>
					<form:input class="single-input" type="text" path="name" placeholder="Nom" /><br>
					<div class="has-error">
						<form:errors path="name" class="help-inline"/>
					</div>
					<br>
					<div id="form-controls">
						<input type="submit" value="Ajouter" class="single-input confirm-btn float-left" />
					</div>
				</form:form>
			</div>
		</div>
	</div>
</div>
</body>
</html>