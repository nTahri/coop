<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<script type="text/javascript">var context = "invoices";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/modifyInvoice.js' />"></script>
	<script type="text/javascript">
		var csrfParameter = '${_csrf.parameterName}';
		var csrfToken = '${_csrf.token}';
	</script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	
	<c:choose>
		<c:when test="${paiment}">
			<title>Payment de Facture</title>
		</c:when>
		<c:otherwise>
			<title>Remboursement de Facture</title>
		</c:otherwise>
	</c:choose>
</head>
<body>
	<div id="wrapper">
		<%@ include file="navigation.jsp"%>
		<div id="tax" data-tvq="${tvq}" data-tps="${tps}" style="display: none;"></div>
		<div id="main-wrapper">
			<div id="main-content">
				
				<c:choose>
					<c:when test="${paiment}">
						<h1>Payment de Facture</h1>
					</c:when>
					<c:otherwise>
						<h1>Remboursement de Facture</h1>
					</c:otherwise>
				</c:choose>
				<div id="bill-client-controls">
					<div id="selected-client-label">Client</div>
					<div id="selected-client-value">${invoice.getBuyerName()}</div>
					<%-- <div id="open-members">
						<img src="<c:url value='/static/images/search.png' />">
					</div> --%>
				</div>
				<div id="bill-event-controls">
					<div id="selected-event-label">evenement</div>
					<div id="selected-event-value">${event}</div>
					<%-- <div id="open-events">
						<img src="<c:url value='/static/images/search.png' />">
					</div> --%>
				</div>
				<table id="bill-items">
					<thead>
						<tr>
							<td>Item</td>
							<td class="align-right">Quantite</td>
							<td></td>
							<td></td>
							<td class="align-right item-value">Prix</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="bill-items-root">
						<!-- here must be the loop for the items -->
						<tr id='bill-add-row' data-invoice="invoice-${invoice.getId()}">
							<td class='align-center'></td>
							<c:forEach items="${invoiceLinesFromMap}" var="invoiceLine">
								<tr id="invoice-${invoice.getId()}">
									<td>${invoiceLine.productName}</td>
									<c:choose>
										<c:when test="${paiment}">
											<td class="align-right">${invoiceLine.quantity}</td>
										</c:when>
										<c:otherwise>
											<td class="align-right">-${invoiceLine.quantity}</td>
										</c:otherwise>
									</c:choose>
									
									<td></td>
									<td></td>
									<td class="align-right">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoiceLine.price}" />
									</td>
									<td></td>
								</tr>
							</c:forEach>
						</tr>
					</tbody>
					<tfoot>
						<tr id="bill-subtotal">
							<td>Sous-Total</td>
							<td></td>
							<td></td>
							<td></td>
							<c:choose>
								<c:when test="${paiment}">
									<td class="align-right" id="bill-subtotal-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getSubtotal()}" />
									</td>
								</c:when>
								<c:otherwise>
									<td class="align-right" id="bill-subtotal-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="-${invoice.getSubtotal()}" />
									</td>
								</c:otherwise>
							</c:choose>
							
						</tr>
						<tr id="bill-sales-tax">
							<td>TVQ</td>
							<td></td>
							<td></td>
							<td></td>
							<c:choose>
								<c:when test="${paiment}">
									<td class="align-right" id="bill-sales-tax-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tvq}" />
									</td>
								</c:when>
								<c:otherwise>
									<td class="align-right" id="bill-sales-tax-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="-${tvq}" />
									</td>
								</c:otherwise>
							</c:choose>
							
						</tr>
						<tr id="bill-tax">
							<td>TPS</td>
							<td></td>
							<td></td>
							<td></td>
							<c:choose>
								<c:when test="${paiment}">
									<td class="align-right" id="bill-tax-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tps}" />
									</td>
								</c:when>
								<c:otherwise>
									<td class="align-right" id="bill-tax-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="-${tps}" />
									</td>
								</c:otherwise>
							</c:choose>
							
						</tr>
						<tr id="bill-total">
							<td>Total</td>
							<td></td>
							<td></td>
							<td></td>
							<c:choose>
								<c:when test="${paiment}">
									<td class="align-right" id="bill-total-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getTotal()}" />
									</td>
								</c:when>
								<c:otherwise>
									<td class="align-right" id="bill-total-value">
										<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="-${invoice.getTotal()}" />
									</td>
								</c:otherwise>
							</c:choose>
							
						</tr>
					</tfoot>
				</table>
				<c:choose>
						<c:when test="${paiment}">
							<div id="bill-controls">
								<div id="paimentt-invoice" class="confirm-btn float-right">Payer</div>
							</div>
						</c:when>
						<c:otherwise>
							<div id="bill-controls">
								<div id="refound-invoice" class="confirm-btn float-right">Rembourser</div>
							</div>
						</c:otherwise>
					</c:choose>
				
			</div>
		</div>
		
	</div>
</body>
</html>