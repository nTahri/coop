<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>La Deraille - Connection</title>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>
	<link href="<c:url value='/static/css/laderaille.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/login.css' />" rel="stylesheet"></link>
</head>
<body>
	<div id="login-wrapper">
		<div id="login-logo">
			<img src="<c:url value='/static/images/bicycle-black.png' />">
		</div>
		<div id="login-label">
			La Deraille
		</div>
		<div id="login-info">
			<div>${success}</div>
			<a href="memberList"><div>Liste des membres</div></a>
		</div>
	</div>
</body>
</html>