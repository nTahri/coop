<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "admin";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Gestion des heures de travail</title>
</head>
<body>

	<form:form commandName="workingTimesAuto">

		<table>
			<tr>
				<td>Starting Work at :</td>
				<td><form:input path="startDateTime" /></td>
			</tr>
			
			<tr>
				<td>Ending Work at :</td>
				<td><form:input path="endDateTime" /></td>
			</tr>
			
			<tr>
				<td>Your timing is :</td>
				
			</tr>
			
			<tr>
				<td colspan="2"><input type="submit" value="SaveTimes" />
				</td>

			</tr>
		</table>
	</form:form>
</body>
</html>