<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>La Deraille - Erreur</title>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/invoice.js' />"></script>
	
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>
	<link href="<c:url value='/static/css/laderaille.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/login.css' />" rel="stylesheet"></link>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<h1>Erreur</h1>
			<div id="login-label">
			La Deraille
		</div>
		<div id="login-info">
			<div>${message}</div>
			<a href="${backPage}"><div>Retour � la page precedente</div></a>
		</div>
		</div>
	</div>
</div>
</body>
</html>