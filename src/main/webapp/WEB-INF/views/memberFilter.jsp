<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">var context = "members";</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Filtre de membres</title>
<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>	
</head>
<body>
	<%@ include file="navigation.jsp" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<div id="main-wrapper">	
	<c:set var="control" value="${1}"/>
	<input var="control" type="number" value="${0}" id="contNum" style="display: none;"/>
	<form:form action="memberFilter.do" method="POST"
		commandName="memberFilter">	
		<input type="checkbox" name="showcontrol" id="myCheck" style="display: none;"/>	
		<div id="search-row" class="content-row">					
					<div class="float-left">
						 <p>						
						<form:select id="searchCriteria" path="searchCriteria" onchange="change(this)" label="${memberFilter.getSearchCriteria()}">
					  		
					  		<c:if test="${memberFilter.getSearchCriteria() == null}">
									<form:option value="NONE" label="Veuillez selectionner un critere de recherche" />
							</c:if>					
							<c:if test="${memberFilter.getSearchCriteria() != null}">
									<form:option value="${memberFilter.getSearchCriteria()}" />
							</c:if>					
								
					  		 <c:forEach items="${criteriaList}" var="item">
							   <option>${item.searchCriteria}</option>
							  </c:forEach>					  		
				       </form:select>  
				       </p>
				       <p>
							<select name="ageInterval" id="age" style="visibility:hidden;">
						  		<option value="NONE" label="Veuillez selectionner une tranche d'�ge" />
						  		 <c:forEach items="${ageIntervalEnumList}" var="item">
								   <option>${item.getfullAgeInterval()}</option>
								  </c:forEach>					  		
					       <select>  
				       </p>	
				       <p>
							<select name="typeAbonnement" id="abonnement" style="visibility:hidden;">
						  		<option value="NONE" label="Veuillez selectionner le type d'abonnement" />
						  		 <c:forEach items="${typeAbonnementEnumList}" var="item">
								   <option>${item.getTypeAbonnement()}</option>
								  </c:forEach>					  		
					       <select>  
				       </p>	
				       	<div id="searchDate" style="visibility:hidden;">
				        <p>
				        	Selectionnez une periode de recherche
				        </p>
				        <p> 
							Debut :<input id="searchFirstDate"  type="date" value ="${startDate}" name = "startDate"/>
							Fin :	<input id="startSecondDate" type="date" value ="${endDate}" name = "endDate" />
				      	</p>
				      		
				       	</div>	       		       
				       <input type="button" value="Rechercher" onclick="validateWorkHourForm()"/>	
				       <input id="recherche" type="submit" name="search" value="Rechercher" style="visibility:hidden;"/>	
				       <!--   <input type="button" value="Exporter" onclick="validateWorkHourForm()"/>-->
				       <input  id="exporte" type="submit" name="export" value="Exporter" style="visibility:hidden;"/>	
				       <div class="float-left">
							<img src="<c:url value='/static/images/search.png' />">
						</div>            
					</div>				
		</div>			
					
	</form:form>	
	
		<table id="member_table">
			<thead>
				<tr>
					<th class="align-left">Prenom</th>
					<th class="align-left">Nom</th>
					<th class="align-left">Mail</th>
					<th class="align-left">Status</th>		
					<th class="align-left">Date d'inscription</th>	
					<th class="align-right">Telephone</th>		
					<th class="align-right">Date de depart</th>
				</tr>
			</thead>
			<tbody>
			<c:set var="total" value="${0}"/>
			<c:forEach items="${memberList}" var="member">
				<tr>
					<td class="align-left">${member.firstName}</td>
					<td class="align-left">${member.lastName}</td>
					<td class="align-left">${member.mail}</td>					
					<c:if test="${member.status == true}">
						<td class="align-left">Actif</td>	
					</c:if>					
					<c:if test="${member.status == false}">
						<t class="align-left">Inactif</td>	
					</c:if>					
					<td class="align-left"><fmt:formatDate value="${member.joiningDate}" pattern='dd/MM/yyyy' /></td>
					<td class="align-right">${member.phoneNumber}</td>
					<td class="align-right"><fmt:formatDate value="${member.leftDate}" pattern='dd/MM/yyyy' /></td>
					<c:set var="total" value="${total + 1}" />
				</tr>
			</c:forEach>
			</tbody>	
			<tfoot>
				<tr>	
					<td></td>
					<td></td>	
					<td></td>	
					<td></td>	
					<td></td>			
					<td class="align-right"><b>Nombre Total de membres</b></td>
					<td class="align-right">${total}</td>
				</tr>
		</table>	

	<script type="text/javascript">
		
		function showTableFunction(){		
			var textarea = document.getElementById("member_table");
		    textarea.style.visibility = 'visible';
   		}

   		/*/
		function unCheckBoxFunction(){		
			document.getElementById("myCheck").checked = true;
			
		}
		/*/
		if(searchCriteria[searchCriteria.selectedIndex].value != "NONE")
		{	
			document.getElementById("exporte").style.visibility = 'visible';			
		}
		
		
		function change(obj) {
			var selectBox = obj;
		    var selected = selectBox.options[selectBox.selectedIndex].value;
		    //var textarea = document.getElementById("member_table");
		    var datecriteria = document.getElementById("searchDate").style.visibility = 'hidden';
		    var agecriteria = document.getElementById("age").style.visibility = 'hidden';
	    	var abonnementcriteria = document.getElementById("abonnement").style.visibility = 'hidden';
		    
		    searchDate
		    //alert("CASE DEFAULT" + selected);
		    if (selected==="Membres qui ont quitte la Coop.")
		    {
		    	
		    }
		    if (selected==="Membres par tranche d'�ge")
		    {
		    	agecriteria = document.getElementById("age").style.visibility = 'visible';
		    }
		    if (selected==="Membres selon la periode d'inscription")
		    {
		    	datecriteria = document.getElementById("searchDate").style.visibility = 'visible';
		    	document.getElementById("exporte").style.visibility = 'hidden';
		    }
		    if (selected==="Membres en fonction de leur implication")
		    {
		    }
		    if (selected==="Membres dont l'abonnement � expire")
		    {
		    }
		    if (selected==="Membres en fonction du type d'abonnement")
		    {
		    	abonnementcriteria = document.getElementById("abonnement").style.visibility = 'visible';
		    	
		    }
		    if (selected==="Tous les membres")
		    {
		    	
		    }	
		}	    
	   function validateWorkHourForm() {
			var searchCriteria = document.getElementById("searchCriteria");
			var startDate = document.getElementById("searchFirstDate");
			var endDate = document.getElementById("startSecondDate");			 
			
			// if(searchCriteria[searchCriteria.selectedIndex].value == "NONE"){
			//  	alert("Thats correct"); 
			// }
		
			
			var errorMessage = ""; 
			if(searchCriteria[searchCriteria.selectedIndex].value == "NONE")
			{				
		        errorMessage += "Vous devez selectionner un critere de recherche.\n";		   
			}
				
			if(searchCriteria[searchCriteria.selectedIndex].value == "Membres selon la periode d'inscription")
			{				
				if(startDate.value==""||endDate.value==""){
					errorMessage += "Vous devez entrez une periode de recherche.\n";
			    }
			}			
			
			if (errorMessage != "") {
				alert(errorMessage);
			}	
			else {
				document.getElementById("recherche").click();				
			}
		}	    
				
	</script>
	</div>
</body>
</html>
