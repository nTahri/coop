<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:h="http://java.sun.com/jsf/html">
<head>
	<script type="text/javascript">var context = "invoices";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/addInvoice.js' />"></script>
	<script type="text/javascript">
		var csrfParameter = '${_csrf.parameterName}';
		var csrfToken = '${_csrf.token}';
		var notPaiedinvoices = '${notPaiedinvoices}';
	</script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<title>Nouvelle Facture</title>
</head>
<body>
	<div id="wrapper">
		<%@ include file="navigation.jsp"%>
		<div id="tax" data-tvq="${tvq}" data-tps="${tps}" style="display: none;"></div>
		<div id="main-wrapper">
			<div id="main-content">
				<h1>Nouvelle Facture</h1>
				<div id="bill-client-controls">
					<div id="selected-client-label">Client</div>
					<div id="selected-client-value"></div>
					<div id="open-members">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
				</div>
				<div id="bill-event-controls">
					<div id="selected-event-label">evenements</div>
					<div id="selected-event-value"></div>
					<div id="open-events">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
				</div>
				<table id="bill-items">
					<thead>
						<tr>
							<td>Item</td>
							<td class="align-right">Quantite</td>
							<td class="align-right">Rabais</td>
							<td></td>
							<td class="align-right item-value">Prix</td>
							<td></td>
						</tr>
					</thead>
					<tbody id="bill-items-root">
						<tr id="bill-add-row">
							<td class="align-center"><div id="bill-add-item">Ajouter</div></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
					<tfoot>
						<tr id="bill-subtotal">
							<td>Sous-Total</td>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right" id="bill-subtotal-value">0$</td>
						</tr>
						<tr id="bill-sales-tax">
							<td>TVQ</td>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right" id="bill-sales-tax-value">0$</td>
						</tr>
						<tr id="bill-tax">
							<td>TPS</td>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right" id="bill-tax-value">0$</td>
						</tr>
						<tr id="bill-total">
							<td>Total</td>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right" id="bill-total-value">0$</td>
						</tr>
					</tfoot>
				</table>

				<div id="bill-controls">
					<div id="save-invoice" class="confirm-btn float-right">Sauvegarder</div>
				</div>
				<div id="inventory-wrapper" style="display: none">
					<div id="inventory-content">
						<div id="title-row" class="content-row">
							<div class="float-left">
								<h1>Inventaire</h1>
							</div>
							<div id="close-inventory" class="confirm-btn float-right">X</div>
						</div>
						<div id="search-row" class="content-row">
							<div class="float-left">
								<img src="<c:url value='/static/images/search.png' />">
							</div>
							<div class="float-left">
								<input type="text" id="inventory-filter" placeholder="Filtrer l'inventaire">
							</div>
							<div class="float-right filter-tag" id="category-filter-tag" style="display: none;">
								<span id="category-filter-tag-label">Categorie: XXXXXXX</span>
								<span class="filter-tag-remove-btn">X</span>
							</div>
						</div>
						<div id="tables-row" class="content-row">
							<table id="categories">
								<thead>
									<tr>
										<td>Categorie</td>
									</tr>
								</thead>
								<tbody id="categories-root">
									<c:forEach items="${categories}" var="category">
										<tr class="category-select">
											<td>${category.getName()}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
							<table id="inventory-items" style="display: none;">
								<thead>
									<tr>
										<td>Item</td>
										<td class="category-column">Categorie</td>
										<td class="align-right">En Stock</td>
										<td class="align-right">Prix</td>
									</tr>
								</thead>
								<tbody id="inventory-items-root">
									<c:forEach items="${products}" var="product">
										<tr id='item-${product.getProductCode()}'
											class='category-${product.getProductCategory().getName()}'
											data-productcode='${product.getProductCode()}'
											data-cat='${product.getProductCategory().getName()}'
											data-name='${product.getProductName()}'
											data-price='${product.getProductSellPrice()}'
											data-ref='${product.getProductRef()}'
											data-description='${product.getProductTaxCat()}'
											data-stock='${product.getProductStock()}'
											data-minStock='${product.getProductStockMin()}'
											data-price='${product.getProductSellPrice()}'>
											<td class='align-left'>${product.getProductName()}</td>
											<td class='align-left category-column'>${product.getProductCategory().getName()}</td>
											<td class='align-right'>${product.getProductStock()}</td>
											<td class='align-right'>${product.getProductSellPrice()}$</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
						<div id="selected-item-row" class="content-row"
							style="display: none">
							<h2 id="selected-item-name-label">ITEM_NAME</h2>
							<div id="selected-item-description">DESCRIPTION</div>
							<div id="controls-row">
								<div id="deselect-item" class="confirm-btn float-left">Annuler</div>
								<div id="inventory-add-button" class="confirm-btn float-right">Ajouter</div>
								<div id="quantity-controls" class="float-right">
									<span>Quantite: </span> <input id="inventory-add-quantity"
										type="text" placeholder="Quantite" value=1>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="members-wrapper" style="display: none">
					<div id="members-content">
						<div id="title-row" class="content-row">
							<div class="float-left">
								<h1>Membres</h1>
							</div>
							<div id="close-members" class="confirm-btn float-right">X</div>
						</div>
						<div id="search-row" class="content-row">
							<div class="float-left">
								<img src="<c:url value='/static/images/search.png' />">
							</div>
							<div class="float-left">
								<input type="text" id="member-filter" placeholder="Filtrer les membres">
							</div>
						</div>
						<div id="tables-row" class="content-row">
							<table id="members-items">
								<thead>
									<tr>
										<td class="align-left">ID</td>
										<td class="align-right">Nom</td>
										<td class="align-right">Courriel</td>
										<td class="align-right">Statut</td>
									</tr>
								</thead>
								<tbody id="members-items-root">
									<c:forEach items="${members}" var="member">
										<tr id='member-${member.getId()}'
											data-memberid='${member.getId()}'
											data-lastname='${member.getLastName()}'
											data-firstname='${member.getFirstName()}'
											data-displayname='${member.getDisplayName()}'
											data-status='${member.getStatus()}'
											data-mail='${member.getMail()}' 
											data-d='${member.getExpiringDate()}' >
											<td class='align-left'>${member.getId()}</td>
											<td class='align-right'>${member.getDisplayName()}</td>
											<td class='align-right'>${member.getMail()}</td>
											<td class='align-right'><c:choose><c:when test="${member.getStatus()}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="events-wrapper" style="display:none">
					<div id="events-content">
						<div id="title-events-row" class="content-row">
							<div class="float-left">
								<h1>evenements</h1>
							</div>
							<div id="close-events" class="confirm-btn float-right">X</div>
						</div>
						<div id="search-row" class="content-row">
							<div class="float-left">
								<img src="<c:url value='/static/images/search.png' />">
							</div>
							<div class="float-left">
								<input type="text" id="event-filter" placeholder="Filtrer les evenements">
							</div>
						</div>
						<div id="tables-row" class="content-row">
							<table id="events-items">
								<thead>
									<tr>
										<th class="align-left">
											ID
										</th>
										<th class="align-left">
											Nom
										</th>
										<th class="align-right">
											Debut
										</th>
										<th class="align-right">
											Fin
										</th>
									</tr>
								</thead>
								<tbody id="events-items-root">
									<c:forEach items="${events}" var="event">
									<tr id='event-${event.getId()}'
										data-eventid='${event.getId()}'
										data-name='${event.getName()}'
										data-startDate='${event.getStartDate()}'
										data-endDate='${event.getEndDate()}'
										data-owner='${member.getOwnerId()}'>
										<td class="align-left">${event.getId()}</td>
										<td class="align-left">${event.getName()}</td>
										<td class="align-right">${event.getStartDate()}</td>
										<td class="align-right">${event.getEndDate()}</td>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="paiment-choose-wrapper" style="display: none">
			<div id="paiment-choose-content">
				<div id="title-paiment-row" class="content-row">
					<div class="float-left">
						<h3>Mode de paiment</h3>
					</div>
					<div id="close-paiment" class="confirm-btn float-right">X</div>
					<br/><br/><br/><br/>
					<div id="paiment-controls-row">
					    <div>
							<input type="checkbox" class="single-input" id="destination-print" value="1"/> Imprimer
							<input type="checkbox" class="single-input" id="destination-email" value="2" checked/> Envoyer par courriel
						</div>
						<div>
						    <input type="radio" class="single-input" name="paiment-mode" value="1" checked/> Cash
					        <input type="radio" class="single-input" name="paiment-mode" value="0" /> Non-payee
					        <input type="radio" class="single-input" name="paiment-mode" value="3" /> Debit
					        <input type="radio" class="single-input" name="paiment-mode" value="4" /> Credit
					    </div>
						
					<div id="paiment-btn" class="confirm-btn float-right">Confirmer</div>	
					</div>
					
				</div>
			</div>
		</div>
		<div id="info-wrapper" style="display: none">
			<div id="paiment-choose-content">
				<div id="title-paiment-row" class="content-row">
					<div class="float-left">
						<h3>Informations essentielles:</h3>
					</div>
					<div id="close-info" class="confirm-btn float-right">X</div>
					<br/><br/><br/><br/>
					<div id="info-controls-row">
					    	<div id="info-container"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>