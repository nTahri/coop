<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "inventory";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/addProduct.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/registration.css' />">
	
	<title>Produits</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<form:form method="POST" modelAttribute="product">
				<input type="hidden" path="id" id="id"/>
				<c:choose>
					<c:when test="${edit}">
						<h1>Modifier Produit</h1>
					</c:when>
					<c:otherwise>
						<h1>Nouveau Produit</h1>
					</c:otherwise>
				</c:choose>
				<div>
					<div class="form-label">Nom</div>
					<form:input class="single-input" path="productName" type="text" required="true" /><br>
					<div class="has-error">
						<form:errors path="productName" class="help-inline"/>
					</div>
				</div>
				
				<div>
					<div class="form-label">Code</div>
					<form:input class="single-input" path="productCode" type="text" required="true" /><br>
					<div class="has-error">
						<form:errors path="productCode" class="help-inline"/>
					</div>
				</div>
				
				<%-- <div>
					<div class="form-label">Type de code</div>
					<form:input class="single-input" path="productCodeType" type="text" required="true" /><br>
					<div class="has-error">
						<form:errors path="productCodeType" class="help-inline"/>
					</div>
				</div> --%>
				
				<div>
					<div class="form-label">Reference</div>
					
					<form:input class="single-input" path="productRef" type="text" 	placeholder="Ajouter le prefixe M pour la reference du membership ex: M12345" required="true"/>
					<a><img class="info-icn" src="<c:url value='/static/images/info.png' />"></img></a>
					<div id="info_ref">MAxxx: abonnement annule <br />MMxxx: abonnement mensuel  .</div>
					<div class="has-error">
						<form:errors path="productRef" class="help-inline"/>
					</div>
				</div>
				
				<div>
					<div class="form-label">Quantite en Stock</div>
					<form:input class="single-input" path="productStock" type="number" min="0" required="true" /><br>
					<div class="has-error">
						<form:errors path="productStock" class="help-inline"/>
					</div>
				</div>
				
				<div>
					<div class="form-label">Quantite en Stock Minimale</div>
					<form:input class="single-input" path="productStockMin" type="number" min="0" required="true" /><br>
					<div class="has-error">
						<form:errors path="productStockMin" class="help-inline"/>
					</div>
				</div>
				
				<div>
					<div class="form-label">Prix de vente</div>
					<form:input class="single-input" path="productSellPrice" type="number" step="0.01" min="0.00" required="true" /><br>
					<div class="has-error">
						<form:errors path="productSellPrice" class="help-inline"/>
					</div>
				</div>
				
				<div>
					<div class="form-label">Prix d'achat</div>
					<form:input class="single-input" path="productPrice" type="number" step="0.01" min="0.00" required="true" /><br>
					<div class="has-error">
						<form:errors path="productPrice" class="help-inline"/>
					</div>
				</div>

				<div class="input-choice-label">Categorie</div>
				<div class="input-choice">
					<form:select path="productCategory.id" items="${categories}" multiple="false" itemValue="id" itemLabel="name" />
					<div class="has-error">
						<form:errors path="productCategory" class="help-inline"/>
					</div>
				</div>
				<div class="input-choice-label">Categorie de taxes</div>
				<div class="input-choice">
					<form:select path="productTaxCat" multiple="false">
						<form:option value="STANDARD" label="STANDARD"/>
						<form:option value="NON TAXABLE" label="NON TAXABLE"/>
					</form:select>
					<div class="has-error">
						<form:errors path="productTaxCat" class="help-inline"/>
					</div>
				</div>
				<div id="form-controls">
				<c:choose>
					<c:when test="${edit}">
						<input type="submit" value="Modifier" class="single-input confirm-btn float-left">
					</c:when>
					<c:otherwise>
						<input type="submit" value="Ajouter" class="single-input confirm-btn float-left">
					</c:otherwise>
				</c:choose>
				</div>
			</form:form>
		
			<div id="export-div">
				<a id="export-btn" class="export-btn float-right" href="/laderaille/in/importexport" style="text-decoration: none !important;"> 
					<span>Import/Export</span>
				</a>
			</div>
		</div>
	</div>
</div>
</body>
</html>