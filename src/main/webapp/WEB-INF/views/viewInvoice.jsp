<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "invoices";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/invoice.js' />"></script>
	<script src="<c:url value='/static/js/modifyInvoice.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	
	<title>Factures</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<h1>Facture #${invoice.getId()}</h1>
			<div id="transactiondate">
				<label class="label">Date de transaction</label><label class="value">
				
					<fmt:formatDate type="both" value="${invoice.getTransactionDate()}" />
				</label>
			</div>
			<div id="event">
				<label class="label">evenement</label><label class="value">${event.getName()}</label><label class="value">
				
					<fmt:formatDate type="both" value="${event.getStartDate()}" />
				</label>
			</div>
			<div id="member">
				<label class="label">Client</label><label class="value">${invoice.getBuyerName()}</label>
			</div>
			<div id="items">
				<table>
					<thead>
						<tr>
							<td class="align-left">Produit</td>
							<td class="align-right">Quantite</td>
							<td class="align-left">Prix</td>
							<td class="align-right">Rabais</td>
							<td class="align-right">Valeur</td>
						</tr>
					</thead>
					<tbody>
					<c:forEach items="${invoiceLinesFromMap}" var="il">
						<tr>
							<td class="align-left">${il.productName}</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="0" maxFractionDigits="0" value="${il.quantity}" /></td>
							<td class="align-left"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${il.price}" />$</td>
							<td class="align-right">${il.discount}</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${il.price}" />$</td>
						</tr>
					</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right label">Sous-Total</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getSubtotal()}" />$</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right label">TVQ</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tvq_v}" />$</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right label">TPS</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tps_v}" />$</td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td class="align-right label">Total</td>
							<td class="align-right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getTotal()}" />$</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div id="controls">
				<c:choose>
					<c:when test="${invoice.getInvoiceStatusBoolean()}">
						<div id="paiementdate">
							<label class="label">Date de paiement</label><label class="value">
								<fmt:formatDate type="both" value="${invoice.getPaiementDate()}" />
							</label>
						</div>
						<a id="refound-btn" class="pay-btn confirm-btn float-right" data-id="${invoice.getId()}" href="#" style="text-decoration: none !important;"> 
							<span>Rembourser</span>
						</a>
					</c:when>
					<c:otherwise>
						<a id="pay-btn" class="pay-btn confirm-btn float-right" data-id="${invoice.getId()}" href="#" style="text-decoration: none !important;"> 
							<span>Payer</span>
						</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>
