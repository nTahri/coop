<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "admin";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Gestion des heures de travail</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>	
</head>
<body>
	<%@ include file="navigation.jsp" %>
<div id="main-wrapper">	
	<form:form action="workingTimes.do" method="POST"
		commandName="workingTimes">
		
		<p>
  			Courriel du membre:
			<input type="mail" name="mail" />
		</p>
		<p>
  			Periode de recherche:
			Debut :	<input type="date" name = "startDate"/>
			Fin :	<input type="date" name = "endDate" />	
		</p>	
		
			<tr>
				<td>Date de debut :</td>
				<td><form:input type="date" path="startDateTime" /></td>
			</tr>
			<tr>
				<td>Date Fin :</td>
				<td><form:input type="date" path="endDateTime" /></td>
			</tr>
			
			<div style="visibility:hidden;">		
				<tr>
					<td>ID Working Times :</td>
					<td><form:input path="id" /></td>
				</tr>
			</div>

			<tr>
				<td>Member Mail :</td>
				<td><form:input  type="mail" path="member.mail"/></td>
				<td><form:input type="hidden" path="member.id"/></td>
			</tr>	
			<p>		
				<tr margin="2">
					<td colspan="2" border="1">
						<input type="submit" name="add" value="Ajouter" /> 					
						<input type="submit" name="update" value="Modifier" />
						<input type="submit" name="delete" value="Supprimer" /> 
						<input type="submit" name="searchworkingtimes" value="Recherche 1 heure" /> 					
						<input type="submit" name="searchmembertimes" value="Recherche Heures du membre" />					
						<input type="submit" name="searchmembertimesByPeriod" value="Recherche Heures du membre/Periode" /></td>
				</tr>
			</p>	

	</form:form>
	<table border="4" style="float: left; width:1000px;">
		<th>ID</th>
		<th>Member ID</th>
		<th>Start Time</th>
		<th>End Time</th>
		<th>Hours</th>
		<c:set var="total" value="${0}"/>
		<c:forEach items="${SearchworkingTimesList}" var="workingTimes">
			<tr>
				<td>${workingTimes.id}</td>
				<td>${workingTimes.member.mail}</td>
				<td>${workingTimes.startDateTime}</td>
				<td>${workingTimes.endDateTime}</td>				
				<td>${workingTimes.endDateTime.getTime()/3600000 - workingTimes.startDateTime.getTime()/3600000}</td>
				<c:set var="total" value="${total + workingTimes.endDateTime.getTime()/3600000 - workingTimes.startDateTime.getTime()/3600000}" />
			</tr>
		</c:forEach>	
		<tr align="center">			
			<td colspan="4">Total times </td>
			<td > ${total}</td>
		</tr>
	</table>
	</div>
</body>
</html>
