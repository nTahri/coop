<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>La Deraille - Connection</title>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/login.js' />"></script>
	<link href="<c:url value='/static/css/laderaille.css' />" rel="stylesheet"></link>
	<link href="<c:url value='/static/css/login.css' />" rel="stylesheet"></link>
	
	<title>Supprimer un Membre</title>
</head>
<body>
	<div id="login-wrapper">
		<div id="login-logo">
			<img src="<c:url value='/static/images/logo_black.png' />">
		</div>
		<div id="login-info">
			<div>${deleteMessage}</div>
			<a href="deleteEvent-${event.id}"><div>Supprimer evenement</div></a>
		</div>
	</div>
</body>
</html>