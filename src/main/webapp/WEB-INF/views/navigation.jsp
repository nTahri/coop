<div id="sidebar-wrapper">
	<div id="sidebar-header">
		<a href="/laderaille/">
			<div id="sidebar-logo">
				<img src="<c:url value='/static/images/logo_black.png' />"></img>
			</div>
			<!-- <div id="sidebar-title" class="title">La Deraille</div> -->
		</a>
	</div>
	<div id="sidebar-content">
		<ul>
			<li class="navlist-expand" id="navlist-expand-events">
				<div class='navlist'>
					<img src="<c:url value='/static/images/calendar.png' />" class="navlist-icon"> 
					<span>
						<span>evenements</span>
						<i class='navlink-arrow navlink-arrow-right'></i>
					</span>
				</div>
				<ul style='display:none' id="navlist-events">
					<a href="/laderaille/event/addEvent"> 
						<li class="navlink">
							<span>Nouveau</span>
						</li>
					</a>
					<a href="/laderaille/event/eventList">
						<li class="navlink">
							<span>Liste</span>
						</li>
					</a>
					<a href="/laderaille/workingTimes">
						<li class="navlink">
							<span>Presence de membres</span>
						</li>
					</a>
					<a href="/laderaille/closingCash">
						<li class="navlink">
							<span>Ouverture de la caisse</span>
						</li>
					</a>
					<a href="/laderaille/closingCash/cashClosingRepport">
						<li class="navlink">
							<span>Fermeture de la caisse</span>
						</li>
					</a>					
				</ul>
			</li>

			<li class="navlist-expand" id="navlist-expand-members">
				<div class='navlist'>
					<img src="<c:url value='/static/images/user.png' />" class="navlist-icon">
					<span>
						<span>Membres</span>
						<i class='navlink-arrow navlink-arrow-right'></i>
					</span>
				</div>
				<ul style='display:none' id="navlist-members">
					<a href="/laderaille/addMember">
						<li class="navlink">
							<span>Nouveau</span>
						</li>
					</a>
					<a href="/laderaille/memberList">
						<li class="navlink">
							<span>Liste</span>
						</li>
					</a>
					<a href="/laderaille/memberFilter">
						<li class="navlink">
							<span>Statistiques membres</span>
						</li>
					</a>
					
				</ul>
			</li>
			
			<li class="navlist-expand" id="navlist-expand-invoices">
				<div class='navlist'>
					<img src="<c:url value='/static/images/dollar.png' />" class="navlist-icon">
					<span>
						<span>Factures</span>
						<i class='navlink-arrow navlink-arrow-right'></i>
					</span>
				</div>
				<ul style='display:none' id="navlist-invoice">
					<a href="/laderaille/invoice/addInvoice">
						<li class="navlink">
							<span>Nouvelle Facture</span>
						</li>
					</a>
					<a href="/laderaille/invoice/invoiceList">
						<li class="navlink">
							<span>Liste</span>
						</li>
					</a>
					<a href="/laderaille/openInvoiceList">
						<li class="navlink">
							<span>Factures ouvertes</span>
						</li>
					</a>
				</ul>
			</li>
			
			<li class="navlist-expand" id="navlist-expand-inventory">
				<div class='navlist'>
					<img src="<c:url value='/static/images/dollar.png' />" class="navlist-icon">
					<span>
						<span>Inventaire</span>
						<i class='navlink-arrow navlink-arrow-right'></i>
					</span>
				</div>	
				<ul style='display:none' id="navlist-inventory">
					
					<a href="/laderaille/invoice/addProduct">
						<li class="navlink">
							<span>Nouveau Produit</span>
						</li>
					</a>
					
					<a href="/laderaille/invoice/categoryList">
						<li class="navlink">
							<span>Categories</span>
						</li>
					</a>
					
					
					<a href="/laderaille/invoice/inventory">
						<li class="navlink">
							<span>Liste des Produits</span>
						</li>
					</a>
					
					
					<a href="/laderaille/in/importexport">
						<li class="navlink">
							<span>Import/Export</span>
						</li>
					</a>
				</ul>
			</li>
			
			<li class="navlist-expand" id="navlist-expand-admin">
				<div class='navlist'>
					<img src="<c:url value='/static/images/admin.png' />" class="navlist-icon">
					<span>
						<span>Administration</span>
						<i class='navlink-arrow navlink-arrow-right'></i>
					</span>
				</div>	
				<ul style='display:none' id="navlist-admin">					
					<a href="/laderaille/workHours">
						<li class="navlink">
							<span>Heures de travail</span>
						</li>
					</a>					
					<a href="/laderaille/mailForm">
						<li class="navlink">
							<span>Courriels</span>
						</li>
					</a>
					<a href="/laderaille/closingCash/cashStatistics">
						<li class="navlink">
							<span>Statistiques caisse</span>
						</li>
					</a>	
					
					<a href="/laderaille/clear/all/">
						<li class="navlink">
							<span>Vider la base de donnees</span>
						</li>
					</a>
				</ul>
			</li>
		</ul>
	</div>
</div>
<div id="topbar-wrapper">
	<div id="topbar-content">
		<div id="disconnect" class="topbar-cell">
			<a href="/laderaille/logout"><img src="<c:url value='/static/images/logout.png' />"></a>
		</div>
		<div id="user" class="topbar-cell">
			<div id="username">${LoggedInUser}</div>
			<div id="userrole">${LoggedInUserRole}</div>
		</div>
	</div>
</div>