<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "events";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/addEvent.js' />"></script>
	<script src="<c:url value='/static/js/bootstrap.min.js' />"></script>
	<script src="<c:url value='/static/js/bootstrap-datetimepicker.min.js' />"></script>
	
 	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/registration.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/addEvent.css' />">
	
	<link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/static/css/bootstrap-datetimepicker.min.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/bootstrap-combined.min.css' />" rel="stylesheet">
	
	<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    
	
	<title>evenements</title>
</head>
<body>
	<div id="wrapper">
		<%@ include file="navigation.jsp"%>
		<div id="main-wrapper">
			<div id="main-content">
				<form:form method="POST" modelAttribute="event">
					<form:input type="hidden" path="id" id="id"/>
					<c:choose>
						<c:when test="${edit}">
							<h1>Modifier evenements</h1>
						</c:when>
						<c:otherwise>
							<h1>Nouvel evenement</h1>
						</c:otherwise>
					</c:choose>
					
					<%-- <div>
						<!-- <div class="form-label">Nom</div> -->
						<form:input path="name" type="text" placeholder="Name" />
						<div class="has-error">
							<form:errors path="name" class="help-inline" />
						</div>
					</div> --%>
					<div class="input-choice-label">Nom</div>
					<div class="input-choice">
						<form:select path="name" items="${eventName}" multiple="false" itemValue="name" itemLabel="name" />
					</div>

					<div class="input-choice-label">Debut</div>
					<div id="datepickerStart" class="input-append">
						<form:input id="startDate" type="text" path="startDate" placeholder="Date de debut" />
 						<span class="add-on"> <i data-time-icon="icon-time"data-date-icon="icon-calendar"> </i></span>
					</div> 
					<div class="input-choice-label">Fin</div>
					<div id="datepickerEnd" class="input-append">
						<form:input id="endDate" type="text" path="endDate" placeholder="Date de fin" />
						<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"> </i></span>
					</div>  
					<div>
						<div class="input-choice-label">Description</div>
						<form:textarea rows="4" cols="50" path="description" placeholder="Notes" />
						<div class="has-error">
							<form:errors path="description" class="help-inline" />
						</div>
					</div>
					<div>
						<div class="input-choice-label">Benevole responsable</div>
						<div class="input-choice">
							<form:select path="ownerId" items="${members}" multiple="false" itemValue="id" itemLabel="mail" />
						</div>
						<c:choose>
							<c:when test="${edit}">
								<h1 class="input-choice-label">Modifier Participants</h1>
								<%-- <div class="input-choice">
									<form:select path="members" items="${members}" multiple="false" itemValue="id" itemLabel="mail" />
								</div> --%>
								<div class="input-choice">
									<form:select style="display:none;" path="members" items="${members}" multiple="true" itemValue="id" itemLabel="mail"/>
								</div>
								<div>
									<table>
										<thead>
											<tr>
												<th class="align-left">Nom</th>
												<th class="align-right">Courriel</th>
												<th class="align-right"></th>
											</tr>
										</thead>
										<tbody id="selected-members-root">
										</tbody>
									</table>
								</div>
								<br>
								<div id="open-members">Ajouter un Participant</div>
							</c:when>
							<c:otherwise>
								
							</c:otherwise>
						</c:choose>
					</div>
					<div id="form-controls">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Modifier" class="single-input confirm-btn float-left">
						</c:when>
						<c:otherwise>
							<input type="submit" value="Ajouter" class="single-input confirm-btn float-left">
						</c:otherwise>
					</c:choose>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<div class="clear">
						<c:choose>
							<c:when test="${edit}">
								<br>
								<h1>Factures</h1>
								<table>
									<thead>
										<tr>
											<th class="align-left">
												ID
											</th>
											<th class="align-left">
												Client
											</th>
											<th class="align-left">
												Responsable
											</th>
											<th class="align-left">
												Date
											</th>
											<th class="align-right">
												Statut
											</th>
											<th class="align-right">
												Valeur
											</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${invoices}" var="invoice">
										<tr>
											<td>${invoice.getId()}</td>
											<td>${invoice.getBuyerName()}</td>
											<td>${invoice.getSellerName()}</td>
											<td>
												<fmt:formatDate type="date" value="${invoice.getTransactionDate()}" />
											</td>
											<td class="align-right">
												<c:choose>
													<c:when test="${invoice.getInvoiceStatusBoolean()}">Paye</c:when>
													<c:otherwise>Non-paye</c:otherwise>
												</c:choose>
											</td>
											<td class="align-right">
												<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getTotal()}" />
												
											</td>
										</tr>
										</c:forEach>
									</tbody>
								</table>

								<br>
								<table style="display:none;" id="init-participants">
									<thead>
										<tr>
											<td class="align-left">Nom</td>
											<td class="align-right">Mail</td>
											<td class="align-right">Statut</td>
										</tr>
									</thead>
									<tbody id="participants-items-root">
										<c:forEach items="${event.getMembers()}" var="participant">
											<tr id='member-${participant.getId()}'
												data-memberid='${participant.getId()}'>
												<td class='align-left'>${participant.getDisplayName()}</td>
												<td class='align-right'>${participant.getMail()}</td>
												<td class='align-right'><c:choose><c:when test="${participant.getStatus()}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</c:when>
							<c:otherwise>
							</c:otherwise>
						</c:choose>
					</div>
				</form:form>
			</div>
		</div>
		<div id="members-wrapper" style="display: none">
			<div id="members-content">
				<div id="title-row" class="content-row">
					<div class="float-left">
						<h1>Membres</h1>
					</div>
					<div id="close-members" class="confirm-btn float-right">X</div>
				</div>
				<div id="search-row" class="content-row">
					<div class="float-left">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
					<div class="float-left">
						<input type="text" id="member-filter" placeholder="Filtrer les membres">
					</div>
				</div>
				<div id="tables-row" class="content-row">
					<table id="members-items">
						<thead>
							<tr>
								<td class="align-left">ID</td>
								<td class="align-right">Nom</td>
								<td class="align-right">Courriel</td>
								<td class="align-right">Statut</td>
							</tr>
						</thead>
						<tbody id="members-items-root">
							<c:forEach items="${members}" var="member">
								<tr id='member-${member.getId()}'
									data-memberid='${member.getId()}'
									data-lastname='${member.getLastName()}'
									data-firstname='${member.getFirstName()}'
									data-displayname='${member.getDisplayName()}'
									data-status='${member.getStatus()}'
									data-mail='${member.getMail()}'>
									<td class='align-left'>${member.getId()}</td>
									<td class='align-right'>${member.getDisplayName()}</td>
									<td class='align-right'>${member.getMail()}</td>
									<td class='align-right'><c:choose><c:when test="${member.getStatus()}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>