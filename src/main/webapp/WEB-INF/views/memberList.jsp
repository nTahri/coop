<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "members";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/memberList.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/memberList.css' />">
	
	<title>Membres</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<div id="members-content">
				<div id="title-row" class="content-row">
					<div class="float-left"><h1>Membres</h1></div>
				</div>
				<div id="search-row" class="content-row">
					<div class="float-left">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
					<div class="float-left">
						<input type="text" id="filter" placeholder="Filtrer les membres">
					</div>
				</div>
				<div id="tables-row" class="content-row">
					<table id="members-items">
						<thead>
							<tr>
								<td class="align-left">Nom</td>
								<td class="align-right">Mail</td>
								<td class="align-right">Statut</td>
								<th class="member-controls">
								</th>
								<th class="member-controls">
								</th>
							</tr>
						</thead>
						<tbody id="members-items-root">
							<c:forEach items="${members}" var="member">
								<tr id='member-${member.getId()}'
									data-memberid='${member.getId()}'
									data-lastname='${member.getLastName()}'
									data-firstname='${member.getFirstName()}'
									data-displayname='${member.getDisplayName()}'
									data-status='${member.getStatus()}'
									data-mail='${member.getMail()}'>
									<td class='align-left'>${member.getDisplayName()}</td>
									<td class='align-right'>${member.getMail()}</td>
									<td class='align-right'><c:choose><c:when test="${member.getStatus() == true}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
									<td class="member-controls">
										<a href="modifyMember-${member.id}">
											<img class="memberlist-icon" src="<c:url value='/static/images/modify.png' />"></img>
										</a>
									</td>
									<%-- <td class="member-controls">
										<a href="deleteMemberConfirm-${member.id}">
											<img class="memberlist-icon" src="<c:url value='/static/images/delete.png' />"></img>
										</a>
									</td> --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>