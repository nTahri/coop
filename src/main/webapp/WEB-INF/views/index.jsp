<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	
	<title>Index</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<img src="<c:url value='/static/images/laderaillecourtoisie.jpg' />" height="520" width="750">
		</div>
	</div>
</div>
</body>
</html>