<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript">
	var context = "members";
</script>
<script src="<c:url value='/static/js/jquery.min.js' />"></script>
<script src="<c:url value='/static/js/laderaille.js' />"></script>
<script src="<c:url value='/static/js/addMember.js' />"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/laderaille.css' />">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/registration.css' />">

<title>Membres</title>
</head>
<body>
	<div id="wrapper">
		<%@ include file="navigation.jsp"%>
		<div id="main-wrapper">
			<div id="main-content">
				<form:form method="POST" modelAttribute="member">
					<form:input type="hidden" path="id" id="id" />
					<c:choose>
						<c:when test="${edit}">
							<h1>Modifier Membre</h1>
						</c:when>
						<c:otherwise>
							<h1>Nouveau Membre</h1>
						</c:otherwise>
					</c:choose>
					<div>
						<div class="form-label">Prenom</div>
						<form:input class="single-input" path="firstName" type="text"
							placeholder="Prenom" />
						<br>
						<div class="has-error">
							<form:errors path="firstName" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Nom</div>
						<form:input class="single-input" path="lastName" type="text"
							placeholder="Nom" />
						<br>
						<div class="has-error">
							<form:errors path="lastName" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Courriel</div>
						<c:choose>
							<c:when test="${edit}">
								<div class="form-value">${member.mail}</div>
								<input class="single-input" name="mail" type="hidden"
									readonly="readonly"
									value='<c:out value="${member.mail}"></c:out>' />
							</c:when>
							<c:otherwise>
								<form:input class="single-input" path="mail" type="text"
									placeholder="email" />
								<br>
							</c:otherwise>
						</c:choose>
						<div class="has-error">
							<form:errors path="mail" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Mot de passe</div>
						<form:input class="single-input" path="password" type="password"
							placeholder="Mot de Passe" />
						<br>
						<div class="has-error">
							<form:errors path="password" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Addresse</div>
						<form:input class="single-input" path="address" type="text"
							placeholder="Addresse" />
						<br>
						<div class="has-error">
							<form:errors path="address" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Telephone</div>
						<form:input class="single-input" path="phoneNumber" type="number"
							placeholder="Telephone" />
						<br>
						<div class="has-error">
							<form:errors path="phoneNumber" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">ID etudiante</div>
						<form:input class="single-input" path="studentID" type="text"
							placeholder="ID Etudiante" />
						<br>
						<div class="has-error">
							<form:errors path="studentID" class="help-inline" />
						</div>
					</div>

					<div>
						<div class="form-label">Date de Naissance (aaaa-mm-jj)</div>
						<form:input class="single-input" path="birthday" type="date" />
						<br>
						<div class="has-error">
							<form:errors path="birthday" class="help-inline" />
						</div>
					</div>
					<c:choose>
						<c:when test="${edit}">
							<div>
								<div class="form-label">Date de depart (aaaa-mm-jj)</div>
								<form:input class="single-input" path="leftDate" type="date" />
								<br>
								<div class="has-error">
									<form:errors path="leftDate" class="help-inline" />
								</div>
							</div>
						</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
					<div>
						<div class="form-label">Nom Facebook</div>
						<form:input class="single-input" path="facebookName" type="text"
							placeholder="Nom Facebook" />
						<br>
						<div class="has-error">
							<form:errors path="facebookName" class="help-inline" />
						</div>
					</div>

					<form:input path="joiningDate" type="hidden" value="${actualDate}" />
					<br>

					<form:checkbox class="single-input" path="status" />Actif<br>
					<div class="has-error">
						<form:errors path="status" class="help-inline" />
					</div>
					<!-- <form:input class="single-input" path="joiningDate" type="text" placeholder="JJ/MM/AAAA" /><br>-->
					<label class="input-choice-label">Sexe</label>
					<br>
					<div class="input-choice">
						<form:select path="sex">
							<form:option value="M" label="Homme" />
							<form:option value="F" label="Femme" />
							<form:option value="A" label="N/A" />
						</form:select>
						<div class="has-error">
							<form:errors path="sex" class="help-inline" />
						</div>
					</div>
					<br>
					<label class="input-choice-label">R�les</label>
					<br>
					<div class="input-choice">
						<form:select path="memberProfiles" items="${roles}"
							multiple="true" itemValue="id" itemLabel="type" />
						<div class="has-error">
							<form:errors path="memberProfiles" class="help-inline" />
						</div>
					</div>
					<br>
					<div id="form-controls">
						<c:choose>
							<c:when test="${edit}">
								<input type="submit" value="Modifier"
									class="single-input confirm-btn float-left">
							</c:when>
							<c:otherwise>
								<input type="submit" value="Ajouter"
									class="single-input confirm-btn float-left">
							</c:otherwise>
						</c:choose>
					</div>
				</form:form>
			</div>
		</div>
	</div>
</body>
</html>