<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "admin";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Suppression</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/cleanup.js' />"></script>
</head>
<body>
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div>
			<h1>Suppression des transactions</h1>
			<div id="delete-transactions" class="confirm-btn float-left">Supprimer</div> 
		</div>
		<br/>
		<br/>
		<div>
			<h1>Suppression des produits</h1>
		
			<div id="delete-products" class="confirm-btn float-left">Supprimer</div>
		</div>
		<br/>
		<br/>
		<div>
			<h1>Suppression des membres</h1>
		
			<div id="delete-members" class="confirm-btn float-left">Supprimer</div>
		</div>
	</div>
</body>
</html>
