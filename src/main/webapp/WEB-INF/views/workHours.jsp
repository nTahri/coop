<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:h="http://java.sun.com/jsf/html">	
<head>
	<script type="text/javascript">var context = "admin";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">	
	<title>Heures de travail</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/workHours.js' />"></script>
	
	<script type="text/javascript">
		var csrfParameter = '${_csrf.parameterName}';
		var csrfToken = '${_csrf.token}';
	</script>	
</head>
<body>
<div id="wrapper">	
	<%@ include file="navigation.jsp"%>		
	<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
	<div id="main-wrapper">
		<form:form action="workHours.do" method="POST"
		commandName="workHours" >		
		 
			<div id="main-content">
			<h1>Heures de travail</h1>
			<form:input path="id" type="hidden"/>
			<input type="checkbox" id="myCheck" style="visibility:hidden;"/>		
			<form:input type="hidden" id="memberMyMail" path="member.mail" name="mail"/>				 	
				<div id="bill-client-controls">
					<div id="selected-client-label">Recherchez le Membre</div>
					<div id="selected-client-value">${workHours.getMember().getMail()}</div>
					<div id="open-members">
						<img src="<c:url value='/static/images/search.png' />">
					</div>
				</div>			
				<div id="sepecificWorkingTimes">						
					<div id="benevoleWorkingTimes">
						<p>
							<h4>Rentrez la periode de recherche</h4>	
							<tr>
								<td>Date de debut :</td>
								<td><input id="startingDate" type="date" name="startingDate" value="${firstDate}"/></td>
							</tr>
							<tr>
								<td>Date de Fin :</td>
								<td><input id="endingDate" type="date" name="endingDate" value="${secondDate}" /></td>
							</tr>
						</p>
					</div>		
					<div>	
						<input type="button" name="searchByPeriod"  value="Rechercher" onclick="validateWorkHoursForm()" />
						<input style="display: none;" id="submitB" type="submit" name="searchByPeriod" onclick="sendMyMail()"/>
						<div class="float-left" id="open-members">
							<img src="<c:url value='/static/images/search.png' />">
						</div>	
					</div>						   														
				</div>
				
				<div id="members-wrapper" style="display: none">
					<div id="members-content">
						<div id="title-row" class="content-row">	
							<div class="float-left">
								<h1>Membres</h1>
							</div>
							<div id="close-members" class="confirm-btn float-right">X</div>
						</div>
						<div id="search-row" class="content-row">
							<div class="float-left">
								<img src="<c:url value='/static/images/search.png' />">
							</div>
							<div class="float-left">
								<input type="text" id="member-filter" placeholder="Filtrer les membres">
							</div>
						</div>
						<div id="tables-row" class="content-row">
							<table id="members-items">
								<thead>
									<tr>
										<th class="align-left">ID</th>
										<th class="align-right">Nom</th>
										<th class="align-right">Courriel</th>
										<th class="align-right">Statut</th>
									</tr>
								</thead>
								<tbody id="members-items-root">
									<c:forEach items="${members}" var="member">
										<tr id='member-${member.getId()}'
											data-memberid='${member.getId()}'
											data-lastname='${member.getLastName()}'
											data-firstname='${member.getFirstName()}'
											data-displayname='${member.getDisplayName()}'
											data-status='${member.getStatus()}'
											data-mail='${member.getMail()}'>
											<td class='align-left'>${member.getId()}</td>
											<td class='align-right'>${member.getDisplayName()}</td>
											<td class='align-right'>${member.getMail()}</td>
											<td class='align-right'><c:choose><c:when test="${member.getStatus()}">Actif</c:when><c:otherwise>Inactif</c:otherwise></c:choose></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>				
			</div>
		</form:form>
		
		<table>
			<thead>
				<tr>
					<th class="align-left">Date de debut</th>
					<th class="align-left">Date de fin</th>
					<th class="align-right">Heures</th>
				</tr>
			</thead>
			<tbody>
				<c:set var="total" value="${0}"/>
				<c:forEach items="${SearchworkingTimesList}" var="workingTimes">
					<tr>
						<td class="align-left"><fmt:formatDate value="${workingTimes.startDateTime}" pattern='dd/MM/yyyy @ HH:mm' /></td>
						<td class="align-left"><fmt:formatDate value="${workingTimes.endDateTime}" pattern='@ HH:mm' /></td>				
						<td class="align-right">${workingTimes.endDateTime.getTime()/3600000 - workingTimes.startDateTime.getTime()/3600000}</td>
						<c:set var="total" value="${total + workingTimes.endDateTime.getTime()/3600000 - workingTimes.startDateTime.getTime()/3600000}" />
					</tr>
				</c:forEach>	
			</tbody>
			<tfoot>
				<tr>	
					<td class="align-left"></td>		
					<td class="align-right"><b>Total times</b></td>
					<td class="align-right">${total}</td>
				</tr>
			</tfoot>
		</table>
	</div>
	</div>
</body>
</html>
