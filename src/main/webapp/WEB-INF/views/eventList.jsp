<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "events";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/eventList.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/eventList.css' />">
	
	<title>evenements</title>
</head>
<body>
<div id="wrapper">
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
			<div id="event-content">
				<div id="title-row" class="content-row">
					<div class="float-left"><h1>evenements</h1></div>
				</div>
				<div id="search-row" class="content-row">
					<div class="float-left"><img src="<c:url value='/static/images/search.png' />"></div>
					<div class="float-left"><input type="text" id="filter" placeholder="Filtrer les evenements"></div>
				</div>
				<div id="tables-row" class="content-row">
					<table>
						<thead>
							<tr>
								<th class="align-left">
									ID
								</th>
								<th class="align-left">
									Nom
								</th>
								<th class="align-right">
									Debut
								</th>
								<th class="align-right">
									Fin
								</th>
								<th class="billlist-controls">
								</th>
								<th class="billlist-controls">
								</th>
							</tr>
						</thead>
						<tbody id="events-items-root">
							<c:forEach items="${events}" var="event">
							<tr id='event-${event.getId()}'
								data-eventid='${event.getId()}'
								data-name='${event.getName()}'
								data-startDate='${event.getStartDate()}'
								data-endDate='${event.getEndDate()}'
								data-owner='${member.getOwnerId()}'>
								<td class="align-left">${event.getId()}</td>
								<td class="align-left">${event.getName()}</td>
								<td class="align-right">
									<fmt:formatDate type="both" value="${event.getStartDate()}" />
								</td>
								<td class="align-right">
									<fmt:formatDate type="both" value="${event.getEndDate()}" />
								</td>
								<td class="billlist-controls">
									<c:choose>
										<c:when test="${event.canBeDeleted()}">
											<a href="modifyEvent-${event.getId()}">
												<img class="billlist-icon" src="<c:url value='/static/images/modify.png' />"></img>
											</a>
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>
								</td>
								<td class="billlist-controls">
									<c:choose>
										<c:when test="${event.canBeDeleted()}">
											<a href="deleteEventConfirm-${event.getId()}">
												<img class="billlist-icon" src="<c:url value='/static/images/delete.png' />"></img>
											</a>
										</c:when>
										<c:otherwise>
											
										</c:otherwise>
									</c:choose>
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>