<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
	<script type="text/javascript">var context = "invoices";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<script src="<c:url value='/static/js/invoice.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	
	<title>Liste de Factures</title>
</head>
<body>
<div id="wrapper">
<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
	<c:set var="control" value="${1}"/>
	<input var="control" type="number" value="${0}" id="contNum" style="display: none;"/>
	<form:form method="POST" action="openInvoiceList.do" commandName="openInvoiceList">	

		<div id="main-content">
			<div id="searchDate">
						<p>
				        	Selectionnez une periode de recherche
				        </p>
				        <p>
							Debut: <input id="searchDate"  type="date" value ="${startDate}" name = "startDate"/>
							Fin: <input id="searchDate" type="date" value ="${endDate}" name = "endDate" />
				      	</p>
			</div>	    
					
		   <input type="submit" name="searchm" value="Rechercher"/>	
	       <input type="submit" name="exporter" value="Exporter"/>	
	       <div class="float-left">
				<img src="<c:url value='/static/images/search.png' />">
		   </div>
			 
  </form:form>           	
			<h1>Factures ouvertes</h1>
			<table>
				<thead>
					<tr>
						<th class="align-left">
							ID
						</th>
						<th class="align-left">
							Client
						</th>
						<th class="align-left">
							Responsable
						</th>
						<th class="align-left">
							Date
						</th>
						<th class="align-right">
							Statut
						</th>
						<th class="align-right">
							Valeur
						</th>
						<th class="billlist-controls">
						</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${invoices}" var="invoice">
					<tr>
						<td>${invoice.getId()}</td>
						<td>${invoice.getBuyerName()}</td>
						<td>${invoice.getSellerName()}</td>
						<td>
							<fmt:formatDate type="date" value="${invoice.getTransactionDate()}" />
						</td>
						<td class="align-right">
							<c:choose>
								<c:when test="${invoice.getInvoiceStatus() == 1}">Paye</c:when>
                                <c:when test="${invoice.getInvoiceStatus() == 0}">Non-paye</c:when>
                                <c:when test="${invoice.getInvoiceStatus() == 2}">Remboursement</c:when>
                                <c:otherwise>Non-paye</c:otherwise>
							</c:choose>
						</td>
						<td class="align-right">
							<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${invoice.getTotal()}" />$
						</td>
						<td class="billlist-controls">						
						</td>
					</tr>
					</c:forEach>
					
				</tbody>
			</table>
		</div>	
</div>

</body>
</html>