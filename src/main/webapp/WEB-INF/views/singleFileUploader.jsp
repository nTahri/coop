<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>

<head>
	<script type="text/javascript">var context = "inventory";</script>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/registration.css' />">
	
	<title>La Deraille</title>
</head>
<body> 
	<div >
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<div id="main-content">
		<h1>Importer fichier excel pour remplir la base de donnees</h1>
		<form:form method="POST" modelAttribute="fileBucket" enctype="multipart/form-data" class="form-horizontal">
		
			<div class="row">
				<div class="form-group col-md-12">
					
					<div class="col-md-7">
						<form:input type="file" path="file" id="file" class="form-control input-sm"/>
						<div class="has-error">
							<form:errors path="file" class="help-inline"/>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="form-actions floatRight">
					<input type="submit" value="Importer" class="export-btn float-left" >   
				</div>
				<p>&nbsp;</p>
			</div>
		</form:form>
		
		<p>&nbsp;</p>
		</div>
		<h1>Exporter le fichier excel de la base de donnees</h1>
		<div id="export-div">
			<a id="export-btn" class="export-btn float-left" href="/laderaille/export/excel" style="text-decoration: none !important;"> 
				<span>Excel</span>
			</a>
		</div>
	</div>
	
	</div>
</body>
</html>
