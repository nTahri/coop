<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script type="text/javascript">var context = "admin";</script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Envoyer un courriel</title>
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/laderaille.css' />">
	<link rel="stylesheet" type="text/css" href="<c:url value='/static/css/invoice.css' />">
	<link href="<c:url value='/static/css/cashClosingRepport.css' />" rel="stylesheet"></link>
	<script src="<c:url value='/static/js/jquery.min.js' />"></script>
	<script src="<c:url value='/static/js/laderaille.js' />"></script>
</head>
<body>
	<%@ include file="navigation.jsp" %>
	<div id="main-wrapper">
		<form:form method="POST" commandName="sendMail">
			<h1>Envoyer un courriel</h1>
			<div>
				<div class="label">Destinataire</div>
				<input class="single-input" type="text" value ="${mailTo}" name="mailTo" size="65" /><br>
			</div>
			<div>
				<div class="label">Sujet</div>
				<input class="single-input" type="text" value ="${subject}" name="subject" size="65" /><br>
			</div>
			<div>
				<div class="label">Message</div>
				<br>
				<textarea cols="50" rows="10" value ="${message}" name="message" ></textarea><br>
			</div>
			<!--<div>
				<div class="label">Pi�ces jointes</div>
				<input type="file" name="attachFile" size="88" /><input class="controls-btn" type="submit" value="Ajouter une pi�ce jointe" name="addAttach" /><br>
			</div> -->
			<div>
				<input class="controls-btn" type="submit" value="Envoyer � Tous Les membres" name="sendtoall" />
				<input class="controls-btn" type="submit" value="Envoyer" name="sendtosome" />
				<div id="export-div">
				<a id="export-btn" class="export-btn float-left"
					href="/laderaille/export/mailingList"
					style="text-decoration: none !important;"> <span>Exporter la liste des courriels</span>
				</a>
			
			<div>
				<ol>
				    <c:forEach items="${listValue}" var="item">
					<li><b>${item}</b></li>
					</c:forEach>
				</ol>
			</div>
		</form:form>	
	</div>
</body>
</html>
