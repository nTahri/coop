<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
	var context = "events";
</script>
<title>Rapport de Fermeture de Caisse</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/laderaille.css' />">
<link rel="stylesheet" type="text/css"
	href="<c:url value='/static/css/invoice.css' />">
<link href="<c:url value='/static/css/cashClosingRepport.css' />"
	rel="stylesheet"></link>
<script src="<c:url value='/static/js/jquery.min.js' />"></script>
<script src="<c:url value='/static/js/laderaille.js' />"></script>
<script src="<c:url value='/static/js/cashClosingReport.js' />"></script>
</head>
<body>
	<%@ include file="navigation.jsp"%>

	<div id="main-wrapper">
		<h1>Rapport de Fermeture de Caisse</h1>
		<form:form action="cashClosingRepport.do" method="POST"
			commandName="cashClosingRepport">
			<form:input id="commentaire" path="closingComments"
				style="display: none;" />
			<form:input id="closingDifference" path="closingDifference"
				style="display: none;" />
			<div>
				<div class="label">Date du rapport de caisse</div>
				<input type="date" name="searchDate" value="${currentDate}"
					readonly="true" /><br>
			</div>

			<c:if test="${closingCashExist == false}">
				<div>
					<div class="label">Montant � l'ouverture</div>
					<div class="value">
						<fmt:formatNumber type="number" minFractionDigits="2"
							maxFractionDigits="2" value="${lastClosingCashAmount}" />
						$
					</div>
					<br>

					<div class="label">Montant des factures fermees</div>
					<div class="value">
						<fmt:formatNumber type="number" minFractionDigits="2"
							maxFractionDigits="2" value="${closeInvoiceAmount}" />
						$
					</div>
					<br> <font color="#008000">
						<div class="label">Montant espere</div>
						<div class="value">
							<fmt:formatNumber type="number" minFractionDigits="2"
								maxFractionDigits="2"
								value="${closeInvoiceAmount + lastClosingCashAmount}" />
							$
						</div>
						<br>
					</font>
				</div>
			</c:if>
			<c:if test="${closingCashExist == true}">
				<div class="label" style="color: #228B22;">La caisse a ete
					fermee, le tableau contient le decompte correspondant.</div>
			</c:if>
			<p>
			<h3 style="color: #FF0000;">${errorAnonymousUserMessage}</h3>
			</p>
			<input type="checkbox" id="myCheck" style="display: none;" />
			<table id="sum_table">
				<thead>
					<tr>
						<th class="align-left">Unite</th>
						<th class="align-left">Decompte</th>
						<th class="align-right">Montant</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-left">5�</td>
						<td class="align-left"><form:input id="fiveCent"
								onclick='removeZeroFunction("fiveCent")'
								onfocusout='unitTypeFunction("fiveCent")'
								onkeyup='unitFunction("fiveCent", 1, 0.05),functionTotaldollars()'
								path="fiveCent" /></td>
						<td class="rowDataSd align-right"><fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.fiveCent*0.05}" />$</td>
					</tr>
					<tr>
						<td class="align-left">10�</td>
						<td class="align-left"><form:input id="tenCent"
								onclick='removeZeroFunction("tenCent")'
								onfocusout='unitTypeFunction("tenCent")'
								onkeyup='unitFunction("tenCent", 2, 0.1), functionTotaldollars()'
								path="tenCent" /></td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.tenCent*0.1}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">25�</td>
						<td class="align-left"><form:input id="twentyFiveCent"
								onclick='removeZeroFunction("twentyFiveCent")'
								onkeyup='unitFunction("twentyFiveCent", 3, 0.25), functionTotaldollars()'
								onfocusout='unitTypeFunction("twentyFiveCent")'
								path="twentyFiveCent" /></td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.twentyFiveCent*0.25}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">1$</td>
						<td class="align-left"><form:input id="oneDollars"
								onclick='removeZeroFunction("oneDollars")'
								onkeyup='unitFunction("oneDollars", 4, 1), functionTotaldollars()'
								onfocusout='unitTypeFunction("oneDollars")' path="oneDollars" />
						</td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.oneDollars*1.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">2$</td>
						<td class="align-left"><form:input id="twoDollars"
								onclick='removeZeroFunction("twoDollars")'
								onkeyup='unitFunction("twoDollars", 5, 2), functionTotaldollars()'
								onfocusout='unitTypeFunction("twoDollars")' path="twoDollars" />
						</td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.twoDollars*2.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">5$</td>
						<td class="align-left"><form:input id="fiveDollars"
								onclick='removeZeroFunction("fiveDollars")'
								onkeyup='unitFunction("fiveDollars", 6, 5), functionTotaldollars()'
								onfocusout='unitTypeFunction("fiveDollars")' path="fiveDollars" />
						</td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.fiveDollars*5.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">10$</td>
						<td class="align-left"><form:input id="tenDollars"
								onclick='removeZeroFunction("tenDollars")'
								onkeyup='unitFunction("tenDollars", 7, 10), functionTotaldollars()'
								onfocusout='unitTypeFunction("tenDollars")' path="tenDollars" />
						</td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.tenDollars*10.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">20$</td>
						<td class="align-left"><form:input id="twentyDollars"
								onclick='removeZeroFunction("twentyDollars")'
								onkeyup='unitFunction("twentyDollars", 8, 20), functionTotaldollars()'
								onfocusout='unitTypeFunction("twentyDollars")'
								path="twentyDollars" /></td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.twentyDollars*20.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">50$</td>
						<td class="align-left"><form:input id="fiftyDollars"
								onclick='removeZeroFunction("fiftyDollars")'
								onkeyup='unitFunction("fiftyDollars", 9, 50), functionTotaldollars()'
								onfocusout='unitTypeFunction("fiftyDollars")'
								path="fiftyDollars" /></td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.fiftyDollars*50.0}" />$
						</td>
					</tr>
					<tr>
						<td class="align-left">100$</td>
						<td class="align-left"><form:input id="hundredDollars"
								onclick='removeZeroFunction("hundredDollars")'
								onkeyup='unitFunction("hundredDollars", 10, 100), functionTotaldollars()'
								onfocusout='unitTypeFunction("hundredDollars")'
								path="hundredDollars" /></td>
						<td class="rowDataSd align-right">
							<fmt:formatNumber
								type="number" minFractionDigits="2" maxFractionDigits="2"
								value="${cashClosingRepport.hundredDollars*100.0}" />$
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td class="align-right"><b>Montant Total</b></td>
						<td class="align-right"><form:input id="somme" path="total"
								readonly="true" /></td>
					</tr>
				</tfoot>
			</table>
			<c:if test="${closingCashExist == false}">
				<div>
					<input class="controls-btn" type="button"
						value="Enregistrer ce decompte"
						onclick="confirmationFunction('${lastClosingCashAmount + closeInvoiceAmount}', '${closingCashExist}')" />
					<input style="display: none;" class="controls-btn" id="saveCash"
						type="submit" name="save" value="Soumettre" />
				</div>
			</c:if>
		</form:form>
	</div>
</body>
</html>