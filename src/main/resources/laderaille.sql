-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: laderaille
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `PARENTID` int(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CATEGORIES_NAME_INX` (`NAME`),
  KEY `CATEGORIES_FK_1` (`PARENTID`),
  CONSTRAINT `CATEGORIES_FK_1` FOREIGN KEY (`PARENTID`) REFERENCES `categories` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (14,'adaptateur PAS',NULL),(15,'axe de moyeu',NULL),(16,'axes de freins avant',NULL),(17,'bottom bracket',NULL),(18,'Bras de frein',NULL),(19,'Câble cantilevier',NULL),(20,'câble frein',NULL),(21,'câble vitesse',NULL),(22,'cadenas',NULL),(23,'cage de roulement à billes',NULL),(24,'cale-pied',NULL),(25,'Cassettes',NULL),(26,'Chaînes',NULL),(27,'Chambre à air',NULL),(28,'coude de frein',NULL),(29,'Derailleur arrière',NULL),(30,'frein',NULL),(31,'guidoline',NULL),(32,'huile',NULL),(33,'kit de reparation pneus',NULL),(34,'kit de rustines',NULL),(35,'Leviers à pneus',NULL),(36,'liquide à suspension',NULL),(37,'lumière',NULL),(38,'maillon rapide',NULL),(39,'Manettes de freins',NULL),(40,'Manettes de vitesses',NULL),(41,'Manettes de vitesses et freins',NULL),(42,'patins de freins',NULL),(43,'Pedales',NULL),(44,'Pedalier ',NULL),(45,'poignees',NULL),(46,'pompe',NULL),(47,'porte bagage ',NULL),(48,'reflecteurs',NULL),(49,'Roue libre',NULL),(50,'sac de selle',NULL),(51,'Selle',NULL),(52,'sonette',NULL),(53,'speedomètre',NULL),(54,'support à gourde',NULL),(55,'tige de selle',NULL),(56,'1-MemberShip',NULL),(57,'Les macabos',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `closedcash`
--

DROP TABLE IF EXISTS `closedcash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `closedcash` (
  `MONEY` varchar(255) NOT NULL,
  `HOSTSEQUENCE` int(11) NOT NULL,
  `DATESTART` datetime NOT NULL,
  `DATEEND` datetime DEFAULT NULL,
  `SUBTOTAL` double NOT NULL DEFAULT '0',
  `TPS` double NOT NULL DEFAULT '0',
  `TVQ` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`MONEY`),
  UNIQUE KEY `CLOSEDCASH_INX_SEQ` (`HOSTSEQUENCE`),
  KEY `CLOSEDCASH_INX_1` (`DATESTART`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `closedcash`
--

LOCK TABLES `closedcash` WRITE;
/*!40000 ALTER TABLE `closedcash` DISABLE KEYS */;
/*!40000 ALTER TABLE `closedcash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `closingcash`
--

DROP TABLE IF EXISTS `closingcash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `closingcash` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MEMBERID` int(11) NOT NULL,
  `CLOSINDGDATE` date NOT NULL,
  `FIVECENT` int(11) NOT NULL,
  `TENCENT` int(11) DEFAULT NULL,
  `TWENTYFIVECENT` int(11) DEFAULT NULL,
  `ONEDOLLARS` int(11) DEFAULT NULL,
  `TWODOLLARS` int(11) DEFAULT NULL,
  `FIVEDOLLARS` int(11) DEFAULT NULL,
  `TENDOLLARS` int(11) DEFAULT NULL,
  `TWENTYDOLLARS` int(11) DEFAULT NULL,
  `FIFTYDOLLARS` int(11) DEFAULT NULL,
  `HUNDREDDOLLARS` int(11) DEFAULT NULL,
  `TOTAL` double NOT NULL,
  `OPENDIFFERENCE` double DEFAULT NULL,
  `CLOSINGDIFFERENCE` double DEFAULT NULL,
  `CLOSINGCOMMENTS` varchar(200) DEFAULT NULL,
  `OPENCOMMENTS` varchar(200) DEFAULT NULL,
  `OPENCASHMEMBER` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`,`FIVECENT`),
  KEY `MEMBERID_FK_MEMBER_idx` (`MEMBERID`),
  KEY `OPENCASHMEMBER_FK_MEMBER_idx` (`OPENCASHMEMBER`),
  CONSTRAINT `MEMBERID_FK_MEMBER` FOREIGN KEY (`MEMBERID`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `OPENCASHMEMBER_FK_MEMBER` FOREIGN KEY (`OPENCASHMEMBER`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `closingcash`
--

LOCK TABLES `closingcash` WRITE;
/*!40000 ALTER TABLE `closingcash` DISABLE KEYS */;
INSERT INTO `closingcash` VALUES (40,9,'2016-12-02',89,0,0,0,45,0,0,45,0,0,994.45,-9802.25,10005,'le decopmte de test.','Merci des correction',9);
/*!40000 ALTER TABLE `closingcash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_name`
--

DROP TABLE IF EXISTS `event_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_name` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_name`
--

LOCK TABLES `event_name` WRITE;
/*!40000 ALTER TABLE `event_name` DISABLE KEYS */;
INSERT INTO `event_name` VALUES (1,'Atelier libre service'),(2,'Formations'),(3,'Promotions'),(4,'Autres');
/*!40000 ALTER TABLE `event_name` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `STARTDATE` timestamp NULL DEFAULT NULL,
  `ENDDATE` timestamp NULL DEFAULT NULL,
  `DESCRIPTION` varchar(500) DEFAULT NULL,
  `NAME` varchar(50) DEFAULT NULL,
  `OWNER` int(11) NOT NULL,
  `eventscol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_name_starDate` (`NAME`,`STARTDATE`),
  KEY `OWNER_FK_MEMBERID_idx` (`OWNER`),
  CONSTRAINT `OWNER_FK_MEMBERID` FOREIGN KEY (`OWNER`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (23,'2016-11-09 03:12:00','2016-11-09 04:13:00','description1','Autres',14,NULL),(26,'2016-11-11 18:00:00','2016-11-13 02:56:00','rien interessant','Promotions',14,NULL),(29,'2016-11-30 03:51:43','2016-11-19 03:11:00','dqwdwqdwqdwqdw','Event Test',14,NULL),(31,'2016-11-18 17:45:21','2016-11-18 17:45:21','wewe','Atelier libre service',14,NULL),(32,'2016-11-22 17:17:09','2016-12-01 17:12:09','ssssss','Event Test',14,NULL),(33,'2016-11-27 13:00:00','2016-11-27 23:00:00','evenement sportif','Promotions',14,NULL),(34,'2016-11-29 13:00:00','2016-11-29 23:00:00','filling good','Formations',14,NULL),(40,'2016-11-28 13:00:00','2016-11-28 23:00:00','hhhhh','Atelier libre service',14,NULL),(41,'2016-11-30 13:00:00','2016-12-01 01:00:00','notes','Atelier libre service',14,NULL),(42,'2016-12-07 02:22:58','2016-12-07 03:22:58','','Atelier libre service',14,NULL),(43,'2016-12-03 02:23:21','2016-12-03 04:23:21','','Formations',14,NULL);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_has_member`
--

DROP TABLE IF EXISTS `events_has_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_has_member` (
  `events_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  PRIMARY KEY (`events_id`,`member_id`),
  KEY `fk_events_has_member_member1_idx` (`member_id`),
  KEY `fk_events_has_member_events1_idx` (`events_id`),
  CONSTRAINT `fk_events_has_member_events1` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_events_has_member_member1` FOREIGN KEY (`member_id`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_has_member`
--

LOCK TABLES `events_has_member` WRITE;
/*!40000 ALTER TABLE `events_has_member` DISABLE KEYS */;
INSERT INTO `events_has_member` VALUES (41,9),(41,10),(41,14),(41,21),(41,25),(41,26);
/*!40000 ALTER TABLE `events_has_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STATUS` int(11) NOT NULL DEFAULT '0',
  `SELLER` varchar(50) NOT NULL DEFAULT 'Anonymous',
  `BUYER` varchar(50) NOT NULL DEFAULT 'Anonymous',
  `TRANSACTIONDATE` date NOT NULL,
  `PAIEMENTDATE` date DEFAULT NULL,
  `SUBTOTAL` double NOT NULL DEFAULT '0',
  `TPS` double NOT NULL DEFAULT '0',
  `TVQ` double NOT NULL DEFAULT '0',
  `EVENTID` int(11) NOT NULL,
  `TOTAL` double DEFAULT '0',
  PRIMARY KEY (`ID`,`EVENTID`),
  KEY `fk_tickets_Member1_idx` (`SELLER`),
  KEY `IDBUYER_idx` (`BUYER`),
  KEY `fk_invoice_events1_idx` (`EVENTID`),
  CONSTRAINT `EVENT_FK_EVENTID` FOREIGN KEY (`EVENTID`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (9,1,'admin@laderaille.ca','admin@laderaille.ca','2016-10-27',NULL,16.5,0,0,23,18.970875),(10,1,'admin@laderaille.ca','admin@laderaille.ca','2016-11-11','2016-11-14',16.5,0,0,23,18.970875),(11,1,'admin@laderaille.ca','admin@laderaille.ca','2016-11-14','2016-11-14',16.5,0,0,23,18.970875),(12,0,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-11','2016-11-11',12,0,0,23,13.797),(13,1,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-14',NULL,12,0,0,23,13.797),(14,0,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-11',NULL,12,0,0,23,13.797),(15,1,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-11',NULL,12,0,0,23,13.797),(16,1,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-11','2016-11-14',12,0,0,23,13.797),(17,0,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-14',NULL,175,0,0,26,201.20625),(18,0,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-14','2016-11-14',10,0,0,26,11.4975),(19,1,'jeanrostandl82@gmail.com','jeanrostandl82@gmail.com','2016-11-15','2016-11-15',30,0,0,23,34.4925),(20,1,'jeanrostandl82@gmail.com','soumrin@gmail.com','2016-11-21','2016-11-21',96,0,0,23,110),(21,1,'jeanrostandl82@gmail.com','soumrin@gmail.com','2016-11-21','2016-11-21',176,0,0,23,202),(22,1,'jeanrostandl82@gmail.com','jacques@gmail.com','2016-11-22','2016-11-22',0,0,0,31,0),(23,0,'jeanrostandl82@gmail.com','lepage@gmail.com','2016-11-22',NULL,10,0,0,26,10.2995),(24,0,'jeanrostandl82@gmail.com','ntahri@gmail.com','2016-11-22',NULL,137,0,0,29,144.03825),(25,1,'jeanrostandl82@gmail.com','pierre@gmail.com','2016-11-22','2016-11-22',137,0,0,32,144.03825),(26,1,'jeanrostandl82@gmail.com','yezsab@yahoo.fr','2016-11-27','2016-11-27',131,0,0,32,138.03825),(27,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',50,0,0,31,50),(28,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',10,0,0,32,10),(29,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',10,0,0,29,10),(30,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',10,0,0,32,10),(31,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',30,0,0,29,30),(32,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',30,0,0,26,30),(33,1,'ntahri@gmail.com','ntahri@gmail.com','2016-12-02','2016-12-02',10,0,0,29,10),(34,1,'ntahri@gmail.com','yezsab@yahoo.fr','2016-12-02','2016-12-02',10,0,0,23,10);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoiceline`
--

DROP TABLE IF EXISTS `invoiceline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoiceline` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `INVOICEID` int(11) NOT NULL,
  `PRODUCTID` varchar(55) DEFAULT NULL,
  `QUANTITY` double NOT NULL,
  `PRICE` double NOT NULL,
  `TVQ` double NOT NULL DEFAULT '0',
  `TSQ` double DEFAULT NULL,
  `DISCOUNT_VALUE` double DEFAULT '0',
  `DISCOUNT_TYPE` varchar(45) DEFAULT '%',
  PRIMARY KEY (`ID`,`INVOICEID`),
  KEY `TICKETlINES_FK_2` (`PRODUCTID`),
  KEY `INVOICEID_FK_INVOICEID_idx` (`INVOICEID`),
  CONSTRAINT `INVOICEID_FK_INVOICEID` FOREIGN KEY (`INVOICEID`) REFERENCES `invoice` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `PRODUCTID_FK_PRODUCTCODE` FOREIGN KEY (`PRODUCTID`) REFERENCES `products` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoiceline`
--

LOCK TABLES `invoiceline` WRITE;
/*!40000 ALTER TABLE `invoiceline` DISABLE KEYS */;
INSERT INTO `invoiceline` VALUES (1,22,'1-06',1,0,0,0,0,'%'),(2,23,'38855',1,0,0,0,0,'%'),(3,23,'74425',5,2,0.1995,0.1,0,'%'),(4,24,'74425',1,2,0.1995,0.1,0,'%'),(5,24,'54550',3,45,4.4887500000000005,2.25,0,'%'),(6,24,'32010',3,0,0,0,0,'%'),(7,25,'74425',1,2,0.1995,0.1,0,'%'),(8,25,'54550',3,45,4.4887500000000005,2.25,0,'%'),(9,26,'74425',1,2,0.1995,0.1,6,'$'),(10,26,'54550',3,45,4.4887500000000005,2.25,0,'%'),(11,27,'AN01',1,50,0,0,0,'%'),(12,28,'MEN01',1,10,0,0,0,'%'),(13,29,'MEN01',1,10,0,0,0,'%'),(14,30,'MEN01',1,10,0,0,0,'%'),(15,31,'MEN01',1,10,0,0,0,'%'),(16,31,'MEN01',1,10,0,0,0,'%'),(17,31,'MEN01',1,10,0,0,0,'%'),(18,32,'MEN01',1,10,0,0,0,'%'),(19,32,'MEN01',1,10,0,0,0,'%'),(20,32,'MEN01',1,10,0,0,0,'%'),(21,33,'MEN01',1,10,0,0,0,'%'),(22,34,'MEN01',1,10,0,0,0,'%');
/*!40000 ALTER TABLE `invoiceline` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(50) DEFAULT NULL,
  `LASTNAME` varchar(50) DEFAULT NULL,
  `ADDRESS` varchar(100) DEFAULT NULL,
  `MAIL` varchar(50) DEFAULT NULL,
  `SEX` varchar(1) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `PHONE` varchar(50) DEFAULT NULL,
  `JOININGDATE` timestamp NULL DEFAULT NULL,
  `FACEBOOKNAME` varchar(50) DEFAULT NULL,
  `STATUS` tinyint(1) DEFAULT NULL,
  `STUDENTID` varchar(50) DEFAULT NULL,
  `LEFTDATE` timestamp NULL DEFAULT NULL,
  `BIRTHDAY` timestamp NULL DEFAULT NULL,
  `EXPIRINGDATE` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Email_UNIQUE` (`MAIL`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (9,'jean','Rostand','montreal est canada','jeanrostandl82@gmail.com','M','$2a$10$cUVXywzvYE29KDyTZ.vlAuDdaf7ZuzghI1Vg.PSImDnANY9wX7jpy','1234569632','2016-09-07 11:22:17','jeanRostan',1,'',NULL,NULL,NULL),(10,'Nabil','SOUMRI','Montreal quebec canada','soumrin@gmail.com','M','$2a$10$wqSetbrkPJLEjqQOxNFztu9OReFuQVLgS8u8Uj3Nf7AiK2C6A5iU.','465726435','2016-11-07 12:22:17','nebil.soumri',1,'456546','2016-11-18 12:22:17','1974-11-18 12:22:17',NULL),(14,'admin','admin','Chez moi la ou tu veux','admin@laderaille.ca','M','','5145555555','2016-11-07 05:00:00','facebook',1,'',NULL,NULL,NULL),(21,'Nabil','Tahri','2500, boul Edouard Monpetit','ntahri@gmail.com','M','$2a$10$m6LX6VpXE9YSBAGgDW9z8eGgt3uQepZA/YZuLuK6TuYGCPtBW6k/G','','2016-11-20 06:08:45','wewewewee',1,'',NULL,NULL,'2017-05-03 02:27:16'),(22,'Sabri','Yezli','somewhere in montreal EST','yezsab@yahoo.fr','M','$2a$10$J0sdAvu3Fdz/iH.b29Ceu.i80VhI0XbU4ByAKv8aAcw/0H4qB8Llq','4562697896','2016-11-20 06:21:00','sabri',1,'','2016-11-18 12:22:17',NULL,'2017-01-03 03:53:00'),(23,'Hubert','St-Hilaire','4326 Maigret	St-Bruno	J3V 5J5','hubert@gmail.com','M','$2a$10$agk4qRNubmbjWDxLM1Bv/OmnL1xU3DFYViHu28n51Y2ygVU2QH6Ou','4385889963','2016-11-22 22:43:08','hubert',1,'',NULL,NULL,NULL),(24,'emile','Legrand ','513 Olivier	Cowansville	J2K 2X9','legrand@gmail.com','F','$2a$10$MgcuLiygSIjYoSOF8ii7AeoCNB2ZTnj69.MiRnauonQnbIrvmUTOu','7894561423','2016-11-22 22:45:30','emilie',1,'',NULL,NULL,NULL),(25,'Pierre','Tasse ','33 General-Triquet	Blainville	J7C 3Y7','pierre@gmail.com','F','$2a$10$oKVYrORnS8FBowRa5C1J7.oOVqpUHB88YJED8yk8zjsl4x6hJJ/je','7568921235','2016-11-22 22:47:38','pierre',1,'7586',NULL,NULL,NULL),(26,'Jacques','Meunier ','45 10e avenue	Deux-Montagnes	J7R 4W6','jacques@gmail.com','M','$2a$10$w2ArraufGbYxZuTUkP9ATOf.p.c.IGioy3YV8N7wxltRRs44beqKS','7891234562','2016-11-22 22:56:44','meunier',1,'',NULL,NULL,NULL),(27,'Roger','Lepage','1000 Lavigne Montreal H1H,1H1','lepage@gmail.com','F','$2a$10$xl1eXvFVy78GzNcaIFo.pepLZcRgn67NN6hbsrOG5eWmG1L3qsE9.','7894562589','2016-11-22 23:04:30','lepage',1,'7895',NULL,NULL,NULL),(28,'Jean','Dongom','montreL EST','rlemogo@yahoo.fr','M','$2a$10$qqKBJzA2Fsap.dGPpsUTKOVRpafMl1JLIO9vk8xnLMN/7QBXiHaSy','4561238965','2016-11-27 16:03:02','jeanr',1,'',NULL,NULL,NULL);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_member_profile`
--

DROP TABLE IF EXISTS `member_member_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_member_profile` (
  `MEMBER_ID` int(11) NOT NULL,
  `MEMBER_PROFILE_ID` bigint(11) NOT NULL,
  PRIMARY KEY (`MEMBER_ID`,`MEMBER_PROFILE_ID`),
  KEY `fk_Member_has_Member_Profile_Member_Profile1_idx` (`MEMBER_PROFILE_ID`),
  KEY `fk_Member_has_Member_Profile_Member1_idx` (`MEMBER_ID`),
  CONSTRAINT `fk_Member_has_Member_Profile_Member1` FOREIGN KEY (`MEMBER_ID`) REFERENCES `member` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Member_has_Member_Profile_Member_Profile1` FOREIGN KEY (`MEMBER_PROFILE_ID`) REFERENCES `member_profile` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_member_profile`
--

LOCK TABLES `member_member_profile` WRITE;
/*!40000 ALTER TABLE `member_member_profile` DISABLE KEYS */;
INSERT INTO `member_member_profile` VALUES (9,1),(14,1),(21,1),(22,1),(23,2),(24,2),(25,4),(26,3),(27,4),(28,2);
/*!40000 ALTER TABLE `member_member_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_profile`
--

DROP TABLE IF EXISTS `member_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_profile` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_UNIQUE` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_profile`
--

LOCK TABLES `member_profile` WRITE;
/*!40000 ALTER TABLE `member_profile` DISABLE KEYS */;
INSERT INTO `member_profile` VALUES (1,'ADMIN'),(3,'MEMBER'),(4,'STUDENT'),(2,'VOLUNTEER');
/*!40000 ALTER TABLE `member_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `memberfilter`
--

DROP TABLE IF EXISTS `memberfilter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `memberfilter` (
  `ID` int(11) NOT NULL,
  `CRITERIA` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `memberfilter`
--

LOCK TABLES `memberfilter` WRITE;
/*!40000 ALTER TABLE `memberfilter` DISABLE KEYS */;
INSERT INTO `memberfilter` VALUES (0,'Membres qui ont quitte la Coop.'),(2,'Membres selon la periode d\'inscription'),(6,'Tous les membres');
/*!40000 ALTER TABLE `memberfilter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAYMENT` varchar(255) NOT NULL,
  `TOTAL` double NOT NULL,
  `tickets_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`,`tickets_ID`),
  KEY `PAYMENTS_INX_1` (`PAYMENT`),
  KEY `fk_payments_tickets1_idx` (`tickets_ID`),
  CONSTRAINT `fk_payments_tickets1` FOREIGN KEY (`tickets_ID`) REFERENCES `invoice` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persistent_logins`
--

DROP TABLE IF EXISTS `persistent_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persistent_logins` (
  `MAIL` varchar(50) NOT NULL,
  `series` varchar(64) NOT NULL,
  `TOKEN` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`),
  UNIQUE KEY `MAIL_UNIQUE` (`MAIL`),
  UNIQUE KEY `TOKEN_UNIQUE` (`TOKEN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persistent_logins`
--

LOCK TABLES `persistent_logins` WRITE;
/*!40000 ALTER TABLE `persistent_logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `persistent_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REFERENCE` varchar(255) NOT NULL,
  `CODE` varchar(255) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `PRICEBUY` double NOT NULL,
  `PRICESELL` double NOT NULL,
  `CATEGORY` int(50) NOT NULL,
  `TAXCAT` varchar(255) NOT NULL,
  `STOCK` double NOT NULL,
  `DESCRIPTION` varchar(400) DEFAULT NULL,
  `MINSTOCK` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `PRODUCTS_INX_0` (`REFERENCE`),
  UNIQUE KEY `PRODUCTS_INX_1` (`CODE`),
  UNIQUE KEY `PRODUCTS_NAME_INX` (`NAME`),
  KEY `PRODUCTS_FK_1` (`CATEGORY`),
  CONSTRAINT `PRODUCTS_FK_1` FOREIGN KEY (`CATEGORY`) REFERENCES `categories` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=135 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (12,'74425','74425','Kool stop',0,2,14,'STANDARD',0,'DESCRIPTION',2),(13,'54385','54385','arrière, plein 3/8 x 175 mm',23,85,15,'STANDARD',0,'DESCRIPTION',0),(14,'54540','54540','declenche rapide M5 x 126 mm',23,56,15,'STANDARD',0,'DESCRIPTION',0),(15,'54550','54550','declenche rapide M5 x 165 mm',89,45,15,'STANDARD',47,'DESCRIPTION',0),(16,'36126','36126','Alhonga long',0,0,39,'STANDARD',2,'DESCRIPTION',2),(17,'36125','36125','Alhonga court',0,0,39,'STANDARD',2,'DESCRIPTION',2),(18,'52511','52511','neco 113 mm',0,0,17,'STANDARD',1,'DESCRIPTION',2),(19,'38855','38855','V-Brake avant',0,0,18,'STANDARD',1,'DESCRIPTION',2),(20,'38865','38865','V-Brake arrière',0,0,18,'STANDARD',2,'DESCRIPTION',2),(21,'32189','32189','Câble cantilevier',0,0,19,'STANDARD',9,'DESCRIPTION',2),(22,'32010','32010','2 têtes',0,0,20,'STANDARD',6,'DESCRIPTION',2),(23,'32060','32060','câble vitesse 2 têtes',0,0,21,'STANDARD',6,'DESCRIPTION',2),(24,'14070','14070','cadenas à roulette avec câble',0,0,22,'STANDARD',4,'DESCRIPTION',2),(25,'14472','14472','cadenas en U Ming tay MT-5100',0,0,22,'STANDARD',3,'DESCRIPTION',2),(26,'98085','98085','cage de roulement à billes 1/4 X 11',0,0,23,'STANDARD',18,'DESCRIPTION',2),(27,'98100','98100','cage de roulement à billes 3/16 X 9',0,0,23,'STANDARD',20,'DESCRIPTION',2),(28,'98110','98110','cage de roulement à billes 5/32 X 16',0,0,23,'STANDARD',20,'DESCRIPTION',2),(29,'92040','92040','medium avec serrage',0,0,24,'STANDARD',1,'DESCRIPTION',2),(30,'92041','92041','large avec serrage',0,0,24,'STANDARD',1,'DESCRIPTION',2),(31,'38132','38132','11-32T Alivio 9 V',0,0,25,'STANDARD',1,'DESCRIPTION',2),(32,'38141','38141','11 - 32T Altus 8 V',0,0,25,'STANDARD',2,'DESCRIPTION',2),(33,'38148','38148','11-34T deore 10 V',0,0,25,'STANDARD',1,'DESCRIPTION',2),(34,'38150','38150','11-28T HG 7 V',0,0,25,'STANDARD',1,'DESCRIPTION',2),(35,'38157','38157','12-28T tiagra 10 V',0,0,25,'STANDARD',1,'DESCRIPTION',2),(36,'38189','38189','11-28T HG 8 V',0,0,25,'STANDARD',1,'DESCRIPTION',2),(37,'46211','46211','1/2 x 1/8 single speed unidirectionnelle',0,0,26,'STANDARD',1,'DESCRIPTION',2),(38,'46320','46320','7 V Zchain Z51',0,0,26,'STANDARD',2,'DESCRIPTION',2),(39,'46327','46327','1/2 x 1/8 single speed',0,0,26,'STANDARD',4,'DESCRIPTION',2),(40,'46415','46415','8 V Shimano IG -51',0,0,26,'STANDARD',5,'DESCRIPTION',2),(41,'46421','46421','7 et 8 V taya ',0,0,26,'STANDARD',3,'DESCRIPTION',2),(42,'46425','46425','9 V taya ',0,0,26,'STANDARD',4,'DESCRIPTION',2),(43,'46465','46465','5 et 6 V taya ',0,0,26,'STANDARD',6,'DESCRIPTION',2),(44,'77345','77345','700 x 18-25 P',0,5,27,'STANDARD',11,'DESCRIPTION',2),(45,'80335','80335','26 x 2.4-2.75',0,5,27,'STANDARD',1,'DESCRIPTION',2),(46,'80600','80600','12 1/2 x 2 1/4 S',0,5,27,'STANDARD',2,'DESCRIPTION',2),(47,'80602','80602','14 X 1.95-2.125 S',0,5,27,'STANDARD',2,'DESCRIPTION',2),(48,'80604','80604','16 x 1.95-2.125 S',0,5,27,'STANDARD',2,'DESCRIPTION',2),(49,'80606','80606','18 x 1.95-2.125 S',0,5,27,'STANDARD',2,'DESCRIPTION',2),(50,'80608','80608','20 x 1.95-2.125S',0,5,27,'STANDARD',5,'DESCRIPTION',2),(51,'80612','80612','24 x 1.95-2.125S',0,5,27,'STANDARD',4,'DESCRIPTION',2),(52,'80614','80614','26 X 1.25-1.5',0,5,27,'STANDARD',18,'DESCRIPTION',2),(53,'80615','80615','26 x 1.5-1.75 S',0,5,27,'STANDARD',42,'DESCRIPTION',2),(54,'80617','80617','26 x 1.95-2.125 S',0,5,27,'STANDARD',31,'DESCRIPTION',2),(55,'80624','80624','26 x 1 3/8 S',0,5,27,'STANDARD',1,'DESCRIPTION',2),(56,'80625','80625','27 x 1 1/4 S',0,5,27,'STANDARD',9,'DESCRIPTION',2),(57,'80630','80630','718 - 25 S',0,5,27,'STANDARD',4,'DESCRIPTION',2),(58,'80631','80631','700 x 28-35 S',0,5,27,'STANDARD',10,'DESCRIPTION',2),(59,'80632','80632','733-37 S',0,5,27,'STANDARD',8,'DESCRIPTION',2),(60,'80633','80633','700 x 38-45 S',0,5,27,'STANDARD',6,'DESCRIPTION',2),(61,'80635','80635','28 1 1/2 S',0,5,27,'STANDARD',5,'DESCRIPTION',2),(62,'80641','80641','27 x 1.95-2.125 P',0,5,27,'STANDARD',4,'DESCRIPTION',2),(63,'80652','80652','700 x 18-23 P',0,5,27,'STANDARD',1,'DESCRIPTION',2),(64,'160310','160310','26 x 1.9-2.125 S',0,5,27,'STANDARD',1,'DESCRIPTION',2),(65,'2005603','2005603','26X1.5-1.9 S',0,5,27,'STANDARD',1,'DESCRIPTION',2),(66,'4785300','4785300','700 x 23-25 P',0,5,27,'STANDARD',1,'DESCRIPTION',2),(67,'36328','36328','v brake court',0,0,28,'STANDARD',20,'DESCRIPTION',2),(68,'36332','36332','v brake long',0,0,28,'STANDARD',8,'DESCRIPTION',2),(69,'38430','38430','Tourney 7 V chape courte',0,0,29,'STANDARD',1,'DESCRIPTION',2),(70,'36841','36841','tirage lateral insertion par le bas',0,0,30,'STANDARD',1,'DESCRIPTION',2),(71,'36842','36842','tirage lateral insertion par le haut',0,0,30,'STANDARD',1,'DESCRIPTION',2),(72,'72191','72191','blanc',0,0,31,'STANDARD',1,'DESCRIPTION',2),(73,'72192','72192','rouge',0,0,31,'STANDARD',1,'DESCRIPTION',2),(74,'72265','72265','noir gel',0,0,31,'STANDARD',3,'DESCRIPTION',2),(75,'72275','72275','noir ',0,0,31,'STANDARD',1,'DESCRIPTION',2),(76,'1-01','1-01','Bionet plus 125 ml',0,0,32,'STANDARD',14,'DESCRIPTION',2),(77,'43187','43187','Zefal à faire tirer, à donner',0,0,33,'STANDARD',3,'DESCRIPTION',2),(78,'74155','74155','Beto',0,0,33,'STANDARD',4,'DESCRIPTION',2),(79,'6347700','6347700','Park tool - VP 1',0,0,34,'STANDARD',16,'DESCRIPTION',2),(80,'39060','39060','polysport à faire tirer, à donner',0,0,35,'STANDARD',3,'DESCRIPTION',2),(81,'1-02','1-02','paquets de 3 rouge et noir à faire tirer, à donner',0,0,35,'STANDARD',11,'DESCRIPTION',2),(82,'61677','61677','5 WT 1 litre rock shox',0,0,36,'STANDARD',1,'DESCRIPTION',2),(83,'61679','61679','15WT 1 litre rock shox',0,0,36,'STANDARD',1,'DESCRIPTION',2),(84,'12710','12710','Babac arrière blanc 0,5W',0,0,37,'STANDARD',4,'DESCRIPTION',2),(85,'12711','12711','Babac arrière rouge 0,5W',0,0,37,'STANDARD',4,'DESCRIPTION',2),(86,'1-03','1-03','turtle MEC à donner, faire tirer',0,0,37,'STANDARD',2,'DESCRIPTION',2),(87,'46520','46520','5 et 6 V taya sc-25',0,0,38,'STANDARD',3,'DESCRIPTION',2),(88,'46521','46521','5 et 6 V taya sc-23',0,0,38,'STANDARD',4,'DESCRIPTION',2),(89,'46523','46523','1/8 taya sc 33',0,0,38,'STANDARD',4,'DESCRIPTION',2),(90,'38730','38730','3 X 7 V SIS Index',0,0,40,'STANDARD',1,'DESCRIPTION',2),(91,'38754','38754','6 et 7 V',0,0,40,'STANDARD',1,'DESCRIPTION',2),(92,'38737','38737','Revoshift 3 X 6 V ',0,0,40,'STANDARD',1,'DESCRIPTION',2),(93,'38705','38705','rapid fire plus 3 x 8 V',0,5,41,'STANDARD',1,'DESCRIPTION',2),(94,'1-04','1-04','Acera 3 X 8 V',0,5,41,'STANDARD',1,'DESCRIPTION',2),(95,'36311','36311','Alhonga complet',0,0,42,'STANDARD',2,'DESCRIPTION',2),(96,'36315','36315','cantilevier complet ',0,0,42,'STANDARD',5,'DESCRIPTION',2),(97,'36336','36336','v-brake cartouche noir',0,0,42,'STANDARD',4,'DESCRIPTION',2),(98,'36350','36350','v-brake alhonga complet noir',0,0,42,'STANDARD',3,'DESCRIPTION',2),(99,'36353','36353','v-brake complet gris',0,0,42,'STANDARD',7,'DESCRIPTION',2),(100,'92115','92115','Aluminium plate-forme petits 3/8',0,0,43,'STANDARD',1,'DESCRIPTION',2),(101,'92125','92125','Plastique 3/8',0,0,43,'STANDARD',1,'DESCRIPTION',2),(102,'38271','38271','alivio 8 V',0,5,44,'STANDARD',1,'DESCRIPTION',2),(103,'50730','50730','Single speed 45T',0,5,44,'STANDARD',1,'DESCRIPTION',2),(104,'72822','72822','long environ 3 pouces',20,45,45,'STANDARD',0,'DESCRIPTION',0),(105,'72845','72845','enfant',0,0,45,'STANDARD',2,'DESCRIPTION',2),(106,'74043','74043','Beto mini pompe 2-stage',30,56,46,'STANDARD',0,'DESCRIPTION',0),(107,'74048','74048','Beto one stage avec jauge',0,0,46,'STANDARD',6,'DESCRIPTION',2),(108,'1-05','1-05','Filzer mini-zee tuyau rigide',0,0,46,'STANDARD',1,'DESCRIPTION',2),(109,'20011','20011','avant low rider',0,0,47,'STANDARD',1,'DESCRIPTION',2),(110,'20018','20018','avant',0,0,47,'STANDARD',3,'DESCRIPTION',2),(111,'20020','20020','arrière 2 tiges de support',0,0,47,'STANDARD',2,'DESCRIPTION',2),(112,'20175','20175','kit d\'attache complet court',0,0,47,'STANDARD',1,'DESCRIPTION',2),(113,'20176','20176','tiges d\'attache',0,0,47,'STANDARD',11,'DESCRIPTION',2),(114,'1-06','1-06','reflecteurs à donner',0,0,48,'STANDARD',0,'DESCRIPTION',2),(115,'38500','38500','14-28T 6 V ',0,0,49,'STANDARD',3,'DESCRIPTION',2),(116,'38505','38505','14-28T 7 V',0,0,49,'STANDARD',3,'DESCRIPTION',2),(117,'1-07','1-07','outback',0,0,50,'STANDARD',1,'DESCRIPTION',2),(118,'70125','70125','confort',0,0,51,'STANDARD',1,'DESCRIPTION',2),(119,'70203','70203','Inclined homme',0,0,51,'STANDARD',1,'DESCRIPTION',2),(120,'70206','70206','Relaxed unisexe',0,0,51,'STANDARD',2,'DESCRIPTION',2),(121,'70208','70208','Relaxed femme',0,0,51,'STANDARD',2,'DESCRIPTION',2),(122,'70948','70948','Babac petit',0,0,51,'STANDARD',1,'DESCRIPTION',2),(123,'70954','70954','performance',0,0,51,'STANDARD',1,'DESCRIPTION',2),(124,'10010','10010','zefal noir',0,0,52,'STANDARD',5,'DESCRIPTION',2),(125,'1-08','1-08','babac 6 fonctions AS 400 - OUVERT',0,0,53,'STANDARD',1,'DESCRIPTION',2),(126,'31321','31321','Quick clic fixer',0,0,54,'STANDARD',1,'DESCRIPTION',2),(127,'1-09','1-09','Zefal plastique aluminium',0,0,54,'STANDARD',5,'DESCRIPTION',2),(128,'24406','24406','Evo 27 mm',0,0,55,'STANDARD',1,'DESCRIPTION',2),(129,'1-10','1-10','30,8 mm babac',0,0,55,'STANDARD',1,'DESCRIPTION',2),(130,'M01-01','01-01','Abonnement',0,10,56,'NON TAXABLE',0,'DESCRIPTION',2),(131,'DF4563','DF4563','Fraises',200,123,56,'STANDARD',100,'DESCRIPTION',0),(132,'DF5000','DF5000','Mongo',2300,400,51,'STANDARD',230,'DESCRIPTION',2),(133,'MM01','MEN01','Abonnement Mensuel',10,10,56,'NON TAXABLE',0,NULL,0),(134,'MA01','AN01','Abonnement Annuel',50,50,56,'NON TAXABLE',0,NULL,0);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `tps` double NOT NULL,
  `tvq` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax`
--

LOCK TABLES `tax` WRITE;
/*!40000 ALTER TABLE `tax` DISABLE KEYS */;
INSERT INTO `tax` VALUES (1,0.05,0.09975);
/*!40000 ALTER TABLE `tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workingtimes`
--

DROP TABLE IF EXISTS `workingtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workingtimes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MEMBERID` int(11) NOT NULL,
  `STARTDATETIME` timestamp NULL DEFAULT NULL,
  `ENDDATETIME` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_workingTimes_member1_idx` (`MEMBERID`),
  CONSTRAINT `fk_memberusername` FOREIGN KEY (`MEMBERID`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workingtimes`
--

LOCK TABLES `workingtimes` WRITE;
/*!40000 ALTER TABLE `workingtimes` DISABLE KEYS */;
INSERT INTO `workingtimes` VALUES (87,23,'2016-11-28 19:47:20','2016-11-28 20:47:20'),(88,10,'2016-11-28 13:00:00','2016-11-29 01:00:00'),(89,23,'2016-11-28 13:00:00','2016-11-29 01:00:00'),(90,22,'2016-11-28 20:41:31','2016-11-28 21:41:31'),(91,22,'2016-11-28 20:41:41','2016-11-28 21:41:41'),(92,23,'2016-11-28 13:00:00','2016-11-29 01:00:00'),(93,23,'2016-11-28 13:00:00','2016-11-29 01:00:00'),(94,14,'2016-11-28 23:12:13','2016-11-29 00:12:13'),(95,25,'2016-11-29 04:19:50','2016-11-29 05:19:50'),(96,25,'2016-11-28 13:00:00','2016-11-28 17:00:00'),(97,14,'2016-11-29 05:00:00','2016-11-29 05:00:00'),(98,27,'2016-11-29 16:15:09','2016-11-29 17:15:09'),(99,23,'2016-11-30 13:00:00','2016-11-30 17:00:00'),(100,23,'2016-11-30 13:00:00','2016-11-30 17:00:00'),(101,23,'2016-11-30 20:17:39','2016-11-30 21:17:39'),(102,23,'2016-11-30 20:27:03','2016-11-30 21:27:03'),(103,23,'2016-11-30 20:27:26','2016-11-30 21:27:26'),(104,14,'2016-11-30 20:27:49','2016-11-30 21:27:49'),(105,23,'2016-11-30 20:36:19','2016-11-30 21:36:19'),(106,25,'2016-11-30 20:37:22','2016-11-30 21:37:22'),(107,26,'2016-11-30 20:42:05','2016-11-30 21:42:05'),(108,26,'2016-11-30 20:42:41','2016-11-30 21:42:41'),(109,22,'2016-11-30 20:50:44','2016-11-30 21:50:44'),(110,27,'2016-11-30 20:51:19','2016-11-30 21:51:19'),(111,25,'2016-11-30 21:23:23','2016-11-30 22:23:23'),(112,27,'2016-11-30 21:26:44','2016-11-30 22:26:44'),(113,28,'2016-11-30 21:29:26','2016-11-30 22:29:26'),(114,28,'2016-11-30 21:32:19','2016-11-30 22:32:19'),(115,28,'2016-11-30 21:33:56','2016-11-30 22:33:56'),(116,28,'2016-11-30 21:35:24','2016-11-30 22:35:24'),(117,28,'2016-11-30 21:35:50','2016-11-30 22:35:50'),(118,28,'2016-11-30 21:35:52','2016-11-30 22:35:52'),(119,22,'2016-11-30 21:36:44','2016-11-30 22:36:44'),(120,26,'2016-11-30 21:44:32','2016-11-30 22:44:32'),(121,28,'2016-11-30 21:44:55','2016-11-30 22:44:55'),(122,28,'2016-11-30 21:46:34','2016-11-30 22:46:34'),(123,28,'2016-11-30 21:48:26','2016-11-30 22:48:26'),(124,23,'2016-11-30 22:18:17','2016-11-30 23:18:17'),(125,28,'2016-11-30 22:18:41','2016-11-30 23:18:41'),(126,23,'2016-11-30 22:20:10','2016-11-30 23:20:10'),(127,28,'2016-11-30 22:20:28','2016-11-30 23:20:28'),(128,26,'2016-11-30 22:25:24','2016-11-30 23:25:24'),(129,28,'2016-11-30 22:26:27','2016-11-30 23:26:27'),(130,23,'2016-11-30 22:28:25','2016-11-30 23:28:25'),(131,26,'2016-11-30 22:34:01','2016-11-30 23:34:01'),(132,28,'2016-11-30 22:34:23','2016-11-30 23:34:23'),(133,24,'2016-11-30 22:34:48','2016-11-30 23:34:48'),(134,28,'2016-11-30 23:01:10','2016-11-30 06:01:10'),(135,26,'2016-11-30 05:00:00','2016-11-30 05:00:00'),(136,26,'2016-11-30 05:00:00','2016-11-30 05:00:00'),(137,28,'2016-11-30 13:00:00','2016-12-01 01:00:00'),(138,28,'2016-11-30 13:00:00','2016-12-01 01:00:00'),(139,14,'2016-11-30 23:35:13','2016-11-30 23:49:30'),(140,26,'2016-11-30 13:50:00','2016-11-30 21:30:00'),(141,26,'2016-11-30 13:50:00','2016-11-30 21:30:00'),(142,9,'2016-11-30 13:00:00','2016-11-30 18:00:00'),(143,14,'2016-12-01 00:27:44','2016-12-01 00:28:42'),(144,9,'2016-12-03 16:15:43','2016-12-03 16:38:23');
/*!40000 ALTER TABLE `workingtimes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-03 12:25:36
