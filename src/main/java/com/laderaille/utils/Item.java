package com.laderaille.utils;

import org.primefaces.json.JSONObject;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import com.laderaille.domain.Category;
import com.laderaille.domain.Product;

public class Item {
	
	private int categoryId;
	
	public int getCategoryId() {return categoryId;}
	public void setCategoryId(int id) {this.categoryId = id;}
	
	private String productId;
	
	public String getProductId() {return productId;}
	public void setProductId(String id) {this.productId = id;}
	
	private double price;
	
	public double getPrice() {return price;}
	public void setPrice(double p) {this.price = p;}
	
	private double tsq;
	
	public double getTSQ() {return tsq;}
	public void setTSQ(double p) {this.tsq = p;}
	
	private double tvq;
	
	public double getTVQ() {return tvq;}
	public void setTVQ(double p) {this.tvq = p;}
	
	private int quantity;
	
	public int getQuantity() {return quantity;}
	public void setQuantity(int qty) {this.quantity = qty;}
		
}
