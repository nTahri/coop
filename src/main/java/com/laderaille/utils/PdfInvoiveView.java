//Ref: http://developers.itextpdf.com/
package com.laderaille.utils;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.laderaille.domain.Invoice;
import com.laderaille.domain.InvoiceLine;
import com.laderaille.service.CategoryService;
import com.laderaille.service.InvoiceLineService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.ProductService;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

public class PdfInvoiveView extends AbstractPdfView {
	@Autowired CategoryService catgoryService;
	@Autowired ProductService productService;
	@Autowired InvoiceLineService invoiceLineService;
	@Autowired InvoiceService invoiceService;
		
	@Value("${compagnie.name}")
	private String compagnieName;
	@Value("${compagnie.address}")
	private String compagnieAddress;
	@Value("${compagnie.ville}")
	private String compagnieVille;
	@Value("${compagnie.telephone}")
	private String compagniePhone;
	private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		int id = 12;
		Invoice invoice = invoiceService.findInvoiceById(id);
		List<InvoiceLine> invoiceLine = (List<InvoiceLine>)model.get("invoiceLine");   //invoiceLineService.findInvoiceLinesByInvoiceID(id);
		PdfWriter writerp = PdfWriter.getInstance(document, byteArrayOutputStream);
		document.open();
		//to start automatically the print option
		writer.addJavaScript("this.print(false);", false);
		Image header = Image.getInstance("http://" + request.getServerName() + ":" + request.getServerPort() + "/laderaille/static/images/bicycle-black.png");
		header.scaleAbsoluteHeight(100);
		header.scaleAbsoluteWidth(100);
		header.setAlignment(Image.RIGHT);
		document.add(header);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE d MMM yyyy");
		Date date = new Date();
		
		Chunk glue = new Chunk(new VerticalPositionMark());
		Paragraph p = new Paragraph();
		p.add(new Chunk(glue));
		p.add(compagnieVille + " le : " + dateFormat.format(date));
		document.add(p);
		
		
		document.add(new Paragraph(compagnieName));
		document.add(new Paragraph(compagnieAddress));
		document.add(new Paragraph(compagnieVille));
		document.add(new Paragraph(compagniePhone));
		
		document.add(new Paragraph(""));
		document.add(new Paragraph(""));
		document.add(new Paragraph("Numero de facture:  " + invoice.getId()));
		
       PdfPTable table = new PdfPTable(3);
       table.setWidthPercentage(100.0f);
       table.setWidths(new float[] {5.0f, 1.0f, 1.0f});
       table.setSpacingBefore(10);
        
       // define font for table header row
       com.lowagie.text.Font font = FontFactory.getFont(FontFactory.HELVETICA);
       font.setColor(Color.WHITE);

       PdfPCell cell = new PdfPCell();
       cell.setBackgroundColor(Color.LIGHT_GRAY);
       cell.setPadding(3);
        
       // write table header
       cell.setPhrase(new Phrase("Porduit", (com.lowagie.text.Font) font));
       table.addCell(cell);
        
       cell.setPhrase(new Phrase("Quantite", (com.lowagie.text.Font) font));
       table.addCell(cell);

       cell.setPhrase(new Phrase("Prix", (com.lowagie.text.Font) font));
       table.addCell(cell);
        
       for(InvoiceLine il: invoiceLine){
    	   table.addCell(productService.findByCode(il.getProductId()).getProductName());
    	   table.addCell(il.getQuantity()+"");
    	   table.addCell(il.getPrice()+"");
       }
       document.add(table);
       
       //response.setHeader("Content-Disposition", String.format("inline; filename=" + id + ".pdf"));
       //to download the pdf file 
       // response.addHeader("Content-Disposition", "attachment;filename=" + id + ".pdf");
       response.addHeader("Content-Disposition", "filename=" + id + ".pdf");
       response.setContentType("application/pdf");
	}

}
