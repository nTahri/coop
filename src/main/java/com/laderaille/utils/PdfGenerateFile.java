package com.laderaille.utils;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.laderaille.domain.Invoice;
import com.laderaille.domain.InvoiceLine;
import com.laderaille.domain.Member;
import com.laderaille.service.CategoryService;
import com.laderaille.service.EventService;
import com.laderaille.service.InvoiceLineService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.MailService;
import com.laderaille.service.MemberService;
import com.laderaille.service.ProductService;
import com.laderaille.service.TaxService;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

@Component
public class PdfGenerateFile extends AbstractPdfView{
	@Autowired CategoryService catgoryService;
	@Autowired ProductService productService;
	@Autowired InvoiceLineService invoiceLineService;
	@Autowired InvoiceService invoiceService;
	@Autowired EventService eventService; 
	@Autowired TaxService taxService;
	@Autowired MemberService memberService;
	@Autowired MailService mailService;
	
	@Value("${compagnie.name}")
	private String compagnieName;
	@Value("${compagnie.address}")
	private String compagnieAddress;
	@Value("${compagnie.ville}")
	private String compagnieVille;
	@Value("${compagnie.telephone}")
	private String compagniePhone;
	
	private int id;
	private boolean show;
	private boolean emailIt;
	public void setId(int id){this.id=id;}
	public void setShow(boolean show){this.show=show;}
	public void setEmailIt(boolean emailIt){this.emailIt=emailIt;}
	public PdfGenerateFile(int id, boolean show, boolean emailIt){
		this.id = id;
		this.show = show;
		this.emailIt=emailIt;
	}
	public PdfGenerateFile(){
	}
	private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("******************************************************* " + id);
		Invoice invoice = invoiceService.findInvoiceById(id);
		List<InvoiceLine> invoiceLine = invoiceLineService.findInvoiceLinesByInvoiceID(id);
		double soutotal = 0.0, tps = 0.0, tvq = 0.0;
		PdfWriter.getInstance(document, byteArrayOutputStream);
		document.open();
		//to start automatically the print option
		writer.addJavaScript("this.print(false);", false);
		Image header = Image.getInstance("http://" + request.getServerName() + ":" + request.getServerPort() + "/laderaille/static/images/logo_black.png");
		header.scaleAbsoluteHeight(100);
		header.scaleAbsoluteWidth(100);
		header.setAlignment(Image.RIGHT);
		document.add(header);
		
		DateFormat dateFormat = new SimpleDateFormat("EEE d MMM yyyy");
		Date date = new Date();
		
		Chunk glueDate = new Chunk(new VerticalPositionMark());
		Paragraph p = new Paragraph();
		p.add(new Chunk(glueDate));
		p.add(compagnieVille + " le : " + dateFormat.format(date));
		document.add(p);
		
		//entete
		document.add(new Paragraph(compagnieName));
		document.add(new Paragraph(compagnieAddress));
		document.add(new Paragraph(compagnieVille));
		document.add(new Paragraph(compagniePhone));
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		Member buyer = memberService.findByMail(invoice.getBuyerName());
		document.add(new Paragraph(buyer.getFirstName() + " " + buyer.getLastName()));
		document.add(new Paragraph(buyer.getAddress()));
		document.add(new Paragraph(buyer.getMail()));
		
		Chunk glueEvent = new Chunk(new VerticalPositionMark());
		Paragraph pEvent = new Paragraph();
		pEvent.add(new Chunk(glueEvent));
		pEvent.add("evenement: " + eventService.findById(invoiceService.findInvoiceById(id).getEventId()).getName());
		document.add(pEvent);
		
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		String status = invoice.getInvoiceStatus() == 0 ? "Non-Payee": (invoice.getInvoiceStatus() == 2 ? "Remboursement" :"Payee");
		document.add(new Paragraph("Numero de facture:  " + invoice.getId() + "  Statut: " + status));

		
       PdfPTable table = new PdfPTable(3);
       table.setWidthPercentage(100.0f);
       table.setWidths(new float[] {5.0f, 2.0f, 1.0f});
       table.setSpacingBefore(30);
        
       // define font for table header row
       com.lowagie.text.Font font = FontFactory.getFont(FontFactory.HELVETICA);
       font.setColor(Color.WHITE);

       PdfPCell cell = new PdfPCell();
       cell.setBackgroundColor(Color.LIGHT_GRAY);
       cell.setPadding(10);
        
       // write table header
       cell.setPhrase(new Phrase("Produit", (com.lowagie.text.Font) font));
       table.addCell(cell);
        
       cell.setPhrase(new Phrase("Quantite", (com.lowagie.text.Font) font));
       table.addCell(cell);

       cell.setPhrase(new Phrase("Prix", (com.lowagie.text.Font) font));
       table.addCell(cell);
        
       for(InvoiceLine il: invoiceLine){

//		   String categoryName = catgoryService.findCategoryById(productService.findByCode(il.getProductId())).getName();
    	   table.addCell(productService.findByCode(il.getProductId()).getProductName());
    	   table.addCell(il.getQuantity()+"");
    	   table.addCell(String.format("%.2f",il.getPrice()));
    	   soutotal += il.getPrice() * il.getQuantity();
    	   tps += il.getTSQ();
    	   tvq += il.getTVQ();
    	   if(il.getDiscount() != 0){
    		   double rabais = 0.0;
    		   table.addCell(productService.findByCode(il.getProductId()).getProductName() + " Rabais");
        	   table.addCell(il.getQuantity()+"");
        	   
    		   if(il.getDiscountType().equals("%")){
    			   rabais = -1 * il.getPrice() * il.getQuantity() * il.getDiscount() / 100;
    		   }else{
    			   rabais = -1 * il.getDiscount();
    		   }
    		   soutotal += rabais;
    		   table.addCell(rabais+"");
    	   }
       }
       PdfPCell cellSpliter = new PdfPCell();
       
       cellSpliter.setBackgroundColor(Color.BLACK);
       cellSpliter.setPadding(2);
       table.addCell(cellSpliter);
       cellSpliter.setBackgroundColor(Color.BLACK);
       cellSpliter.setPadding(2);
       table.addCell(cellSpliter);
       cellSpliter.setBackgroundColor(Color.BLACK);
       cellSpliter.setPadding(2);
       table.addCell(cellSpliter);
       //Taxes
       PdfPCell emptyCell = new PdfPCell();
       emptyCell.setBorderWidthBottom(0);
       emptyCell.setBorderWidthTop(0);
       emptyCell.setBorderWidthLeft(0);
       table.addCell(emptyCell);
	   table.addCell("SOUSTOTAL");
	   table.addCell(String.format("%.2f",invoice.getSubtotal()));
	   
       table.addCell(emptyCell);
	   table.addCell("TPS");
	   table.addCell(String.format("%.2f",tps)); //soutotal * taxService.getTps(1)
	   
	   table.addCell(emptyCell);
	   table.addCell("TVQ");
	   table.addCell(String.format("%.2f",tvq));//soutotal * taxService.getTvq(1)
	   
	   table.addCell(emptyCell);
	   table.addCell("TOTAL");
	   table.addCell(String.format("%.2f",invoice.getTotal()));//(taxService.getTps(1) + taxService.getTvq(1))
	   
       document.add(table);
       document.close();

	   if(emailIt){
		   String absoluteFilesystemPath = getServletContext().getRealPath("/");
	       FileOutputStream fos = new FileOutputStream (new File(absoluteFilesystemPath + "static/Factures/Facture " + compagnieName + " #" + id + ".pdf"));
	       
    	   try {
    		    byteArrayOutputStream.writeTo(fos);
    		    
    		} catch(IOException ioe) {

    		    ioe.printStackTrace();
    		} finally {
    		    fos.close();
    		    File facture = new File(absoluteFilesystemPath + "static/Factures/Facture " + compagnieName + " #" + id + ".pdf");
    		    List<File> attachments = new ArrayList<File>();
    		    String[] toAll = new String[1];
    		    toAll[0] = invoice.getBuyerName();
    		    String senderMail = invoice.getSellerName();
    		    String subject = "Facture " + compagnieName + " #" + id;
    		    String message = "Ci-joint la facture #" + id;
    		    attachments.add(facture);
    		    mailService.sendEmail(toAll, senderMail, subject, message, attachments);
    		    facture.delete();
    		}
       }
       //response.setHeader("Content-Disposition", String.format("inline; filename=" + id + ".pdf"));
       //to download the pdf file 
       // response.addHeader("Content-Disposition", "attachment;filename=" + id + ".pdf");
       if(show){
    	   response.addHeader("Content-Disposition", "filename=" + id + ".pdf");
       }else{
    	   response.addHeader("Content-Disposition", "attachment;filename=" + id + ".pdf"); 
       }
       response.setContentType("application/pdf");
	}

}
