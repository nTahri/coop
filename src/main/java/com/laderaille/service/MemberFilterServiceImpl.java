package com.laderaille.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.laderaille.domain.MemberFilter;
import com.laderaille.repository.MemberFilterDao;

@Service
public class MemberFilterServiceImpl implements MemberFilterService{

		@Autowired
		private MemberFilterDao memberFilterDao;

		@Override
		public List<MemberFilter> getAllCriteria() {			
			return memberFilterDao.getAllCriteria();
		}
}

