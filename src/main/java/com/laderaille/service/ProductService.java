package com.laderaille.service;

import java.util.List;

import com.laderaille.domain.Product;

public interface ProductService {
	Product findById(int id);
	
	Product findByCode(String code);
	
	Product findProductByName (String name);

	void saveProduct(Product product);
	
	void updateProduct(Product product);
	
	void deleteProductByName (String name);
	
	void deleteProductByID(Integer id);
	
	List<Product> findAllProducts();
	void deleteAllProducts();
}
