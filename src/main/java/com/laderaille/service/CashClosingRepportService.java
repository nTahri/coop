package com.laderaille.service;

import java.util.Date;
import org.springframework.stereotype.Service;
import com.laderaille.utils.Pair;

@Service("CashClosingRepportService")
public interface CashClosingRepportService {	
	Pair<Double, Double> findNumberOfMemberShipInDay(Date date, String productID);	
	Pair<Double, Double> findAllUnclosedInvoiceInDay(Date date);	
	Pair<Double, Double> findAllInvoiceInDay(Date date);
	Pair<Double, Double> findInvoicesClosedThisDay(Date date);	
}
