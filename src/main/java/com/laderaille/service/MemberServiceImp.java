package com.laderaille.service;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.laderaille.domain.Member;
import com.laderaille.repository.MemberDao;

/**
 * This class contains the methods that translates the business logic related to
 * the MEMBER object.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Soumri.N
 * @author Tahri.N
 * @author Yezli.S
 * 
 * @version 1.0
 */

@Service("MemberService")
@Transactional
public class MemberServiceImp implements MemberService {

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public Member findById(int id) {
		return memberDao.findById(id);
	}

	@Override
	public Member findByMail(String mail) {
		return memberDao.findMemberByMail(mail);
	}

	@Override
	public void saveMember(Member member) {
		member.setPassword(passwordEncoder.encode(member.getPassword()));
		memberDao.saveMember(member);
	}

	@Override
	public void updateMember(Member member) {
		// member.setPassword(passwordEncoder.encode(member.getPassword()));
		// memberDao.updateMember(member);
		Member entityMember = memberDao.findById(member.getId());
		if (entityMember != null) {
			entityMember.setFirstName(member.getFirstName());
			entityMember.setLastName(member.getLastName());
			entityMember.setMail(member.getMail());
			entityMember.setAddress(member.getAddress());
			if (!member.getPassword().equals(entityMember.getPassword())) {
				entityMember.setPassword(passwordEncoder.encode(member.getPassword()));
			}
			entityMember.setFacebookName(member.getFacebookName());
			entityMember.setPhoneNumber(member.getPhoneNumber());
			entityMember.setStudentID(member.getStudentID());
			entityMember.setJoiningDate(member.getJoiningDate());
			entityMember.setSex(member.getSex());
			entityMember.setStatus(member.getStatus());
			entityMember.setMemberProfiles(member.getMemberProfiles());
			if(member.getBirthday() != null){
				entityMember.setBirthday(member.getBirthday());
			}
			if(member.getLeftDate() != null){
				entityMember.setLeftDate(member.getLeftDate());
			}
			if(member.getExpiringDate() != null){
				entityMember.setExpiringDate(member.getExpiringDate());
			}
		}

	}

	@Override
	public void deleteMemberByMail(String mail) {
		memberDao.deleteMemberByMail(mail);
	}

	@Override
	public void deleteMemberByID(Integer id) {
		memberDao.deleteMemberByID(id);
	}

	@Override
	public List<Member> findAllMembers() {
		return memberDao.findAllMembers();
	}

	@Override
	public List<Member> findFormerMembers() {
		return memberDao.findFormerMembers();
	}

	@Override
	public List<Member> findAllMemberBetweenJoiningDate(Date stardDate, Date endDate) {
		return memberDao.findAllMemberBetweenJoiningDate(stardDate, endDate);
	}
	
	@Override
	public void deleteAllMembers(){
		memberDao.deleteAllMembers();
	}
	// @Override
	// public boolean isMemberMailUnique(Integer id, String mail) {
	// Member member = findByMail(mail);
	// return ( member == null || ((id != null) && (member.getId() == id)));
	// }

}
