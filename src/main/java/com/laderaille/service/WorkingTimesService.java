package com.laderaille.service;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;

public interface WorkingTimesService {

	void saveWorkingTimes(WorkingTimes workingTimes);

	WorkingTimes findWorkingTimesByID(int workingTimesID);

	void deleteWorkingTimesByID(int workingTimesID);
	
	void editWorkingTimes(WorkingTimes workingTimes);
	
	List<WorkingTimes> getAllWorkingTimes();	

	List<WorkingTimes> findAllWorkingTimeBetweenDate(Member member, Date stardDate, Date endDate);
	
	List<WorkingTimes> findAllWorkingTimeByMember(Member member);
	
	boolean updateEventMembers(Member member, int eventId);	

}
