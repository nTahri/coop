package com.laderaille.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Event;
import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;
import com.laderaille.repository.WorkingTimesDao;


@Service
public class WorkingTimesServiceImp implements WorkingTimesService {

	@Autowired
	private WorkingTimesDao workingTimesDao;	
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	EventService eventService;

	@Transactional
	public void saveWorkingTimes(WorkingTimes workingTimes) {
		workingTimesDao.saveWorkingTimes(workingTimes);		
	}	

	@Transactional
	public void deleteWorkingTimesByID(int workingTimesID) {
		workingTimesDao.deleteWorkingTimesByID(workingTimesID);
	}

	@Transactional
	public void editWorkingTimes(WorkingTimes workingTimes) {
		workingTimesDao.editWorkingTimes(workingTimes);
	}

	@Transactional
	public WorkingTimes findWorkingTimesByID(int workingTimesID) {
		return workingTimesDao.getWorkingTimesByID(workingTimesID);
	}

	@Transactional
	public List<WorkingTimes> findAllWorkingTimeBetweenDate(Member member, Date stardDate, Date endDate) {
		return workingTimesDao.findAllWorkingTimeBetweenDate(member, stardDate, endDate);
	}

	@Transactional
	public List<WorkingTimes> getAllWorkingTimes() {
		return workingTimesDao.getAllWorkingTimes();
	}

	@Transactional
	public List<WorkingTimes> findAllWorkingTimeByMember(Member member) {
		return workingTimesDao.findAllWorkingTimeByMember(member);
	}

	@Transactional
	public boolean updateEventMembers(Member member, int eventId) {
		boolean found = false;
		boolean returnValue = false;
		Event event = eventService.findById(eventId);
		Set<Member> participants = event.getMembers();			
		for (Member participant  : participants) {
			if(participant.getId() == member.getId())
			{
				found = true;
				break;				
			}			
		}		
		if (found == false){
			participants.add(member);
			event.setMembers(participants);
			eventService.updateEvent(event);
			returnValue= true ; 
			
		}else{			
			returnValue= false ; 
		}
		
		return returnValue ;
	}	
}

