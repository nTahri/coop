package com.laderaille.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Category;
import com.laderaille.repository.CategoryDao;

@Service("CategoryService")
@Transactional
public class CategoryServiceImp implements CategoryService {
	@Autowired
	private CategoryDao categoryDao;
	
	@Override
	public Category findById(int id) {
		
		return categoryDao.findById(id);
	}

	@Override
	public Category findCategoryByName(String name) {
		return categoryDao.findByName(name);
	}

	@Override
	public void saveCategory(Category category) {
		categoryDao.saveCategory(category);
	}

	@Override
	public void updateCategory(Category category) {
		Category cat = categoryDao.findById(category.getId());
		if(cat != null){
			cat.setName(category.getName());
			cat.setCategoryParent(category.getCategoryParent());
			
		}
	}

	@Override
	public void deleteCategoryByName(String name) {
		categoryDao.deleteByName(name);
	}

	@Override
	public void deleteCategoryById(Integer id) {
		categoryDao.deleteById(id);
	}

	@Override
	public List<Category> findAllCategories() {
		return categoryDao.findAll();
	}
	
	@Override
	public void deleteAllCategories(){
		categoryDao.deleteAllCategories();
	}
}
