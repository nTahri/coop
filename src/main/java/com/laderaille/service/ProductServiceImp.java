package com.laderaille.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Product;
import com.laderaille.repository.ProductDao;

/**
 * This class contains the methods that translates the business logic related to
 * the PRODUCT object.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Soumri.N
 * @author Tahri.N
 * @author Yezli.S
 * 
 * @version 1.0
 */

@Service("ProductService")
@Transactional
public class ProductServiceImp implements ProductService {

	@Autowired
	private ProductDao productDao;

	@Override
	public Product findById(int id) {
		return productDao.findById(id);
	}

	@Override
	public Product findByCode(String code) {
		return productDao.findByCode(code);
	}

	@Override
	public Product findProductByName(String name) {
		return productDao.findProductByName(name);
	}

	@Override
	public void saveProduct(Product product) {
		productDao.saveProduct(product);
	}

	@Override
	public void updateProduct(Product product) {
		productDao.updateProduct(product);
		// Product pro = productDao.findById(product.getId());
		// if(pro != null){
		// pro.setProductCategory(product.getProductCategory());
		// pro.setProductCode(product.getProductCode());
		// pro.setProductCodeType(product.getProductCodeType());
		// pro.setProductName(product.getProductName());
		// pro.setProductPrice(product.getProductPrice());
		// pro.setProductRef(product.getProductRef());
		// pro.setProductTaxCat(product.getProductTaxCat());
		// pro.setProductStock(product.getProductStock());
		// }
	}

	@Override
	public void deleteProductByName(String name) {
		productDao.deleteProductByName(name);
	}

	@Override
	public void deleteProductByID(Integer id) {
		productDao.deleteProductByID(id);
	}

	@Override
	public List<Product> findAllProducts() {
		return productDao.findAllProducts();
	}
	@Override
	public void deleteAllProducts(){
		productDao.deleteAllProducts();
	}
}
