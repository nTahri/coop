package com.laderaille.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.MemberProfile;
import com.laderaille.repository.MemberProfileDao;

@Service("MemberProfileService")
@Transactional
public class MemberProfileServiceImp implements MemberProfileService {

	@Autowired
	MemberProfileDao dao;
	
	@Override
	public MemberProfile findById(int id) {
		return dao.findById(id);
	}

	@Override
	public MemberProfile findByType(String type) {
		return dao.findByType(type);
	}

	@Override
	public List<MemberProfile> findAll() {
		return dao.findAll();
	}
	
	@Override
	public void deleteAllMembersProfile(){
		dao.deleteAllMembersProfile();
	}
	

}
