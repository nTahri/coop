package com.laderaille.service;

import java.util.List;

import com.laderaille.domain.EventName;

public interface EventNameService {
	EventName findById(int id);
	EventName findByName(String name);
	List<EventName> findAllEventsName();
	void deleteAllEventName();
}
