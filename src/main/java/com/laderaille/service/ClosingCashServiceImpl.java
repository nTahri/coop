package com.laderaille.service;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.laderaille.domain.ClosingCash;
import com.laderaille.repository.ClosingCashDao;

@Service
public class ClosingCashServiceImpl implements ClosingCashService{

	@Autowired
	private ClosingCashDao closingCashDao;

	@Transactional	
	public ClosingCash findClosingCashById(int id) {
		return closingCashDao.findClosingCashById(id);
	}

	@Transactional
	public ClosingCash findClosingCashByDate(Date date) {
		return closingCashDao.findClosingCashByDate(date);
	}

	@Transactional
	public void saveClosingCash(ClosingCash closingCash) {
		closingCashDao.saveClosingCash(closingCash);
	}

	@Transactional
	public void deleteClosingCashByDate(Date date) {
		closingCashDao.deleteClosingCashByDate(date);
	}

	@Transactional
	public void editClosingCash(ClosingCash closingCash) {
		closingCashDao.editClosingCash(closingCash);
	}

	@Override
	public ClosingCash findLastClosingCash(Date date) {
		return closingCashDao.findLastClosingCash(date);
	}
	@Override
	public void deleteAllClosingCashs(){
		closingCashDao.deleteAllClosingCashs();
	}
}
