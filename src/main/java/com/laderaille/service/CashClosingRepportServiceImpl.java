package com.laderaille.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.InvoiceLine;
import com.laderaille.utils.Pair;

@Service
public class CashClosingRepportServiceImpl implements CashClosingRepportService{
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	InvoiceLineService invoiceLineService;
	
	@Autowired
	InvoiceService invoiceService;	

	/*
	 * @see com.laderaille.service.CashClosingRepportService#numberOfSubscriptionInDay
	 * (java.util.Date.
	 * 
	 * @function Get number of new members(of this day) who have payed memberShip.	 
	 * 
	 * @param Date date (this report date)
	 */
	@Transactional
	public Pair<Double, Double> findNumberOfMemberShipInDay(Date date, String productID) {			
		double memberShipNumber = 0;
		double memberShipAmount = 0;
		InvoiceLine invoiceLine = new InvoiceLine();		
		if (invoiceService.findAllInvoiceByDate(date) != null){	
			List<Invoice> invoiceList = invoiceService.findAllInvoiceByDate(date);			
			for(Invoice item: invoiceList){				
				if(invoiceLineService.findInvoiceLineByInvoiceAndProductID(item.getId(), productID) != null){	
					invoiceLine = invoiceLineService.findInvoiceLineByInvoiceAndProductID(item.getId(), productID);
					memberShipAmount += invoiceLine.getPrice();
					memberShipNumber +=invoiceLine.getQuantity();
				}
			}			
		}	
		return new Pair<Double, Double>(memberShipNumber, memberShipAmount);
	}

	@Transactional
	public Pair<Double, Double> findAllUnclosedInvoiceInDay(Date date) {
		double uncloseInvoiceNumber = 0;
		double uncloseInvoiceAmount = 0;
		if (invoiceService.findAllInvoiceByDate(date) != null){	
			List<Invoice> invoiceList = invoiceService.findAllInvoiceByDate(date);			
			for(Invoice item: invoiceList){	
				if (item.getInvoiceStatus() != 1){	
					uncloseInvoiceNumber ++;
					uncloseInvoiceAmount +=item.getTotal();
				}
			}	
		}		
		return new Pair<Double, Double>(uncloseInvoiceNumber, -uncloseInvoiceAmount);
	}
	
	@Transactional
	public Pair<Double, Double> findAllInvoiceInDay(Date date) {
		double invoiceLineNumber = 0;
		double InvoiceAmount = 0;
		if (invoiceService.findAllInvoiceByDate(date) != null){	
			List<Invoice> invoiceList = invoiceService.findAllInvoiceByDate(date);			
			for(Invoice item: invoiceList){	
				InvoiceAmount +=item.getTotal();
				if(invoiceLineService.findAllInvoiceLineByInvoiceID(item.getId()) != null){	
					for (InvoiceLine subItem:invoiceLineService.findAllInvoiceLineByInvoiceID(item.getId())){
						invoiceLineNumber += subItem.getQuantity();
					}
				}	
			}
		}		
		return new Pair<Double, Double>(invoiceLineNumber, InvoiceAmount);
	}
	
	@Transactional
	public Pair<Double, Double> findInvoicesClosedThisDay(Date date) {		
		double closePriorInvoiceNumber = 0;
		double closePriorInvoiceAmount = 0;
		if (invoiceService.findInvoicesClosedThisDay(date) != null){	
			List<Invoice> invoiceList = invoiceService.findInvoicesClosedThisDay(date);			
			for(Invoice item: invoiceList){	
				closePriorInvoiceNumber ++;
				closePriorInvoiceAmount +=item.getTotal();
			}	
		}		
		return new Pair<Double, Double>(closePriorInvoiceNumber, closePriorInvoiceAmount);
	}
}
