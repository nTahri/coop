package com.laderaille.service;

import java.util.List;

import com.laderaille.domain.MemberFilter;

public interface MemberFilterService {
	
	List<MemberFilter> getAllCriteria();	

}
