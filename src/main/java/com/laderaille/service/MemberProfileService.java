package com.laderaille.service;

import java.util.List;

import com.laderaille.domain.MemberProfile;

public interface MemberProfileService {

	MemberProfile findById(int id);

	MemberProfile findByType(String type);
	
	List<MemberProfile> findAll();
	void deleteAllMembersProfile();
}
