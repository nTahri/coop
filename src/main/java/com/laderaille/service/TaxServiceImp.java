package com.laderaille.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.laderaille.domain.Tax;
import com.laderaille.repository.TaxDao;


@Service("TaxService")
@Transactional
public class TaxServiceImp implements TaxService {
	@Autowired TaxDao taxDao;
	
	@Override
	public Tax findTaxById(int id) {
		return taxDao.findTaxById(id);
	}

	@Override
	public double getTps(int id) {
		
		return taxDao.getTps(id);
	}

	@Override
	public double getTvq(int id) {
		return taxDao.getTvq(id);
	}

}
