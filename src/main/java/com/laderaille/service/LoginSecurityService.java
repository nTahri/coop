package com.laderaille.service;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Member;
import com.laderaille.domain.MemberProfile;

@Service("LoginSecurityService")
public class LoginSecurityService implements UserDetailsService{

	static final Logger logger = LoggerFactory.getLogger(LoginSecurityService.class);
	
	@Autowired
	private MemberService memberService;
	
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String mail)
			throws UsernameNotFoundException {
		Member member = memberService.findByMail(mail);
		logger.info("Member : {}", member);
		if(member==null){
			logger.info("Member not found");
			throw new UsernameNotFoundException("Membername not found");
		}
			return new org.springframework.security.core.userdetails.User(member.getMail(), member.getPassword(), 
				 true, true, true, true, getGrantedAuthorities(member));
	}

	
	private List<GrantedAuthority> getGrantedAuthorities(Member member){
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		for(MemberProfile memberProfile : member.getMemberProfiles()){
			logger.info("UserProfile : {}", memberProfile);
			authorities.add(new SimpleGrantedAuthority("ROLE_"+memberProfile.getType()));
		}
		logger.info("authorities : {}", authorities);
		return authorities;
	}



	
}


