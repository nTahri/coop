package com.laderaille.service;

import com.laderaille.domain.Tax;

public interface TaxService {
	Tax findTaxById(int id);
	double getTps(int id);
	double getTvq(int id);
}
