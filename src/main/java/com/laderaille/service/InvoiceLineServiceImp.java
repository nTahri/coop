package com.laderaille.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.InvoiceLine;
import com.laderaille.repository.InvoiceLineDao;

@Service("InvoiceLineService")
@Transactional
public class InvoiceLineServiceImp implements InvoiceLineService {
	@Autowired
	private InvoiceLineDao invoiceLineDao;
	
	@Override
	public InvoiceLine findInvoiceLineById(int id) {
		return invoiceLineDao.findInvoiceLineByID(id);
	}

	@Override
	public List<InvoiceLine> findInvoiceLinesByInvoiceID(int id) {
		
		return invoiceLineDao.findInvoiceLinesByInvoiceID(id);
	}

	@Override
	public void saveInvoiceLine(InvoiceLine invoiceLine) {
		invoiceLineDao.saveInvoiceLine(invoiceLine);
	}

	@Override
	public void updateInvoiceLine(InvoiceLine invoiceLine) {
		InvoiceLine il = invoiceLineDao.findInvoiceLineByID(invoiceLine.getId());
		if(il != null){
			il.setInvoiceId(invoiceLine.getInvoiceId());
			il.setQuantity(invoiceLine.getQuantity());
			il.setProductId(invoiceLine.getProductId());
			il.setPrice(invoiceLine.getPrice());
			il.setTSQ(invoiceLine.getTSQ());
			il.setTVQ(invoiceLine.getTVQ());
		}
	}

	@Override
	public void deleteInvoiceLineByID(Integer id) {
		invoiceLineDao.deleteInvoiceLineByID(id);
	}

	@Override
	public List<InvoiceLine> findAll() {
		
		return invoiceLineDao.findAllInvoice();
	}

	@Override
	public InvoiceLine findInvoiceLineByInvoiceAndProductID(int invoiceID, String productID) {
		return invoiceLineDao.findInvoiceLineByInvoiceAndProductID(invoiceID, productID);
	}

	@Override
	public List<InvoiceLine> findAllInvoiceLineByInvoiceID(int invoiceID) {
		return invoiceLineDao.findAllInvoiceLineByInvoiceID(invoiceID);
	}
	@Override
	public void deleteAllInvoiceLines(){
		invoiceLineDao.deleteAllInvoiceLines();
	}

}
