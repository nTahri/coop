package com.laderaille.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.laderaille.domain.Member;
import com.laderaille.service.MemberService;

@Component
public class StringToMemberConverter implements Converter<String, Member>{

	// static final Logger logger = LoggerFactory.getLogger(RoleToUserProfileConverter.class);
	
	@Autowired
	MemberService memberService;

	/**
	 * Gets UserProfile by Id
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public Member convert(String element) {
		Integer id = Integer.parseInt(element);
		Member profile = memberService.findById(id);

		return profile;
	}
	
}
