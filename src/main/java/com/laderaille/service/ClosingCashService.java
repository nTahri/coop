package com.laderaille.service;

import java.util.Date;

import com.laderaille.domain.ClosingCash;


public interface ClosingCashService {
	
	ClosingCash findClosingCashById(int id);
	
	ClosingCash findClosingCashByDate(Date date);

	void saveClosingCash(ClosingCash closingCash);

	public void deleteClosingCashByDate(Date date);
	
	public void editClosingCash(ClosingCash closingCash);
	
	ClosingCash findLastClosingCash(Date date);

	public void deleteAllClosingCashs();
}
