package com.laderaille.service;
import java.util.Date;
import java.util.List;
//import com.laderaille.domain.Member;

import com.laderaille.domain.Invoice;

public interface InvoiceService {
	
	Invoice findInvoiceById(int id);
	
	Invoice findInvoiceByBuyerMail (String mail);
	
	Invoice findInvoiceBySellerMail (String mail);

	void saveInvoice(Invoice invoice);
	
	void updateInvoice(Invoice invoice);
	
	// void deleteInvoiceById(int invoiceID);
	
	void deleteInvoiceById(Integer id);
	
	List<Invoice> findAllInvoices();
	
	List<Invoice> findAllOpenInvoices();
	
	List<Invoice> findAllInvoiceBetweenDate(Date stardDate, Date endDate);
	
	List<Invoice> findOpenInvoiceBetweenDate(Date stardDate, Date endDate);
	
	List<Invoice> findAllInvoiceByDate(Date date);	
	
	List<Invoice> findInvoicesClosedThisDay(Date date);
	
	Invoice findByEventId(int eventID); 
	
	List<Invoice> findInvoicesByEventId(int eventID);
	void deleteAllInvoices();
}
