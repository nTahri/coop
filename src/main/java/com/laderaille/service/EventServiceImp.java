package com.laderaille.service;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;

import com.laderaille.domain.Event;
import com.laderaille.repository.EventDao;

/**
 * This class contains the methods that translates the business logic related to
 * the EVENT object.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Soumri.N
 * @author Tahri.N
 * @author Yezli.S
 * 
 * @version 1.0
 */

@Service("EventService")
@Transactional
public class EventServiceImp implements EventService {

	@Autowired
	private EventDao eventDao;

	@Override
	public void saveEvent(Event event) {
		
		eventDao.saveEvent(event);

	}

	@Override
	public void updateEvent(Event event) {
//		eventDao.updateEvent(event);
		Event entityEvent = eventDao.findById(event.getId());
		if (entityEvent != null) {
			entityEvent.setName(event.getName());
			entityEvent.setStartDate(event.getStartDate());
			entityEvent.setEndDate(event.getEndDate());
			entityEvent.setDescription(event.getDescription());
			entityEvent.setMembers(event.getMembers());
//			if(event.getInvoices().size() != 0){
//				entityEvent.setInvoices(event.getInvoices());
//			}
		}
	}

	@Override
	public void deleteEvent(Event event) {
		int id = event.getId();
		eventDao.deleteEventByID(id);

	}

	@Override
	public Event findById(int id) {
		return eventDao.findById(id);
	}

	@Override
	public Event findMemberByNameAndStartDate(String name, Date startDate) {
		return eventDao.findEventByNameAndStartDate(name, startDate);
	}

	@Override
	public List<Event> findAllEvents() {
		return eventDao.findAllEvents();

	}

	@Override
	public List<Event> findAllDaylyEvents(Date startDate, Date endDate) {
		return eventDao.findAllDaylyEvents(startDate, endDate);
	}	
	
	@Override
	public void deleteAllEvents(){
		eventDao.deleteAllEvents();
	}
}
