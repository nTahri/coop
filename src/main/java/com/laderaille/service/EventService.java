package com.laderaille.service;


import java.util.Date;
import java.util.List;

import com.laderaille.domain.Event;

public interface EventService {

	void saveEvent(Event event);

	void updateEvent(Event event);

	void deleteEvent(Event event);

	Event findById(int id);

	Event findMemberByNameAndStartDate(String name, Date startDate);

	List<Event> findAllEvents();
	
	List<Event> findAllDaylyEvents(Date startDate, Date endDate);
	void deleteAllEvents();

}
