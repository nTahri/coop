package com.laderaille.service;

import java.util.List;
import com.laderaille.domain.Category;

public interface CategoryService {
	Category findById(int id);
	
	Category findCategoryByName (String name);

	void saveCategory(Category category);
	
	void updateCategory(Category category);
	
	void deleteCategoryByName(String name);
	
	void deleteCategoryById(Integer id);
	
	List<Category> findAllCategories();
	void deleteAllCategories();
}
