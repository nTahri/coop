package com.laderaille.service;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Invoice;
import com.laderaille.repository.InvoiceDao;

/**
 * This class contains the methods that translates the business logic related to
 * the INVOICE object.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Soumri.N
 * @author Tahri.N
 * @author Yezli.S
 * 
 * @version 1.0
 */
@Service("InvoiceService")
@Transactional
public class InvoiceServiceImp implements InvoiceService {
	@Autowired
	private InvoiceDao invoiceDao;
	
	@Override
	public Invoice findInvoiceById(int id) {
		
		return invoiceDao.findInvoiceById(id);
	}

	@Override
	public Invoice findInvoiceByBuyerMail(String mail) {
		return invoiceDao.findInvoiceByBuyerMail(mail);
	}

	@Override
	public Invoice findInvoiceBySellerMail(String mail) {
		return invoiceDao.findInvoiceBySellerMail(mail);
	}

	@Override
	public void saveInvoice(Invoice invoice) {
		invoiceDao.saveInvoice(invoice);
	}

	@Override
	public void updateInvoice(Invoice invoice) {
		invoiceDao.updateInvoice(invoice);
//		Invoice inv = invoiceDao.findInvoiceById(invoice.getId());
//		if(inv != null){
//			inv.setBuyerName(invoice.getBuyerName());
//			inv.setInvoiceStatus(invoice.getInvoiceStatus());
//			inv.setSubtotal(invoice.getSubtotal());
//			inv.setTransactionDate(invoice.getTransactionDate());
//		}
	}

//	@Override
//	public void deleteInvoiceById(int invoiceID) {
//		invoiceDao.deleteInvoiceById(invoiceID);
//	}

	@Override
	public void deleteInvoiceById(Integer id) {
		invoiceDao.deleteInvoiceById(id);
	}

	@Override
	public List<Invoice> findAllInvoices() {
		return invoiceDao.findAllInvoices();
	}
	@Override
	public List<Invoice> findAllOpenInvoices() {
		return invoiceDao.findAllOpenInvoices();
	}
	
	@Override
	public List<Invoice> findAllInvoiceBetweenDate(Date stardDate, Date endDate) {
		return invoiceDao.findAllInvoiceBetweenDate(stardDate, endDate);
	}
	
	
	@Override
	public List<Invoice> findOpenInvoiceBetweenDate(Date stardDate, Date endDate) {
		return invoiceDao.findOpenInvoiceBetweenDate(stardDate, endDate);
	}

	@Override
	public List<Invoice> findAllInvoiceByDate(Date date) {
		return invoiceDao.findAllInvoiceByDate(date);
	}

	@Override
	public List<Invoice> findInvoicesClosedThisDay(Date date) {
		return invoiceDao.findInvoicesClosedThisDay(date);
	}

	@Override
	public Invoice findByEventId(int eventId) {
		
		return invoiceDao.findByEventId(eventId);
	}

	@Override
	public List<Invoice> findInvoicesByEventId(int eventID) {
		
		return invoiceDao.findInvoicesByEventId(eventID);
	}
	@Override
	public void deleteAllInvoices(){
		invoiceDao.deleteAllInvoices();
	}
	

}
