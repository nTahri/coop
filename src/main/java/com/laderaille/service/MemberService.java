package com.laderaille.service;

import java.util.Date;
import java.util.List;

import com.laderaille.domain.Member;

public interface MemberService {
	
	
	Member findById (int id);
	
	Member findByMail (String mail);
	
	void saveMember (Member member);
	
	void updateMember (Member member);
	
	void deleteMemberByMail (String mail);
	
	void deleteMemberByID (Integer id);
	
	List<Member> findAllMembers();
	
	List<Member> findFormerMembers();
	
	List<Member> findAllMemberBetweenJoiningDate(Date stardDate, Date endDate);
	
//	boolean isMemberMailUnique(Integer id, String mail);
	void deleteAllMembers();
	
}
