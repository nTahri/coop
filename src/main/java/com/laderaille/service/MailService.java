package com.laderaille.service;

import java.io.File;
import java.util.List;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("mailService")


public class MailService {
	
	@Autowired
	JavaMailSender eMailSender; 
	
	 
	@Transactional
	/**
	 * This function used for sending mail with attachments to one or many mail adress
	 * @param toAddress
	 * @param fromAddress
	 * @param subject
	 * @param msgBody
	 * @param attachments
	 * @throws MessagingException
	 */
	public void sendEmail(String[] toAddress, String fromAddress, String subject, String msgBody,
			List<File> attachments) throws MessagingException {
		
		//Create message to send
		MimeMessage mimeMessage = eMailSender.createMimeMessage();
		MimeMessageHelper eMailmessage = new MimeMessageHelper(mimeMessage, true);
		// Add mail informations
		eMailmessage.setFrom(fromAddress);
		eMailmessage.setTo(toAddress);
		eMailmessage.setSubject(subject);
		eMailmessage.setText(msgBody);
		//add attachments
		for (File file : attachments) {
            FileSystemResource attachementFile = new FileSystemResource(file);
            eMailmessage.addAttachment(file.getName(), attachementFile);
        }
		
		try {
			eMailSender.send(mimeMessage);

		} catch (MailException ex) {
	            System.err.println(ex.getMessage());
	    }
	}
}