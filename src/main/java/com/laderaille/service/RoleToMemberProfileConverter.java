package com.laderaille.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.laderaille.domain.MemberProfile;

/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class RoleToMemberProfileConverter implements Converter<Object, MemberProfile>{

	static final Logger logger = LoggerFactory.getLogger(RoleToMemberProfileConverter.class);
	
	@Autowired
	MemberProfileService memberProfileService;

	/**
	 * Gets UserProfile by Id
	 * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
	 */
	public MemberProfile convert(Object element) {
		Integer id = Integer.parseInt((String)element);
		MemberProfile profile= memberProfileService.findById(id);
		logger.info("Profile : {}",profile);
		return profile;
	}
	
}