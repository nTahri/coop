package com.laderaille.service;

import java.util.List;
import com.laderaille.domain.InvoiceLine;

public interface InvoiceLineService {
	InvoiceLine findInvoiceLineById (int id);
	List<InvoiceLine> findInvoiceLinesByInvoiceID(int id);
	void saveInvoiceLine(InvoiceLine tickeline);
	
	void updateInvoiceLine(InvoiceLine tickeline);
	
	void deleteInvoiceLineByID(Integer id);
	
	List<InvoiceLine> findAll();
	
	InvoiceLine findInvoiceLineByInvoiceAndProductID(int invoiceID, String productID);
	
	List<InvoiceLine> findAllInvoiceLineByInvoiceID(int invoiceID);
	void deleteAllInvoiceLines();
}
