package com.laderaille.repository;

import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.laderaille.domain.Invoice;


@Repository("invoiceDao")
public class InvoiceDaoImp extends AbstractDataAnnotationObject<Integer, Invoice> implements InvoiceDao {
	static final Logger loggerInvoice = LoggerFactory.getLogger(InvoiceDaoImp.class);

	@Override
	public Invoice findInvoiceById(int id) {
		Invoice invoice = getByKey(id);
		if(invoice!=null){
			Hibernate.initialize(invoice);
		}
		return invoice;
	}

	@Override
	public Invoice findInvoiceByBuyerMail(String mail) {
		loggerInvoice.info("Invoice by Buyer mail : {}", mail);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("buyerName", mail));
		Invoice invoice = (Invoice)crit.uniqueResult();
		if(invoice!=null){
			Hibernate.initialize(invoice);
		}
		return invoice;
	}

	@Override
	public Invoice findInvoiceBySellerMail(String mail) {
		loggerInvoice.info("Invoice by Buyer mail : {}", mail);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("sellerName", mail));
		Invoice invoice = (Invoice)crit.uniqueResult();
		if(invoice!=null){
			Hibernate.initialize(invoice);
		}
		return invoice;
	}

	@Override
	public void saveInvoice(Invoice invoice) {
		persist(invoice);
	}

	@Override
	public void updateInvoice(Invoice invoice) {
		update(invoice);
	}

	@Override
	public void deleteInvoiceById(Integer id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Invoice invoice = (Invoice)crit.uniqueResult();
		delete(invoice);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findAllInvoices() {
		Criteria criteria = createEntityCriteria();
		return (List<Invoice>) criteria.list();	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findAllOpenInvoices() {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("invoiceStatus", 0));
		return (List<Invoice>) criteria.list();	
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findAllInvoiceBetweenDate(Date stardDate, Date endDate) {
		 Criteria crit = createEntityCriteria();
		 crit.add(Restrictions.gt("transactionDate", stardDate));
		 crit.add(Restrictions.lt("transactionDate", endDate));
		return (List<Invoice>) crit.list() ;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findOpenInvoiceBetweenDate(Date stardDate, Date endDate) {
		 Criteria crit = createEntityCriteria();
		 crit.add(Restrictions.eq("invoiceStatus", 0));
		 crit.add(Restrictions.gt("transactionDate", stardDate));
		 crit.add(Restrictions.lt("transactionDate", endDate));
		return (List<Invoice>) crit.list() ;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findAllInvoiceByDate(Date date) {
		 Criteria crit = createEntityCriteria();
		 crit.add(Restrictions.eq("transactionDate", date));
		 return (List<Invoice>) crit.list() ;
	}

	/*
	 * (non-Javadoc)
	 * @see com.laderaille.repository.InvoiceDao#findPriorInvoicesClosedThisDay(java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findInvoicesClosedThisDay(Date date) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("paiementDate", date));
		return (List<Invoice>) crit.list() ;
	}

	@Override
	public Invoice findByEventId(int eventId) {
		loggerInvoice.info("Invoice by event id : {}", eventId);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("eventId", eventId));
		Invoice invoice = (Invoice)crit.uniqueResult();
		if(invoice!=null){
			Hibernate.initialize(invoice);
		}
		return invoice;
	}

	@Override
	public List<Invoice> findPriorInvoicesClosedThisDay(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Invoice> findInvoicesByEventId(int eventId) {
		
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("eventId",eventId));
 		return (List<Invoice>) crit.list();
	}	
	@Override
	public void deleteAllInvoices(){
		deleteAll(createEntityCriteria().list());
	}
	
}
