package com.laderaille.repository;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import com.laderaille.domain.Event;

@Repository("eventDao")
public class EventDaoImp extends AbstractDataAnnotationObject<Integer, Event> implements EventDao {

	@Override
	public void saveEvent(Event event) {
		persist(event);
	}

	@Override
	public void updateEvent(Event event) {
		update(event);
	}

	@Override
	public void deleteEventByID(Integer id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Event event = (Event) crit.uniqueResult();
		delete(event);
	}

	@Override
	public Event findById(int id) {
		Event event = getByKey(id);
		return event;
	}

	@Override
	public Event findEventByNameAndStartDate(String name, Date startDate) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		crit.add(Restrictions.eq("startDate", startDate));
		Event event = (Event) crit.uniqueResult();
		return event;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void deleteAllEvents(){
		deleteAll(createEntityCriteria().list());
	}
	@Override
	public List<Event> findAllEvents() {
		List<Event> newEvents = new ArrayList<>();
		Criteria criteria = createEntityCriteria();
		@SuppressWarnings("unchecked")
		List<Event> events = criteria.list();
		if(events != null)
		{
			for (Event event  : events) {
				if(!newEvents.contains(event)){
					newEvents.add(event);
				}					
			}			
		}
		return newEvents;
	}
	/*/
	@Override
	public List<Event> findAllEvents() {
		Criteria criteria = createEntityCriteria();
		return (List<Event>) criteria.list();

	}
	/*/
	@SuppressWarnings("unchecked")
	@Override
	public List<Event> findAllDaylyEvents(Date startDate, Date endDate) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.gt("startDate", startDate));
		crit.add(Restrictions.lt("startDate", endDate));
		List<Event> newEvents = new ArrayList<>();
		List<Event> events = crit.list();
		if(events != null)
		{
			for (Event event  : events) {
				if(!newEvents.contains(event)){
					newEvents.add(event);
				}					
			}			
		}
		return newEvents;
	}

}
