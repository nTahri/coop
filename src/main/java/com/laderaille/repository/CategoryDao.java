package com.laderaille.repository;

import java.util.List;
import com.laderaille.domain.Category;


public interface CategoryDao {
	Category findById(int id);
	
	Category findByName (String name);

	void saveCategory(Category category);
	
	void deleteByName(String name);
	
	void deleteById(Integer id);
	
	List<Category> findAll();
	void deleteAllCategories();
}
