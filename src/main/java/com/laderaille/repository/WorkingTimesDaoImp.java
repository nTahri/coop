
package com.laderaille.repository;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;

@Repository("WorkingTimesDao")
@Transactional
public class WorkingTimesDaoImp extends AbstractDataAnnotationObject<Integer, WorkingTimes> implements WorkingTimesDao {

	static final Logger logger = LoggerFactory.getLogger(WorkingTimesDaoImp.class);

	@Override
	public void saveWorkingTimes(WorkingTimes workingTimes) {
		getSession().save(workingTimes);
	}

	@Override
	public void editWorkingTimes(WorkingTimes workingTimes) {
		getSession().update(workingTimes);
	}

	@Override
	public void deleteWorkingTimesByID(int workingTimesID) {
		getSession().delete(getWorkingTimesByID(workingTimesID));
	}

	@Override
	public WorkingTimes getWorkingTimesByID(int workingTimesID) {
		return (WorkingTimes) getSession().get(WorkingTimes.class, workingTimesID);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkingTimes> getAllWorkingTimes() {
		return (List<WorkingTimes>) getSession().createQuery("from WorkingTimes").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkingTimes> findAllWorkingTimeBetweenDate(Member member, Date stardDate, Date endDate) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("member.id", member.getId()));
		crit.add(Restrictions.gt("startDateTime", stardDate));
		crit.add(Restrictions.lt("startDateTime", endDate));
		return (List<WorkingTimes>) crit.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<WorkingTimes> findAllWorkingTimeByMember(Member member) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("member.id", member.getId()));
		return (List<WorkingTimes>) crit.list();
	}
}