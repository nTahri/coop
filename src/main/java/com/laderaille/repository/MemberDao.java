package com.laderaille.repository;

import java.util.Date;
import java.util.List;
import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes; 

public interface MemberDao {
	
	Member findById(int id);
	
	Member findMemberByMail (String mail);

	void saveMember(Member Member);
	
	void updateMember(Member Member);
	
	void deleteMemberByMail(String mail);
	
	void deleteMemberByID(Integer id);
	
	List<Member> findAllMembers();
	
	List<Member> findFormerMembers();
	
	List<Member> findAllMemberBetweenJoiningDate(Date stardDate, Date endDate);

	void deleteAllMembers();

	
}
