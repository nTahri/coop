package com.laderaille.repository;

import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import com.laderaille.domain.*;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Repository("memberDao")
public class MemberDaoImp extends AbstractDataAnnotationObject<Integer, Member> implements MemberDao{

	static final Logger logger = LoggerFactory.getLogger(MemberDaoImp.class);	
	
	
	public Member findById(int id) {
		Member member = getByKey(id);
		if(member!=null){
			Hibernate.initialize(member.getMemberProfiles());
		}
		return member;
	}
	
	@Override
	public Member findMemberByMail(String mail) {

		logger.info("MAIL : {}", mail);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("mail", mail));
		Member member = (Member)crit.uniqueResult();
		if(member!=null){
			Hibernate.initialize(member.getMemberProfiles());
		}
		return member;
	}
	
	@Override
	public void saveMember(Member Member) {
		persist(Member);
	}

	@Override
	public void updateMember(Member Member) {
		update(Member);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Member> findAllMembers() {
		Criteria criteria = createEntityCriteria();
		return (List<Member>) criteria.list();	
	}


	@Override
	public void deleteMemberByMail(String mail) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("mail", mail));
		Member member = (Member)crit.uniqueResult();
		delete(member);
		
	}
	
	@Override
	public void deleteMemberByID(Integer id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Member member = (Member)crit.uniqueResult();
		delete(member);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Member> findFormerMembers() {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.isNotNull("leftDate"));
		return (List<Member>) criteria.list();	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Member> findAllMemberBetweenJoiningDate(Date stardDate, Date endDate) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.gt("joiningDate", stardDate));
		crit.add(Restrictions.lt("joiningDate", endDate));
		return (List<Member>) crit.list();
	}

	@Override
	public void deleteAllMembers(){
		Member admin = this.findMemberByMail("admin@laderaille.ca");
		
		Query queryProfile = getSession().createSQLQuery("delete from member_member_profile where MEMBER_ID <> :MEMBER_ID");
		queryProfile.setInteger("MEMBER_ID", admin.getId());
		queryProfile.executeUpdate();
        
		Query query = getSession().createSQLQuery("delete from member where mail <> :mail");
        query.setString("mail", "admin@laderaille.ca");
        query.executeUpdate();
//		Criteria crit = createEntityCriteria();
//		crit.add(Restrictions.sqlRestriction("SELECT * FROM MEMBER WHERE MAIL <> 'admin@laderaille.ca'"));
//		List<Member> members = (List<Member>)crit.list();
//		for(Member m: members){
//			delete(m);
//		}
	}
}
