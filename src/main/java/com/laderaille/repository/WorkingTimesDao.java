package com.laderaille.repository;

import java.util.Date;
import java.util.List;

import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;

public interface WorkingTimesDao {

	public WorkingTimes getWorkingTimesByID(int workingTimesID);

	public void saveWorkingTimes(WorkingTimes workingTimes);

	public void deleteWorkingTimesByID(int workingTimesID);
	
	public void editWorkingTimes(WorkingTimes workingTimes);
	
	public List<WorkingTimes> getAllWorkingTimes();	

	public List<WorkingTimes> findAllWorkingTimeBetweenDate(Member member, Date stardDate, Date endDate);
	
	public List<WorkingTimes> findAllWorkingTimeByMember(Member member);
}