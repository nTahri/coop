package com.laderaille.repository;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Category;

@Repository("categoryDao")
@Transactional
public class CategoryDaoImp extends AbstractDataAnnotationObject<Integer, Category> implements CategoryDao {
	static final Logger loggerCategory = LoggerFactory.getLogger(CategoryDaoImp.class);
	@Override
	public Category findById(int id) {
		Category category = getByKey(id);
		if(category!=null){
			Hibernate.initialize(category);
		}
		return category;
	}

	@Override
	public Category findByName(String name) {
		loggerCategory.info("Category Name : {}", name);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Category category = (Category)crit.uniqueResult();
		if(category!=null){
			Hibernate.initialize(category.getName());
		}
		return category;
	}

	@Override
	public void saveCategory(Category category) {
		persist(category);
	}

	@Override
	public void deleteByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		Category category = (Category)crit.uniqueResult();
		delete(category);
	}

	@Override
	public void deleteById(Integer id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Category category = (Category)crit.uniqueResult();
		delete(category);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> findAll() {
		Criteria crit = createEntityCriteria();
		
		return (List<Category>)crit.list();
	}
	@Override
	public void deleteAllCategories(){
		deleteAll(createEntityCriteria().list());
	}

}
