package com.laderaille.repository;

import com.laderaille.domain.Tax;

public interface TaxDao {
	Tax findTaxById(int id);
	double getTps(int id);
	double getTvq(int id);
}
