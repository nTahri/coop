package com.laderaille.repository;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.InvoiceLine;

@Repository("invoiceLineDao")
public class InvoiceLineDaoImp extends AbstractDataAnnotationObject<Integer, InvoiceLine> implements InvoiceLineDao {
	static final Logger loggerInvoiceLine = LoggerFactory.getLogger(InvoiceLineDaoImp.class);
	@Override
	public InvoiceLine findInvoiceLineByID(int id) {
		InvoiceLine invoiceLine = getByKey(id);
		if(invoiceLine!=null){
			Hibernate.initialize(invoiceLine);
		}
		return invoiceLine;
	}
	@Override
	public List<InvoiceLine> findInvoiceLinesByInvoiceID(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("invoiceID", id));
		List<InvoiceLine> invoiceLine = (List<InvoiceLine>)crit.list();
		if(invoiceLine!=null){
			Hibernate.initialize(invoiceLine);
		}
		return invoiceLine;
	}

	@Override
	public void saveInvoiceLine(InvoiceLine invoiceLine) {
		persist(invoiceLine);
	}

	@Override
	public void deleteInvoiceLineByID(Integer id) {
		
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		InvoiceLine invoiceLine = (InvoiceLine)crit.uniqueResult();
		loggerInvoiceLine.info("InvoiceLine to delete : {}", invoiceLine.toString());
		
		delete(invoiceLine);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceLine> findAllInvoice() {
		Criteria crit = createEntityCriteria();
		return (List<InvoiceLine>)crit.list();
	}
	
	@Override
	public InvoiceLine findInvoiceLineByInvoiceAndProductID(int invoiceID, String productID) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("invoiceID", invoiceID));
		crit.add(Restrictions.eq("productId", productID));
		return (InvoiceLine)crit.uniqueResult();		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceLine> findAllInvoiceLineByInvoiceID(int invoiceID) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("invoiceID", invoiceID));
		return (List<InvoiceLine>)crit.list();		
	}
	@Override
	public void deleteAllInvoiceLines(){
		deleteAll(createEntityCriteria().list());
	}
}
