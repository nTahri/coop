package com.laderaille.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.laderaille.domain.EventName;
@Repository("EventNameDao")
public class EventNameDaoImp extends AbstractDataAnnotationObject<Integer, EventName>  implements EventNameDao {

	@Override
	public EventName findById(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		
		return (EventName) crit.uniqueResult();
	}

	@Override
	public EventName findByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("name", name));
		
		return (EventName) crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<EventName> findAllEventsName() {
		Criteria criteria = createEntityCriteria();
		return (List<EventName>) criteria.list();
	}
	@SuppressWarnings("unchecked")
	@Override
	public void deleteAllEventName(){
		deleteAll(createEntityCriteria().list());
	}
}
