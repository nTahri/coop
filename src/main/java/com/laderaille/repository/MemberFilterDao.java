package com.laderaille.repository;

import java.util.List;

import com.laderaille.domain.MemberFilter;

public interface MemberFilterDao {
		
		List<MemberFilter> getAllCriteria();	

}
