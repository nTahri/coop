package com.laderaille.repository;


import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import com.laderaille.domain.Tax;
@Repository("TaxDao")
public class TaxDaoImp extends AbstractDataAnnotationObject<Integer, Tax>implements TaxDao {

	@Override
	public Tax findTaxById(int id) {
		Tax tax = getByKey(id);
		if(tax !=null){
			Hibernate.initialize(tax.getId());
		}
		return tax;
	}
	@Override
	public double getTps(int id){
		return getByKey(id).getTps();
	}
	@Override
	public double getTvq(int id){
		return getByKey(id).getTvq();
	}
}
