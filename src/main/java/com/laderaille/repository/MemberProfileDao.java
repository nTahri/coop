package com.laderaille.repository;

import java.util.List;

import com.laderaille.domain.MemberProfile;

public interface MemberProfileDao {

	List<MemberProfile> findAll();
	
	MemberProfile findByType(String type);
	
	MemberProfile findById(int id);
	void deleteAllMembersProfile();
}
