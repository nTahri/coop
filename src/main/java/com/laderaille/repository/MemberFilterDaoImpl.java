package com.laderaille.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.MemberFilter;

@Repository("MemberFilterDao")
@Transactional
public class MemberFilterDaoImpl extends AbstractDataAnnotationObject<Integer, MemberFilter> 
implements MemberFilterDao {

	static final Logger logger = LoggerFactory.getLogger(MemberFilterDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<MemberFilter> getAllCriteria() {
		Criteria crit = createEntityCriteria();
		return (List<MemberFilter>) crit.list();
	}

}