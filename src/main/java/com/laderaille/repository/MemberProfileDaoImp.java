package com.laderaille.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.laderaille.domain.MemberProfile;

@Repository("memberProfileDao")
public class MemberProfileDaoImp extends AbstractDataAnnotationObject<Integer, MemberProfile>
		implements MemberProfileDao {

	public MemberProfile findById(int id) {
		return getByKey(id);
	}

	public MemberProfile findByType(String type) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("type", type));
		return (MemberProfile) crit.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<MemberProfile> findAll() {
		Criteria crit = createEntityCriteria();
		crit.addOrder(Order.asc("type"));
		return (List<MemberProfile>) crit.list();
	}
	public void deleteAllMembersProfile(){
		deleteAll(createEntityCriteria().list());
	}
}
