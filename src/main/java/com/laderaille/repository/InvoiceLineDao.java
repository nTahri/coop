package com.laderaille.repository;

import java.util.List;
import com.laderaille.domain.InvoiceLine;

public interface InvoiceLineDao {
	InvoiceLine findInvoiceLineByID (int id);
	List<InvoiceLine> findInvoiceLinesByInvoiceID(int id);
	void saveInvoiceLine(InvoiceLine tickeline);
	
	void deleteInvoiceLineByID(Integer id);
	
	List<InvoiceLine> findAllInvoice();
	
	InvoiceLine findInvoiceLineByInvoiceAndProductID(int invoiceID, String productID);
	
	List<InvoiceLine> findAllInvoiceLineByInvoiceID(int invoiceID);
	void deleteAllInvoiceLines();
}
