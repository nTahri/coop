package com.laderaille.repository;

import java.util.Date;
import com.laderaille.domain.ClosingCash;

public interface  ClosingCashDao {
	
	ClosingCash findClosingCashById(int id);
	
	ClosingCash findClosingCashByDate(Date date);
	
	ClosingCash findLastClosingCash(Date date);

	void saveClosingCash(ClosingCash closingCash);

	public void deleteClosingCashByDate(Date date);
	
	public void editClosingCash(ClosingCash closingCash);
	
	public void deleteAllClosingCashs();
}
