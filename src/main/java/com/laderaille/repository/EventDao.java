package com.laderaille.repository;


import java.util.Date;
import java.util.List;

import com.laderaille.domain.Event;

public interface EventDao {

	void saveEvent(Event event);
	
	void updateEvent(Event event);

	void deleteEventByID(Integer id);

	Event findById(int id);

	Event findEventByNameAndStartDate(String name, Date statDate);

	List<Event> findAllEvents();
	
	List<Event> findAllDaylyEvents(Date startDate, Date endDate);
	void deleteAllEvents();
}
