package com.laderaille.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.Product;

@Repository("productDao")
@Transactional
public class ProductDaoImp extends AbstractDataAnnotationObject<Integer, Product> implements ProductDao {
	static final Logger loggerProduct = LoggerFactory.getLogger(ProductDaoImp.class);
	@Override
	public Product findById(int id) {
		Product product = getByKey(id);
		if(product!=null){
			Hibernate.initialize(product);
		}
		return product;
	}

	@Override
	public Product findByCode(String code) {
		loggerProduct.info("Product Name : {}", code);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("productCode", code));
		Product product = (Product)crit.uniqueResult();
		if(product!=null){
			Hibernate.initialize(product.getProductName());
		}
		return product;
	}

	@Override
	public Product findProductByName(String name) {
		loggerProduct.info("Product Name : {}", name);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("productName", name));
		Product product = (Product)crit.uniqueResult();
		if(product!=null){
			Hibernate.initialize(product.getProductName());
		}
		return product;
	}

	@Override
	public void saveProduct(Product product) {
			persist(product);
	}

	@Override
	public void deleteProductByName(String name) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("productName", name));
		Product product = (Product)crit.uniqueResult();
		delete(product);
	}

	@Override
	public void deleteProductByID(Integer id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		Product product = (Product)crit.uniqueResult();
		delete(product);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findAllProducts() {
		Criteria crit = createEntityCriteria();
		return (List<Product>)crit.list();
	}

	@Override
	public void updateProduct(Product product) {
		update(product);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void deleteAllProducts(){
		deleteAll(createEntityCriteria().list());
	}
}
