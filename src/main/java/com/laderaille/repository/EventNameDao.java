package com.laderaille.repository;

import java.util.List;

import com.laderaille.domain.EventName;

public interface EventNameDao {
	EventName findById(int id);
	EventName findByName(String name);
	List<EventName> findAllEventsName();
	void deleteAllEventName();
}
