package com.laderaille.repository;

import java.util.Date;
import java.util.List;
import com.laderaille.domain.Invoice;

public interface InvoiceDao {
	// Invoice findInvoiceById(int id);

	Invoice findInvoiceById(int invoiceID);

	Invoice findInvoiceByBuyerMail(String mail);

	Invoice findInvoiceBySellerMail(String mail);

	void saveInvoice(Invoice invoice);

	void updateInvoice(Invoice invoice);

	void deleteInvoiceById(Integer id);

	List<Invoice> findAllInvoices();

	List<Invoice> findAllOpenInvoices();

	List<Invoice> findAllInvoiceBetweenDate(Date stardDate, Date endDate);

	List<Invoice> findOpenInvoiceBetweenDate(Date stardDate, Date endDate);

	List<Invoice> findAllInvoiceByDate(Date date);

	/*
	 * Find all invoices closed this day.
	 * 
	 * @param : Date date
	 */
	List<Invoice> findInvoicesClosedThisDay(Date date);

	List<Invoice> findPriorInvoicesClosedThisDay(Date date);

	Invoice findByEventId(int eventId);

	List <Invoice> findInvoicesByEventId (int eventId);
	void deleteAllInvoices();
}
