package com.laderaille.repository;

import java.util.Date;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.laderaille.domain.ClosingCash;
import com.laderaille.domain.WorkingTimes;

@Repository("ClosingCashDAO")
@Transactional
public class ClosingCashDAOImpl extends AbstractDataAnnotationObject<Integer, ClosingCash> implements ClosingCashDao {

	static final Logger logger = LoggerFactory.getLogger(ClosingCashDAOImpl.class);

	@Override
	public ClosingCash findClosingCashById(int id) {
		return (ClosingCash) getSession().get(ClosingCash.class, id);
	}

	@Override
	public ClosingCash findClosingCashByDate(Date date) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("closingDate", date));
		return (ClosingCash)crit.uniqueResult();
	}

	@Override
	public void saveClosingCash(ClosingCash closingCash) {
		getSession().save(closingCash);		
	}

	@Override
	public void deleteClosingCashByDate(Date date) {
		getSession().delete(findClosingCashByDate(date));		
	}

	@Override
	public void editClosingCash(ClosingCash closingCash) {
		getSession().update(closingCash);		
	}

	@Override
	public ClosingCash findLastClosingCash(Date date) {
		DetachedCriteria maxDateQuery = DetachedCriteria.forClass(ClosingCash.class);
		ProjectionList proj = Projections.projectionList();
		proj.add(Projections.max("closingDate"));
		maxDateQuery.setProjection(proj);
		
		
		
		/*/
		
		Criterion exclusion = Restrictions.conjunction().add(Restrictions.eq("closingDate", date));
		
		
		DetachedCriteria excludes = DetachedCriteria.forClass(ClosingCash.class);
		excludes.add(Restrictions.disjunction().add(exclusion));
		excludes.setProjection(Projections.property("id"));
		/*/
		
		Criteria crit = createEntityCriteria();	
		//crit.add(Property.forName("id").notIn(excludes));


		crit.add(Subqueries.propertiesEq(new String[] {"closingDate"}, maxDateQuery));
		return (ClosingCash)crit.uniqueResult();
	}
	
	@Override
	public void deleteAllClosingCashs(){
		deleteAll(createEntityCriteria().list());
	}

}
