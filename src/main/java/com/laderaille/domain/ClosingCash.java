package com.laderaille.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "closingcash")
public class ClosingCash {	

		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
		
		@ManyToOne
	    @JoinColumn(name = "MEMBERID", nullable = false)
		private Member member;	
		
		@ManyToOne
	    @JoinColumn(name = "OPENCASHMEMBER")
		private Member openCashMember;		
		
		@NotNull
		@Temporal(TemporalType.TIMESTAMP)
		@Column(name = "CLOSINDGDATE", nullable = false, unique=true)
		private Date closingDate;		
		
		@Column(name = "FIVECENT")
		private int fiveCent;
		
		@Column(name = "TENCENT")
		private int tenCent;
		
		@Column(name = "TWENTYFIVECENT")
		private int twentyFiveCent;	
		
		@Column(name = "ONEDOLLARS")
		private int oneDollars;
		
		@Column(name = "TWODOLLARS")
		private int twoDollars;
		
		@Column(name = "FIVEDOLLARS")
		private int fiveDollars;
		
		@Column(name = "TENDOLLARS")
		private int tenDollars;
		
		@Column(name = "TWENTYDOLLARS")
		private int twentyDollars;
		
		@Column(name = "FIFTYDOLLARS")
		private int fiftyDollars;
		
		@Column(name = "HUNDREDDOLLARS")
		private int hundredDollars;
		
		@Column(name = "TOTAL")
		private double total;
		
		@Column(name = "CLOSINGDIFFERENCE")
		private double closingDifference;	
		
		@Column(name = "OPENDIFFERENCE")
		private double openDifference;	
		 
		@Column(name = "CLOSINGCOMMENTS")
		private String closingComments;	
		
		@Column(name = "OPENCOMMENTS")
		private String openComments;
		
		public Member getOpenCashMember() {
			return openCashMember;
		}

		public void setOpenCashMember(Member openCashMember) {
			this.openCashMember = openCashMember;
		}

		
		public double getClosingDifference() {
			return closingDifference;
		}

		public void setClosingDifference(double closingDifference) {
			this.closingDifference = closingDifference;
		}

		public double getOpenDifference() {
			return openDifference;
		}

		public void setOpenDifference(double openDifference) {
			this.openDifference = openDifference;
		}	
		
		public ClosingCash() {
		}		
		
		public String getClosingComments() {
			return closingComments;
		}

		public void setClosingComments(String closingComments) {
			this.closingComments = closingComments;
		}

		public String getOpenComments() {
			return openComments;
		}

		public void setOpenComments(String openComments) {
			this.openComments = openComments;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public Member getMember() {
			return member;
		}

		public void setMember(Member member) {
			this.member = member;
		}

		public Date getClosingDate() {
			return closingDate;
		}

		public void setClosingDate(Date closingDate) {
			this.closingDate = closingDate;
		}

		public int getFiveCent() {
			return fiveCent;
		}

		public void setFiveCent(int fiveCent) {
			this.fiveCent = fiveCent;
		}

		public int getTenCent() {
			return tenCent;
		}

		public void setTenCent(int tenCent) {
			this.tenCent = tenCent;
		}

		public int getTwentyFiveCent() {
			return twentyFiveCent;
		}

		public void setTwentyFiveCent(int twentyFiveCent) {
			this.twentyFiveCent = twentyFiveCent;
		}		

		public int getOneDollars() {
			return oneDollars;
		}

		public void setOneDollars(int oneDollars) {
			this.oneDollars = oneDollars;
		}

		public int getTwoDollars() {
			return twoDollars;
		}

		public void setTwoDollars(int twoDollars) {
			this.twoDollars = twoDollars;
		}

		public int getFiveDollars() {
			return fiveDollars;
		}

		public void setFiveDollars(int fiveDollars) {
			this.fiveDollars = fiveDollars;
		}

		public int getTenDollars() {
			return tenDollars;
		}

		public void setTenDollars(int tenDollars) {
			this.tenDollars = tenDollars;
		}

		public int getTwentyDollars() {
			return twentyDollars;
		}

		public void setTwentyDollars(int twentyDollars) {
			this.twentyDollars = twentyDollars;
		}

		public int getFiftyDollars() {
			return fiftyDollars;
		}

		public void setFiftyDollars(int fiftyDollars) {
			this.fiftyDollars = fiftyDollars;
		}

		public int getHundredDollars() {
			return hundredDollars;
		}

		public void setHundredDollars(int hundredDollars) {
			this.hundredDollars = hundredDollars;
		}

		public double getTotal() {
			return total;
		}

		public void setTotal(double total) {
			this.total = total;
		}

		@Override
		public String toString() {
			return "ClosingCash [id=" + id + ", member=" + member + ", openCashMember=" + openCashMember
					+ ", closingDate=" + closingDate + ", fiveCent=" + fiveCent + ", tenCent=" + tenCent
					+ ", twentyFiveCent=" + twentyFiveCent + ", oneDollars=" + oneDollars + ", twoDollars=" + twoDollars
					+ ", fiveDollars=" + fiveDollars + ", tenDollars=" + tenDollars + ", twentyDollars=" + twentyDollars
					+ ", fiftyDollars=" + fiftyDollars + ", hundredDollars=" + hundredDollars + ", total=" + total
					+ ", closingDifference=" + closingDifference + ", openDifference=" + openDifference
					+ ", closingComments=" + closingComments + ", openComments=" + openComments + "]";
		}	
		
		
}
