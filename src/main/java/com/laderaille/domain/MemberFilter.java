package com.laderaille.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "memberfilter")
public class MemberFilter {	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "CRITERIA", nullable = false, unique=true)
	private String searchCriteria;

	public MemberFilter() {
	}

	public MemberFilter(int id, String searchCriteria) {
		this.id = id;
		this.searchCriteria = searchCriteria;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	@Override
	public String toString() {
		return "memberFilter [id=" + id + ", searchCriteria=" + searchCriteria + "]";
	}	
	
}
