package com.laderaille.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the CATEGORY attributes. 
 * 
 * <p>
 * The CATEGORY object is persisted in the table CATEGORY in the database.
 * 
 * <p>
 * The toString method in this class allows the user to get the CATEGORY object in
 * a textual form.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Entity
@Table(name = "categories")
public class Category implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotNull
	@Size(min = 3, max = 255)
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@OneToOne
    @JoinColumn(name = "ID", nullable = true)
    private Category categoryParent;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategoryParent() {
		return categoryParent;
	}

	public void setCategoryParent(Category categoryParent) {
		this.categoryParent = categoryParent;
	}

	@Override
	public String toString() {
		return "Category [name=" + name + "]";
	}
	
	
}
