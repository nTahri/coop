package com.laderaille.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the PERSISTENT_LOGIN attributes. 
 * 
 * <p>
 * The PERSISTENLOGIN object is persisted in the table PERSISTENT_LOGINS in the database, it is
 * stored to keep track of the MEMBER who is using the program and avoid requesting the 
 * credentials from the user when he jumps from one module to another of the program.
 * 
 * <p>
 * The Token attribute is unique and the same one is used as far as the session is opened.
 * Once a session is closed, reopening the program (new session) will create a new token
 * totally different from the previous one even if it is the same user (MEMBER).
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Entity
@Table(name="PERSISTENT_LOGINS")
public class PersistentLogin implements Serializable{

	@Id
	private String series;

	@Column(name="MAIL", unique=true, nullable=false)
	private String username;
	
	@Column(name="TOKEN", unique=true, nullable=false)
	private String token;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date last_used;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getLast_used() {
		return last_used;
	}

	public void setLast_used(Date last_used) {
		this.last_used = last_used;
	}
	
	
}
