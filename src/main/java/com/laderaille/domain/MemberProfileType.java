package com.laderaille.domain;

import java.io.Serializable;

public enum MemberProfileType implements Serializable{
	MEMBER("MEMBER"),
	VOLUNTEER("VOLUNTEER"),
	STUDENT("STUDENT"),
	ADMIN("ADMIN");
	
	String memberProfileType;
	
	private MemberProfileType(String memberProfileType){
		this.memberProfileType = memberProfileType;
	}
	
	public String getMemberProfileType(){
		return memberProfileType;
	}
	
}
