package com.laderaille.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the PRODUCT attributes. 
 * 
 * <p>
 * The PRODUCT object is persisted in the table PRODUCTS in the database, it is
 * identified by its id attribute which is unique and automatically generated.
 * 
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */


@Entity
@Table(name = "products")
public class Product implements Serializable {
	/**
	 * `products`.`CATEGORY`
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(min = 3, max = 255)
	@Column(name = "REFERENCE", nullable = false)
	private String productRef;

	@Size(min = 3, max = 255)
	@Column(name = "CODE", nullable = false)
	private String productCode;

	@Size(min = 3, max = 255)
	@Column(name = "NAME", nullable = false)
	private String productName;

	@Column(name = "PRICESELL", nullable = false)
	private double productSellPrice;

	@Column(name = "PRICEBUY", nullable = false)
	private double productPrice;

	@Column(name = "STOCK", nullable = false)
	private double productStock;

	@Size(min = 3, max = 255)
	@Column(name = "TAXCAT", nullable = false)
	private String productTaxCat;

	@ManyToOne(cascade = CascadeType.DETACH)
	@JoinColumn(name = "CATEGORY")
	private Category productCategory;

	@Size(min = 3, max = 400)
	@Column(name = "DESCRIPTION", nullable = true)
	private String productDescription;

	@Column(name = "MINSTOCK", nullable = true)
	private double productStockMin;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductRef() {
		return productRef;
	}

	public void setProductRef(String productRef) {
		this.productRef = productRef;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductTaxCat() {
		return productTaxCat;
	}

	public void setProductTaxCat(String productTaxCat) {
		this.productTaxCat = productTaxCat;
	}

	public Category getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(Category productCategory) {
		this.productCategory = productCategory;
	}

	public double getProductSellPrice() {
		return productSellPrice;
	}

	public void setProductSellPrice(double productSellPrice) {
		this.productSellPrice = productSellPrice;
	}

	public double getProductStock() {
		return productStock;
	}

	public void setProductStock(double productStock) {
		this.productStock = productStock;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public double getProductStockMin() {
		return productStockMin;
	}

	public void setProductStockMin(double productStockMin) {
		this.productStockMin = productStockMin;
	}

}
