package com.laderaille.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the EVENT attributes. It contains another method that check if
 * the event can be updated or deleted and return a boolean. 
 * 
 * <p>
 * The event object is persisted in the table EVENTS in the database.
 * 
 * <p>
 * The combination of the name and the startdate of the event is unique in the
 * EVENTS table.
 * 
 * <p>
 * The toString method in this class allows the user to get the EVENT object in
 * a textual form.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Entity
@Table(name = "EVENTS", uniqueConstraints = { @UniqueConstraint(columnNames = { "NAME", "STARTDATE" }) })
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "NAME", nullable = false)
	private String name;

	// @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	// @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "STARTDATE", nullable = false)
	private Date startDate;

	// @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	// @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ENDDATE", nullable = false)
	private Date endDate;

	@Column(name = "DESCRIPTION", nullable = false)
	private String description;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "EVENTS_HAS_MEMBER", 
			joinColumns = { @JoinColumn(name = "EVENTS_ID") }, 
			inverseJoinColumns = { @JoinColumn(name = "MEMBER_ID") })
	private Set<Member> members = new HashSet<Member>();

	// @ManyToOne(fetch = FetchType.LAZY)
	@Column(name = "OWNER")
	private int ownerid;

	public int getOwnerId() {
		return this.ownerid;
	}

	public void setOwnerId(int memb) {
		this.ownerid = memb;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Member> getMembers() {
		return members;
	}

	public void setMembers(Set<Member> members) {
		this.members = members;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", description=" + description + "]";
	}

	public boolean canBeDeleted() {
		Date todayDate = new Date();
		Date eventEndDate = this.getEndDate();
		if (todayDate.before(eventEndDate)) {
			return true;
		} else
			return false;
	}
}
