package com.laderaille.domain;

public enum AgeIntervalEnum {
	ENFANTS("7-13 ans"),
	ADOLESCENTS("14-18 ans"),
    ADULTES("19-50 ans"),
    AINES("51+ ans");
	
	private String fullAgeInterval;

	
	private AgeIntervalEnum( String s )
	{
		fullAgeInterval = s;
	}
		
	public String getfullAgeInterval()
	{
		return fullAgeInterval;
	}
}
