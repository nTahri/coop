package com.laderaille.domain;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "invoiceline")
public class InvoiceLine implements Serializable  {
	/**
    `ticketlines`.`PRODUCT`, fk product
	 */
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Column(name = "INVOICEID", nullable = false)
	private int invoiceID;
	
	@NotNull
	@Column(name = "QUANTITY", nullable = false)
	private double quantity;
	
	@NotNull
	@Column(name = "PRICE", nullable = false)
	private double price;
	
	@NotNull
	@Column(name = "TVQ", nullable = false)
	private double tvq;
	
	@NotNull
	@Column(name = "TSQ", nullable = false)
	private double tsq;
	
	@NotNull
	@Column(name = "DISCOUNT_VALUE", nullable = false)
	private double discount;
	
	@NotNull
	@Column(name = "DISCOUNT_TYPE", nullable = false)
	private String discountType;
	

	// @ManyToOne(optional=false)
    // @JoinColumn(name="PRODUCTID", nullable=false, updatable=false)
    // public Product getProduct() { return this.product; }
	
	@NotNull
	@Column(name = "PRODUCTID", nullable = false)
	private String productId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInvoiceId() {
		return invoiceID;
	}

	public void setInvoiceId(int invoiceId) {
		this.invoiceID = invoiceId;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getTVQ() {
		return tvq;
	}

	public void setTVQ(double tvq) {
		this.tvq = tvq;
	}
	
	public double getTSQ() {
		return tsq;
	}

	public void setTSQ(double tsq) {
		this.tsq = tsq;
	}
	
	/*public void setProduct (Product product) {
		this.product = product;
	}
	public Product getProduct () {
		return this.product;
	}*/

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double disc) {
		this.discount = disc;
	}
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	
	public double getRealValue() {
		if (getDiscountType() == "%") {
			return getPrice() * getQuantity() - getDiscount();
		} else if (getDiscountType() == "$") {
			return getPrice() * getQuantity() - getPrice() * getQuantity() * getDiscount();
		}
		
		return getPrice() * getQuantity();
	}
}
