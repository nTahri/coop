package com.laderaille.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;
import com.laderaille.utils.Item;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the INVOICE attributes. 
 * 
 * <p>
 * The INVOICE object is persisted in the table INVOICE in the database.
 * it is always linked to an EVENT object.
 * 
 * <p>
 * The toString method in this class allows the user to get the INVOICE object
 * in a textual form.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "STATUS", nullable = false)
	private int invoiceStatus;

	@Size(min = 3, max = 50)
	@Column(name = "SELLER", nullable = false)
	private String sellerName;

	@Size(min = 3, max = 50)
	@Column(name = "BUYER", nullable = false)
	private String buyerName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TRANSACTIONDATE", nullable = false)
	private Date transactionDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PAIEMENTDATE")
	private Date paiementDate;

	@Column(name = "SUBTOTAL", nullable = false)
	private double subtotal;

	@Column(name = "TOTAL", nullable = false)
	private double total;

	@Column(name = "EVENTID", nullable = false)
	private int eventId;

	@Transient
	private List<Item> items = new ArrayList<Item>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInvoiceStatus() {
		return invoiceStatus;
	}

	public boolean getInvoiceStatusBoolean() {
		return invoiceStatus != 0;
	}

	public void setInvoiceStatus(int invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double invoiceSubtotal) {
		this.subtotal = invoiceSubtotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getPaiementDate() {
		return paiementDate;
	}

	public void setPaiementDate(Date paiementDate) {
		this.paiementDate = paiementDate;
	}

	public int getEventId() {
		return this.eventId;
	}

	public void setEventId(int id) {
		this.eventId = id;
	}

	@Override
	public String toString() {
		return "{id: " + id + ", invoiceStatus: " + invoiceStatus + ", sellerName: " + sellerName + ", buyerName: "
				+ buyerName + ", transactionDate: " + transactionDate + ", paiementDate: " + paiementDate + ", subtotal: "
				+ subtotal + ", total: " + total + ", eventId: " + eventId + "}";
	}

}
