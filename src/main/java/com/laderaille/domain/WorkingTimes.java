package com.laderaille.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "workingtimes")
public class WorkingTimes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
    @JoinColumn(name = "MEMBERID", nullable = false)
	private Member member;	
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "STARTDATETIME", nullable = false)
	private Date startDateTime;

	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ENDDATETIME", nullable = false)
	private Date endDateTime;

	public WorkingTimes()
	{		
		
	}	
	
	public WorkingTimes(Member member, Date startDateTime, Date endDateTime) {
		super();
		this.member = member;
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Member getMember() {
		return member;
	}


	public void setMember(Member member) {
		this.member = member;
	}


	public Date getStartDateTime() {
		return startDateTime;
	}


	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}


	public Date getEndDateTime() {
		return endDateTime;
	}


	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}


	@Override
	public String toString() {
		return "WorkingTimes [id=" + id + ", memberMail=" + member.getMail() + ", startDateTime=" + startDateTime
				+ ", endDateTime=" + endDateTime + "]";
	}	
}
