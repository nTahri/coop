package com.laderaille.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
//import com.laderaille.domain.MemberProfile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * This class consists exclusively of constructors, getters and setters methods that operate
 * on or return the MEMBER attributes.
 * 
 * <p>
 * The MEMBER object is persisted in the table MEMBER in the database.
 * 
 * <p>
 * The Id attribute is unique and is automatically generated. The mail attribute
 * is unique too and it is used as the key to use the program.
 * <p>
 * The toString method in this class allows the user to get the MEMBER object in
 * a textual form.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Entity
@Table(name = "MEMBER")
public class Member implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "FIRSTNAME", nullable = false)
	private String firstName;

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "LASTNAME", nullable = false)
	private String lastName;


	@Size(min = 3, max = 100)
	@Column(name = "ADDRESS", nullable = true)
	private String address;

	@NotNull
	@Column(name = "MAIL", unique = true, nullable = false)
	@Email
	@NotBlank
	private String mail;

	
	@Column(name = "FACEBOOKNAME", nullable = true)
	private String facebookName;

	@Column(name = "PHONE", nullable = true)
	private String phoneNumber;

	@Column(name = "STATUS", nullable = false)
	private boolean status;

	@Column(name = "STUDENTID", nullable = true)
	private String studentID;

//	@DateTimeFormat(pattern = "DD/MM/YYYY")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "JOININGDATE", nullable = false)
	private Date joiningDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LEFTDATE", nullable = true)
	private Date leftDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BIRTHDAY", nullable = true)
	private Date birthday;
	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRINGDATE", nullable = true)
	private Date expiringDate;
	
	
	public Date getLeftDate() {
		return leftDate;
	}

	public void setLeftDate(Date leftDate) {
		this.leftDate = leftDate;
	}
	
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

//	@Size(min = 3, max = 100)
	@Column(name = "PASSWORD", nullable = true)
	private String password;

	@Size(min = 0, max = 1)
	@Column(name = "SEX", nullable = true)
	private String sex;

	@NotEmpty
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "MEMBER_MEMBER_PROFILE", 
             joinColumns = { @JoinColumn(name = "MEMBER_ID") }, 
             inverseJoinColumns = { @JoinColumn(name = "MEMBER_PROFILE_ID") })
	private Set<MemberProfile> memberProfiles = new HashSet<MemberProfile>();	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getDisplayName() {
		return getLastName() + ", " + getFirstName();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getFacebookName() {
		return facebookName;
	}

	public void setFacebookName(String facebookName) {
		this.facebookName = facebookName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getStudentID() {
		return studentID;
	}

	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}

	public Date getJoiningDate() {
		return joiningDate;
	}

	public void setJoiningDate(Date joiningDate) {
		this.joiningDate = joiningDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Set<MemberProfile> getMemberProfiles() {
		return memberProfiles;
	}

	public void setMemberProfiles(Set<MemberProfile> memberProfiles) {
		this.memberProfiles = memberProfiles;
	}
	

	public Date getExpiringDate() {
		return expiringDate;
	}

	public void setExpiringDate(Date expiringDate) {
		this.expiringDate = expiringDate;
	}

	@Override
	public String toString() {
		return "Member [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
				+ ", mail=" + mail + ", facebookName=" + facebookName + ", phoneNumber=" + phoneNumber + ", status="
				+ status + ", studentID=" + studentID + ", joiningDate=" + joiningDate + ", leftDate=" + leftDate
				+ ", birthday=" + birthday + ", expiringDate=" + expiringDate + ", password=" + password + ", sex="
				+ sex + ", memberProfiles=" + memberProfiles + "]";
	}

	/*@Override
	public String toString() {
		return "Member [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", sex=" + sex
				+ ", address=" + address + ", mail=" + mail + ", phoneNumber=" + phoneNumber + ", joiningDate="
				+ joiningDate + ", facebookName=" + facebookName + ", studentID=" + studentID + ", status=" + status
				+ "expiringDate: " + expiringDate +"]";
	}*/

}
