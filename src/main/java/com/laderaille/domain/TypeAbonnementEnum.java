package com.laderaille.domain;

public enum TypeAbonnementEnum {
	
		MENSUEL("Abonnement mensuel"),
		ANNUEL("Abonnement annuel"),
	    TRIMESTRIEL("Abonnement trimestriel"),
	    ETUDIANT("Abonnement etudiant"),
	 	AINE("Abonnement aine");
		
		private TypeAbonnementEnum( String s )
		{
			typeAbonnement = s;
		}
			
		private String typeAbonnement;

		public String getTypeAbonnement()
		{
			return typeAbonnement;
		}
	}
