package com.laderaille.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * This class contains the methods that create and operate on or return the
 * needed datasource components for the program.
 * 
 * <p>
 * This is one of the most important in the program because it allows the user
 * to manipulate data. It gets the input of its methods from the
 * "app.properties" file located in the "resources" folder.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 */

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.laderaille" })
@PropertySource(value = { "classpath:app.properties" })
public class HibernateConfig {

	@Autowired
	private Environment environment;

	/**
	 * "sessionFactory" method creates a Hibernate SessionFactory and is the
	 * usual way to set up a shared Hibernate SessionFactory in a Spring
	 * application context; the SessionFactory can then be passed to
	 * Hibernate-based DAOs via dependency injection. 
	 * 
	 * @return LocalSessionFactoryBean
	 */
	@Bean	
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.laderaille.domain" });
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	/**
	 * A factory for connections to the physical data source that this
	 * DataSource object represents.
	 * 
	 * <p>
	 * A DataSource object has properties that can be modified when necessary.
	 * by updating the "app.properties" file that contains all the details of
	 * the datasource server. The benefit of this method is that properties can
	 * be changed, any code accessing that data source does not need to be
	 * changed.
	 * 
	 * @return DataSource
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
		return dataSource;
	}

	/**
	 * This method allows Hibernate to know in advance where to find the mapping
	 * information that defines how Java classes in this program relate to the
	 * database tables. it also set up some needed Hibernate configuration
	 * settings related to database and other related parameters.
	 * 
	 * @return Properties
	 */
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		return properties;
	}

	/**
	 * Binds a Hibernate Session from the specified factory to the thread,
	 * potentially allowing for one thread-bound Session per factory.
	 *
	 *@param s SessionFactory
	 *
	 *@return HibernateTransactionManager
	 */
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
