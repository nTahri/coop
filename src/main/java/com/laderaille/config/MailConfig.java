package com.laderaille.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@ComponentScan(basePackages = "com.laderaille")
public class MailConfig {
	
		// In this configuration we use google SMTP Server
		// with an account create for sending email
		@Bean
		/**
		 * Configure SMTP mailServer
		 * @return server configuration
		 */
		public JavaMailSender getConfigServer(){
			
			JavaMailSenderImpl server = new JavaMailSenderImpl();
			
			//Here we use the SMTP Server of google.
			server.setHost("smtp.gmail.com");
			server.setPort(587);
			server.setUsername("mycoop.laderaille");
			server.setPassword("LaDeraille1");
			
			//Here we add properties of configuration
			Properties mailConfiguration = new Properties();
			mailConfiguration.put("mail.smtp.starttls.enable", "true");
			mailConfiguration.put("mail.smtp.ssl.trust","*");
			mailConfiguration.put("mail.smtp.auth", "true");
			mailConfiguration.put("mail.transport.protocol", "smtp");
			mailConfiguration.put("mail.debug", "true");
			
			
			server.setJavaMailProperties(mailConfiguration);
			return server;
		}

}
