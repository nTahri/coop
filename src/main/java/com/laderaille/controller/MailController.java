package com.laderaille.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.laderaille.domain.Member;
import com.laderaille.service.MailService;
import com.laderaille.service.MemberService;

@Controller
@RequestMapping("/mailForm")
public class MailController {

	@Autowired
	MailService mailService;

	@Autowired
	MemberService memberService;

	List<File> attachments = new ArrayList<File>();
	List<String> attachmentsNames = new ArrayList<String>();

	/**
	 * This method return an email adress of a member that we specify the id
	 * 
	 * @param id
	 *            the id of the member
	 * 
	 * @return String the email address of the memeber
	 */

	public String getEMailMember(int id) {

		Member member = memberService.findById(id);
		String email = member.getMail();
		return email;
	}

	/**
	 * This method return all email adress in DB
	 * 
	 * @return String[] the list of emails of all the members
	 */
	public String[] listEMailsMembers() {

		List<Member> members = memberService.findAllMembers();
		ArrayList<String> listEmail = new ArrayList<String>();
		for (int i = 0; i < members.size(); i++) {
			listEmail.add(members.get(i).getMail());
			System.out.println(members.get(i).getMail());
			System.out.println(i);
		}
		String[] to = listEmail.toArray(new String[members.size()]);
		return to;
	}

	@RequestMapping()
	public String showMailForm(ModelMap model) throws MessagingException {

		attachments.clear();
		model.addAttribute("LoggedInUser", getPrincipal());
		return "mailForm";
	}

	/**
	 * This method is used to allow the user to attach a file to the mail he is
	 * sending
	 * 
	 * @param model ModelMap
	 * @param message String the message content 
	 * @param subject String the subject of the message 
	 * @param mailTo String the receiver of the mail
	 * @param request HttpServletRequest
	 * 
	 * @throws MessagingException IOException
	 * @throws IOException IOException
	 * 
	 * @return String the URL that needs to be used 
	 */
	@RequestMapping(params = "addAttach", method = RequestMethod.POST)
	public String addAttachments(HttpServletRequest request, @RequestParam("mailTo") String mailTo,
			@RequestParam("subject") String subject, @RequestParam("message") String message, ModelMap model)
			throws IOException, MessagingException {

		model.addAttribute("mailTo", mailTo);
		model.addAttribute("subject", subject);
		model.addAttribute("message", message);

		return "mailForm";
	}

	/**
	 * This method send mail to all member in DB
	 * 
	 * @param model ModelMap
	 * @param message String the message content 
	 * @param subject String the subject of the message 
	 * @param request HttpServletRequest
	 * 
	 * @throws MessagingException MessagingException
	 * 
	 * @return String the URL to be used 
	 */

	@RequestMapping(params = "sendtoall", method = RequestMethod.POST)
	public String sendMail(HttpServletRequest request, @RequestParam("subject") String subject,
			@RequestParam("message") String message, 
			ModelMap model) throws MessagingException {

		String[] toAll = listEMailsMembers();
		String senderMail = "admin@laderaille.ca";
		mailService.sendEmail(toAll, senderMail, subject, message, attachments);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "Result";
	}

	/**
	 * This method send mail to some member
	 * 
	 * @param message String the message content 
	 * @param subject String the subject of the message 
	 * @param request HttpServletRequest
	 * @param mailTo String the receiver of the mail 
	 * 
	 * @throws MessagingException MessagingException
	 * 
	 * @return String the URL to be used 
	 */
	@RequestMapping(params = "sendtosome", method = RequestMethod.POST)
	public String sendMailToOneMember(HttpServletRequest request, @RequestParam("subject") String subject,
			@RequestParam("mailTo") String mailTo,
			@RequestParam("message") String message/*
													 * ,@RequestParam(
													 * "attachFile") String
													 * attachFile
													 */
	) throws MessagingException {

		String[] to = mailTo.split(";");
		String senderMail = "admin@laderaille.ca";
		mailService.sendEmail(to, senderMail, subject, message, attachments);
		return "Result";

	}

	/**
	 * This method is used to get the details of the current user of the program
	 * in order to avoid asking the user for credentials each time he requests
	 * other secured pages.
	 * 
	 * @return String: the member name
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

}
