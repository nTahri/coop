//http://developers.itextpdf.com/
package com.laderaille.controller;



import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.laderaille.domain.Category;
import com.laderaille.domain.Member;
import com.laderaille.domain.Product;
import com.laderaille.repository.CategoryDaoImp;
import com.laderaille.service.CategoryService;
import com.laderaille.service.InvoiceLineService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.MemberService;
import com.laderaille.service.ProductService;
import com.laderaille.service.TaxService;
import com.laderaille.utils.PdfGenerateFile;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.document.AbstractXlsView;



@Controller
@RequestMapping("/export")
public class UtilController {
	@Autowired CategoryService catgoryService;
	@Autowired ProductService productService;
	@Autowired MemberService memberService;
	@Autowired PdfGenerateFile pdfGenerateFile;
	
	
	@RequestMapping("/excel")
	public View listExcell() {
	    return new AbstractXlsView() {
	    	Logger log = LoggerFactory.getLogger(CategoryDaoImp.class);
	        @Override
	        protected void buildExcelDocument(Map<String, Object> model, org.apache.poi.ss.usermodel.Workbook workbook,
	    			HttpServletRequest request, HttpServletResponse response) throws Exception {
	        	response.addHeader("Content-Disposition", "attachment; filename=\"dataSource.xls\"");
	    		List<Category> listCategory = catgoryService.findAllCategories();
	            Sheet sheetCat = workbook.createSheet("Categories");
	            sheetCat.setDefaultColumnWidth(30);
	             
	            // create style for header cells
	            CellStyle style = workbook.createCellStyle();
	            Font font = workbook.createFont();
	            font.setFontName("Arial");
	            style.setFillForegroundColor(HSSFColor.BLUE.index);
	            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	            font.setColor(HSSFColor.WHITE.index);
	            style.setFont(font);
	             
	            // create header row
	            Row headerCat = sheetCat.createRow(0);
	             
	            headerCat.createCell(0).setCellValue("NAME");
	            headerCat.getCell(0).setCellStyle(style);
	             
	            headerCat.createCell(1).setCellValue("Parent");
	            headerCat.getCell(1).setCellStyle(style);
	             
	             
	            // create data rows
	            int rowCount = 1;
	             
	            for (Category cat : listCategory) {
	                Row aRow = sheetCat.createRow(rowCount++);
	                aRow.createCell(0).setCellValue(cat.getName());
	                log.info("================cat.getCategoryParent() Name : {}", cat.getCategoryParent().getName());
	                aRow.createCell(1).setCellValue(cat.getCategoryParent().getName() == cat.getName() ? "" : cat.getCategoryParent().getName());
	            }
	            
	            Sheet sheetProduct = workbook.createSheet("Products");
	            sheetProduct.setDefaultColumnWidth(30);
	            
	            // create header row
	            Row headerProduct = sheetProduct.createRow(0);
	             
	            headerProduct.createCell(0).setCellValue("CODE");
	            headerProduct.getCell(0).setCellStyle(style);
	             
	            headerProduct.createCell(1).setCellValue("REFERENCE");
	            headerProduct.getCell(1).setCellStyle(style);
	            
	            headerProduct.createCell(2).setCellValue("NAME");
	            headerProduct.getCell(2).setCellStyle(style);
	             
	            headerProduct.createCell(3).setCellValue("PRICESELL");
	            headerProduct.getCell(3).setCellStyle(style);
	            
	            headerProduct.createCell(4).setCellValue("PRICEBUY");
	            headerProduct.getCell(4).setCellStyle(style);
	             
	            headerProduct.createCell(5).setCellValue("STOCK");
	            headerProduct.getCell(5).setCellStyle(style);
	            
	            headerProduct.createCell(6).setCellValue("TAXCAT");
	            headerProduct.getCell(6).setCellStyle(style);
	             
	            headerProduct.createCell(7).setCellValue("CATEGORY");
	            headerProduct.getCell(7).setCellStyle(style);
	            
	            headerProduct.createCell(8).setCellValue("DESCRIPTION");
	            headerProduct.getCell(8).setCellStyle(style);
	            
	            headerProduct.createCell(9).setCellValue("MINSTOCK");
	            headerProduct.getCell(9).setCellStyle(style);
	             
	             
	            // create data rows
	            int rowCountPrd = 1;
	             
	            for (Product prod : productService.findAllProducts()) {
	                Row aRow = sheetProduct.createRow(rowCountPrd++);
	                aRow.createCell(0).setCellValue(prod.getProductCode());
	                aRow.createCell(1).setCellValue(prod.getProductRef());
	                aRow.createCell(2).setCellValue(prod.getProductName());
	                aRow.createCell(3).setCellValue(prod.getProductSellPrice());
	                aRow.createCell(4).setCellValue(prod.getProductPrice());
	                aRow.createCell(5).setCellValue(prod.getProductStock());
	                aRow.createCell(6).setCellValue(prod.getProductTaxCat());
	                aRow.createCell(7).setCellValue(prod.getProductCategory().getName());
	                aRow.createCell(8).setCellValue(prod.getProductDescription() == null ? "DESCRIPTION":prod.getProductDescription());
	                aRow.createCell(9).setCellValue(prod.getProductStockMin());
	            }
	        }
	    };
	}
	@RequestMapping("/mailingList")
	public View getmailinglist() {
	    return new AbstractXlsView() {
	        @Override
	        protected void buildExcelDocument(Map<String, Object> model, org.apache.poi.ss.usermodel.Workbook workbook,
	    			HttpServletRequest request, HttpServletResponse response) throws Exception {
	        	response.addHeader("Content-Disposition", "attachment; filename=\"mailingList.xls\"");
	    		List<Member> members = memberService.findAllMembers();
	            Sheet mailingListSheet = workbook.createSheet("MailingList");
	            mailingListSheet.setDefaultColumnWidth(30);
	             
	            // create style for header cells
	            CellStyle style = workbook.createCellStyle();
	            Font font = workbook.createFont();
	            font.setFontName("Arial");
	            style.setFillForegroundColor(HSSFColor.BLUE.index);
	            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	            font.setColor(HSSFColor.WHITE.index);
	            style.setFont(font);
	             
	            // create header row
	            Row columnHeader = mailingListSheet.createRow(0);
	             
	            columnHeader.createCell(0).setCellValue("Prenom");
	            columnHeader.getCell(0).setCellStyle(style);
	            
	            columnHeader.createCell(1).setCellValue("Nom");
	            columnHeader.getCell(1).setCellStyle(style);
	             
	            columnHeader.createCell(2).setCellValue("Email");
	            columnHeader.getCell(2).setCellStyle(style);
	             
	            columnHeader.createCell(3).setCellValue("JoiningDate");
	            columnHeader.getCell(3).setCellStyle(style);
	             
	            // create data rows
	            int rowCount = 1;
	             
	            for (Member member : members) {
	                Row aRow = mailingListSheet.createRow(rowCount++);
	                aRow.createCell(0).setCellValue(member.getFirstName());
	                aRow.createCell(1).setCellValue(member.getLastName());
	                aRow.createCell(2).setCellValue(member.getMail());
	                aRow.createCell(3).setCellValue(member.getJoiningDate());
	            }
	        }
	    };
	}
	
	@RequestMapping("/downloadPDF/{id}")
    public PdfGenerateFile downloadPDF(@PathVariable Integer id, @RequestParam boolean show, @RequestParam boolean emailIt) {
		pdfGenerateFile.setId(id);
		pdfGenerateFile.setShow(show);
		pdfGenerateFile.setEmailIt(emailIt);
        return pdfGenerateFile;
    }
	
}
