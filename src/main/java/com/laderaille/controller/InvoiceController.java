package com.laderaille.controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.google.gson.Gson;
import com.laderaille.domain.Category;
import com.laderaille.domain.Event;
import com.laderaille.domain.FileBucket;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.Product;
import com.laderaille.domain.InvoiceLine;
import com.laderaille.domain.Member;
import com.laderaille.service.CategoryService;
import com.laderaille.service.EventService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.MemberService;
import com.laderaille.service.ProductService;
import com.laderaille.service.TaxService;
import com.laderaille.utils.PdfGenerateFile;
import com.laderaille.service.InvoiceLineService;

/**
 * This class contains the methods that map the INVOICE model with all the
 * view(s) and URL(s) that need an INVOICE or its details.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 * 
 * 
 */

@Controller
@RequestMapping("/invoice")
@SessionAttributes({ "roles" })
// @SessionAttributes({"roles", "Category"})
public class InvoiceController {

	final String uriForPDF = "http://localhost:8080/laderaille/export/downloadPDF";
	@Autowired
	CategoryService categoryService;
	@Autowired
	EventService eventService;
	@Autowired
	ProductService productService;
	@Autowired
	InvoiceService invoiceService;
	@Autowired
	InvoiceLineService invoiceLineService;
	@Autowired
	TaxService taxService;
	@Autowired
	MemberService memberService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	@Autowired PdfGenerateFile pdfGenerateFile;

	/**
	 * This method will provide the medium to add a new Invoice.
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */

	@RequestMapping(value = { "/addInvoice" }, method = RequestMethod.GET)
	public String newInvoice(ModelMap model) {

		Invoice invoice = new Invoice();
		model.addAttribute("invoice", invoice);

		List<Category> categories = categoryService.findAllCategories();
		List<Product> products = productService.findAllProducts();
		List<Member> members = memberService.findAllMembers();
		members.removeIf(m -> m.getStatus() == false);
		List<Invoice> notPaiedinvoices = invoiceService.findAllInvoices();
		notPaiedinvoices.removeIf(inv -> inv.getInvoiceStatus() == 1 || inv.getInvoiceStatus() == 2 || inv.getInvoiceStatus() == 3 || inv.getInvoiceStatus() == 4);
		
		List<Event> events = eventService.findAllEvents();
		
		Date d = new Date();
		events.removeIf(evn -> d.after(evn.getEndDate()));
		
		System.out.println("============= " + memberService.findAllMembers().toString());
		model.addAttribute("categories", categories);
		model.addAttribute("products", products);
		model.addAttribute("members", members);
		model.addAttribute("events", events);
		model.addAttribute("notPaiedinvoices", new Gson().toJson(notPaiedinvoices));
		model.addAttribute("tps", taxService.getTps(1));
		model.addAttribute("tvq", taxService.getTvq(1));
		model.addAttribute("LoggedInUser", getPrincipal());
		return "addInvoice";
	}

	/**
	 * This method will provide the medium to get all the invoices persisted in
	 * the database.
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */

	@RequestMapping(value = { "/invoiceList" }, method = RequestMethod.GET)
	public String getInvoiceList(ModelMap model) {
		System.out.println("addInvoice");

		List<Category> categories = categoryService.findAllCategories();
		List<Invoice> invoices = invoiceService.findAllInvoices();

		model.addAttribute("invoices", invoices);

		model.addAttribute("categories", categories);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "invoiceList";
	}

	/**
	 * This method will provide the medium to present a list of all the
	 * categories that exist in the database.
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/categoryList" }, method = RequestMethod.GET)
	public String getCategoryList(ModelMap model) {
		Category category = new Category();
		model.addAttribute("category", category);

		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "categoryList";
	}

	/**
	 * This method will provide the medium to persist a newly added category
	 * into the database.
	 * 
	 * @param category
	 *            Category: the object that needs to be persisted into the
	 *            database
	 * @param model
	 *            ModelMap
	 * @param result
	 *            BindingResult
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/categoryList" }, method = RequestMethod.POST)
	public String addCategory(@Valid Category category, BindingResult result, ModelMap model) {

		model.addAttribute("LoggedInUser", getPrincipal());

		if (result.hasErrors()) {
			List<Category> categories = categoryService.findAllCategories();
			model.addAttribute("categories", categories);
			return "categoryList";
		}

		categoryService.saveCategory(category);

		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return "categoryList";
	}

/*	@RequestMapping(value = { "/deleteCategory-{id}" }, method = RequestMethod.GET)
	public String deleteCategory(@PathVariable String id) {
		categoryService.deleteCategoryById(Integer.parseInt(id));
		// List<Category> categories = categoryService.findAllCategories();

		// model.addAttribute("categories", categories);
		// model.addAttribute("LoggedInUser", getPrincipal());
		return "redirect:/invoice/categoryList";
	}
*/
	/**
	 * This method will provide the medium to persist a newly added INVOICE into
	 * the database.It will throw an exception if something goes wrong during
	 * the persistence process
	 * 
	 * @param invoice
	 *            Invoice: the object that needs to be persisted into the
	 *            database
	 * @param model
	 *            ModelMap
	 * @param result
	 *            BindingResult
	 * @param result
	 *            String
	 * @param body
	 *            String
	 * @param response
	 *            HttpServletResponse
	 * @param print
	 *            boolean: if the user wants to print the invoice or not
	 * @param emailIt
	 *            boolean: if the user wants to send the invoice by mail or not
	 * 
	 * @throws IOException
	 *             input output exception
	 * @throws JsonParseException
	 *             JsonParseException
	 * @throws URISyntaxException
	 *             URISyntaxException
	 */
	@ResponseStatus(value = HttpStatus.OK)
	@RequestMapping(value = { "/saveInvoice" }, method = RequestMethod.POST)
	public void saveInvoice(@Valid Invoice invoice, BindingResult result, ModelMap model, @RequestBody String body, 
			HttpServletResponse response, @RequestParam boolean print, @RequestParam boolean emailIt) throws IOException, JsonParseException, URISyntaxException {

		//boolean print = false;
		String sendEmail = "";
		String msg = "";

		invoice.setTransactionDate(new Date());
		if(invoice.getInvoiceStatus() != 0){
			invoice.setPaiementDate(new Date());
		 }
		System.out.println("================ " + invoice.getInvoiceStatus());
		invoiceService.saveInvoice(invoice); // Initial save

		body = new URI(body).getPath();
		String[] bodyParts = body.split("&");
		// System.out.println("================ " + body);
		Map<String, String> tmpMap = new HashMap<String, String>();

		// List<Item> items = new ArrayList<Item>();
		Set<InvoiceLine> invoiceLines = new HashSet<InvoiceLine>();

		for (int i = 0; i < bodyParts.length; i++) {

			String[] bp_split = bodyParts[i].split("=");
			if (bp_split.length > 1)
				tmpMap.put(bp_split[0], bp_split[1]);

			// System.out.println(bp_split[0]);
			if (bp_split[0].equals("items_str")) {
				String json_str = bp_split[1];

				ObjectMapper mapper = new ObjectMapper();
				JsonFactory factory = mapper.getJsonFactory();
				JsonParser parser = factory.createJsonParser(json_str);
				JsonNode parsedObj = mapper.readTree(parser);

				System.out.println(parsedObj);

				if (parsedObj.isArray()) {
					for (final JsonNode objNode : parsedObj) {
						// Item it = mapper.readValue(objNode, Item.class);
						// items.add(it);

						InvoiceLine il = mapper.readValue(objNode, InvoiceLine.class);
						il.setInvoiceId(invoice.getId());
						
						
						// Verification of discounts
						if (il.getDiscountType() == "%") {
							il.setDiscount(Math.max(0, il.getDiscount()));
							il.setDiscount(Math.min(100, il.getDiscount()));
						} else if (il.getDiscountType() == "$") {
							double max_flat_discount_value = il.getPrice() * il.getQuantity();
							il.setDiscount(Math.max(0, il.getDiscount()));
							il.setDiscount(Math.min(max_flat_discount_value, il.getDiscount()));
						}
						invoiceLines.add(il);

						Product pr = productService.findByCode(il.getProductId());
						if (pr.getProductRef().startsWith("M")) {
							Member mem = memberService.findByMail(invoice.getBuyerName());
							Calendar c = Calendar.getInstance();
							//We have an expiring date and this date is more that the actual date than we add into it
							if(mem.getExpiringDate() != null && mem.getExpiringDate().compareTo(new Date()) > 0){
								c.setTime(mem.getExpiringDate());
							}else{
								c.setTime(new Date());
							}
							
							if (pr.getProductRef().startsWith("MA")) {
								c.add(Calendar.YEAR, 1);// set one year from now
							}else if (pr.getProductRef().startsWith("MM")) {
								c.add(Calendar.MONTH, 1);// set one month from now
							}
							mem.setExpiringDate(c.getTime()); 
							mem.setStatus(true);
							memberService.updateMember(mem);
							invoiceLineService.saveInvoiceLine(il);
						} else {
							double actualStock = pr.getProductStock() - il.getQuantity();
							if (actualStock >= 0) {
								pr.setProductStock(actualStock);
								productService.updateProduct(pr);
								invoiceLineService.saveInvoiceLine(il);
							}
						}

					}
				}
			}
			if (bp_split[0].equals("sendEmail")) {
				sendEmail = Boolean.parseBoolean(bp_split[1]) ? "&sendEmail=" + invoice.getBuyerName() : "";
			}
			if (bp_split[0].equals("print")) {
				print = Boolean.parseBoolean(bp_split[1]);
			}

		}

		invoiceService.updateInvoice(invoice);
		System.out.println("================ " + print + " ================ " + sendEmail);
		msg = uriForPDF + "/" + invoice.getId() + "?show=" + print + "&emailIt=" + emailIt;
		//if(print){
			response.setContentType("text/html");
			response.getWriter().write(msg);
		//}
	}
	
	/**
	 * This method allows the user to see the invoice details
	 * 
	 * @param id
	 *            Integer that represent the invoice id
	 * @param model
	 *            ModelMap
	 * 
	 * @return String the URL
	 */
	@RequestMapping(value = { "/view-{id}" }, method = RequestMethod.GET)
	public String viewInvoice(@PathVariable Integer id, ModelMap model) {
		model.addAttribute("LoggedInUser", getPrincipal());
		
		Invoice invoice = invoiceService.findInvoiceById(id);
		if (invoice != null) {
			Event event = eventService.findById(invoice.getEventId());
			List<InvoiceLine> ils = invoiceLineService.findAllInvoiceLineByInvoiceID(invoice.getId());
			List<HashMap<String, String>> invoiceLines = getInvoiceLinesWithPrductsName(invoiceLineService.findAllInvoiceLineByInvoiceID(invoice.getId()));
			

			model.addAttribute("paiment", invoice.getInvoiceStatus() == 0 ? true : false);
			model.addAttribute("invoice", invoice);
			model.addAttribute("tps_v", taxService.getTps(1) * invoice.getSubtotal());
			model.addAttribute("tvq_v", taxService.getTvq(1) * invoice.getSubtotal());
			model.addAttribute("buyer", invoice.getBuyerName());
			model.addAttribute("event", event);
			model.addAttribute("invoiceLines", invoiceLineService.findAllInvoiceLineByInvoiceID(invoice.getId()));
			model.addAttribute("invoiceLinesFromMap", invoiceLines);

			// Member buyer = memberService.findByMail(invoice.getBuyerName());
			
//			model.addAttribute("invoice", invoice);
//			model.addAttribute("event", event);
			// model.addAttribute("buyer", buyer);

			model.addAttribute("invoiceLines", ils);

			return "viewInvoice";
		}
		
		return "redirect:/invoiceList";
	}
	@RequestMapping(value = { "/payInvoice-{id}" }, method = RequestMethod.GET)
	public void paimentInvoice(@PathVariable Integer id, ModelMap model, HttpServletResponse response) throws IOException {
		Invoice invoice = invoiceService.findInvoiceById(id);
		invoice.setInvoiceStatus(1);
		invoice.setPaiementDate(new Date());
		invoiceService.updateInvoice(invoice);
		//boolean print = false;
		//String sendEmail = invoice.getBuyerName();
		String msg = uriForPDF + "/" + invoice.getId() + "?show=" + false + "&emailIt=" + true;
		response.setContentType("text/html");
		response.getWriter().write(msg);
		 
	}
	@RequestMapping(value = { "/refundInvoice-{id}" }, method = RequestMethod.GET)
	public void refoundInvoice(@PathVariable Integer id, ModelMap model, HttpServletResponse response) throws IOException {
		Invoice invoice = invoiceService.findInvoiceById(id);
		Invoice refoundInvoice = new Invoice();

		refoundInvoice.setBuyerName(invoice.getBuyerName());
		refoundInvoice.setSellerName(invoice.getSellerName());
		refoundInvoice.setTransactionDate(new Date());
		refoundInvoice.setPaiementDate(new Date());
		refoundInvoice.setEventId(invoice.getEventId());
		invoiceService.saveInvoice(refoundInvoice);
		
		List<InvoiceLine> ils = invoiceLineService.findAllInvoiceLineByInvoiceID(id);
		
		for(InvoiceLine il:ils){
			InvoiceLine ilRefound = new InvoiceLine();
			ilRefound.setInvoiceId(refoundInvoice.getId());
			
			ilRefound.setPrice(-1 * il.getPrice());
			ilRefound.setProductId(il.getProductId());
			ilRefound.setQuantity(-1 * il.getQuantity());
			ilRefound.setTSQ(-1 * il.getTSQ());
			ilRefound.setTVQ(-1 * il.getTVQ());
			ilRefound.setDiscountType(il.getDiscountType());
			ilRefound.setDiscount(il.getDiscount());
			Product p = productService.findByCode(il.getProductId());
			p.setProductStock(p.getProductStock() + il.getQuantity());
			productService.updateProduct(p);
			invoiceLineService.saveInvoiceLine(ilRefound);
		}
		refoundInvoice.setInvoiceStatus(2);
		refoundInvoice.setSubtotal((-1 * invoice.getSubtotal()));
		refoundInvoice.setTotal((-1 * invoice.getTotal()));
		
		invoiceService.updateInvoice(refoundInvoice);
		
		String msg = uriForPDF + "/" + refoundInvoice.getId() + "?show=" + false + "&emailIt=" + true;
		response.setContentType("text/html");
		response.getWriter().write(msg);
		
	}
	
	/**
	 * This method will provide the medium to view the inventory
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/inventory" }, method = RequestMethod.GET)
	public String inventory(ModelMap model) {
		List<Category> categories = categoryService.findAllCategories();
		List<Product> products = productService.findAllProducts();
		List<Member> members = memberService.findAllMembers();
		model.addAttribute("categories", categories);
		model.addAttribute("products", products);
		model.addAttribute("members", members);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "inventory";
	}

	/**
	 * This method will provide the medium to view the inventory
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/addProduct" }, method = RequestMethod.GET)
	public String addProduct(ModelMap model) {
		Product product = new Product();
		model.addAttribute("product", product);

		FileBucket fileModel = new FileBucket();
		model.addAttribute("fileBucket", fileModel);
		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("edit", false);
		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return "addProduct";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * saving member in database. It also validates the member input
	 * 
	 * 
	 * @param product
	 *            Product: the object that needs to be persisted into the
	 *            database
	 * @param model
	 *            ModelMap
	 * @param result
	 *            BindingResult
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/addProduct" }, method = RequestMethod.POST)
	public String saveProduct(@Valid Product product, BindingResult result, ModelMap model) {
		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("backPage", "/laderaille/invoice/addProduct");
		Category category = categoryService.findById(product.getProductCategory().getId());
		System.out.println(
				"============== " + category.getName() + " ============== " + product.getProductCategory().getId());
		if (result.hasErrors()) {
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("edit", false);
			List<Category> categories = categoryService.findAllCategories();
			model.addAttribute("categories", categories);

			model.addAttribute("message", result.getAllErrors());

			return "Error";
		}
		try {
			Product newProduct = new Product();
			newProduct = product;
			newProduct.setProductCategory(category);
			productService.saveProduct(newProduct);
			// model.addAttribute("success", "Membre " + member.getFirstName() +
			// " "+ member.getLastName() + " ajout� avec succ�s");
			return "redirect:/invoice/inventory";
		} catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "Error";
		}
	}

	/**
	 * This method will provide the medium to view the product details.
	 * 
	 * @param id
	 *            Integer: the id of the product that needs to be updated in the
	 *            database
	 * @param model
	 *            ModelMap
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/modifyProduct-{id}" }, method = RequestMethod.GET)
	public String modifyItem(@PathVariable Integer id, ModelMap model) {
		Product product = productService.findById(id);
		model.addAttribute("product", product);

		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("edit", true);
		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);
		return "addProduct";
	}

	/**
	 * This method will provide the medium to update the product details and
	 * persist them into the database.
	 * 
	 * @param product
	 *            Product: the PRODUCT object that needs to be updated
	 * @param result
	 *            BindingResult
	 * @param id:
	 *            the id of the product that needs to be updated in the database
	 * @param model
	 *            ModelMap
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/modifyProduct-{id}" }, method = RequestMethod.POST)
	public String modifyItem(@Valid Product product, BindingResult result, @PathVariable Integer id, ModelMap model) {

		List<Category> categories = categoryService.findAllCategories();
		model.addAttribute("categories", categories);

		if (result.hasErrors()) {
			model.addAttribute("edit", true);
			model.addAttribute("LoggedInUser", getPrincipal());
			return "addProduct";
		}

		productService.updateProduct(product);

		model.addAttribute("LoggedInUser", getPrincipal());

		return "redirect:/invoice/inventory";
	}

	/**
	 * This method will provide the medium to provide the details of the product
	 * that needs to be deleted from the database.
	 *
	 * @param id:
	 *            the id of the product that needs to be updated in the database
	 * 
	 * @return String that represents the path that need to be added to the URL
	 */
	@RequestMapping(value = { "/deleteProduct-{id}" }, method = RequestMethod.GET)
	public String deleteItem(@PathVariable Integer id) {
		// memberService.deleteMemberByID(id);
		return "redirect:/invoice/inventory";
	}

	/**
	 * This method is used to get the details of the current user of the program
	 * in order to avoid asking the user for credentials each time he requests
	 * other secured pages.
	 * 
	 * @return String: the member name
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

	/**
	 * This method returns true if users is already authenticated [logged-in],
	 * else false.
	 * 
	 * @return boolean that indicates type of the used authentication
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authenticationTrustResolver.isAnonymous(authentication);
	}
	@SuppressWarnings("serial")
	private List<HashMap<String, String>> getInvoiceLinesWithPrductsName(List<InvoiceLine> invoiceLines){
		List<HashMap<String, String>> invLines = new ArrayList<HashMap<String, String>>();
		for(InvoiceLine invoiceLine:invoiceLines){
			HashMap<String, String> line = new HashMap<String, String>(){{
				put("code", invoiceLine.getProductId());
				put("category", productService.findByCode(invoiceLine.getProductId()).getProductCategory().getName());
				put("productName", productService.findByCode(invoiceLine.getProductId()).getProductName());
				put("quantity", invoiceLine.getQuantity()+"");
				put("price", invoiceLine.getPrice()+"");
				put("discount", invoiceLine.getDiscount()+invoiceLine.getDiscountType()+"");
			}};
			invLines.add(line);
			if(invoiceLine.getDiscount() != 0){
				HashMap<String, String> lineDiscount = new HashMap<String, String>(){{
					put("code", invoiceLine.getProductId());
					put("category", productService.findByCode(invoiceLine.getProductId()).getProductCategory().getName());
					put("productName", productService.findByCode(invoiceLine.getProductId()).getProductName() + " Rabais");
					put("quantity", invoiceLine.getQuantity()+"");
					if(invoiceLine.getDiscountType().equals("%")){
						put("price", (-1 * (invoiceLine.getPrice() * invoiceLine.getDiscount() / 100))+"");
					}else{
						put("price", (-1 * (invoiceLine.getPrice() - invoiceLine.getDiscount()))+"");
					}
					put("discount", "");
				}};
				invLines.add(lineDiscount);
			}
		}
		return invLines;
		
	}
}
