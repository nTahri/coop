package com.laderaille.controller;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;
import com.laderaille.service.EventService;
import com.laderaille.service.MemberService;
import com.laderaille.service.WorkingTimesService;

@Controller
@RequestMapping("/workHours")
public class WorkHoursController {	

		@Autowired
		WorkingTimesService workingTimesService;

		@Autowired
		MemberService memberService;
		
		@Autowired
		EventService eventService;		

		/*
		 * This method help to get all workingTimes for only one member
		 * @param : mail.
		 */
		@RequestMapping()
		public String setupForm(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
				Map<String, Object> map, ModelMap model) {			
		
			//WorkingTimes workHours = new WorkingTimes();
			map.put("workHours", workingTimes);
			map.put("members", memberService.findAllMembers());
			return "workHours";
		}			
	
		/*
		 * This method help to get all workingTimes for only one member for a specific period
		 * @param : mail, startDate, startDate.
		 */
		@RequestMapping(params = "searchByPeriod", method = RequestMethod.POST)
		public String searchMemberWorkingTimesByPeriod(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
				@RequestParam("startingDate") String startingDate, @RequestParam("endingDate") String endingDate, Map<String, Object> map, ModelMap model) {			

			String mail = workingTimes.getMember().getMail();
			List<WorkingTimes> memberWorkingTimesList = null;
			if(startingDate != "" && endingDate != "" )
			{			
				Date startDate =  dateFormatConverterInDate(startingDate);
				//new Date(this.dateFormatConverterFunction(startingDate));	
				@SuppressWarnings("deprecation")
				Date endDate =  dateFormatConverterInDate(endingDate);
				//new Date(this.dateFormatConverterFunction(endingDate));				
				if(startDate != null && endDate != null && mail != null)
				{	
					 memberWorkingTimesList = workingTimesService
							.findAllWorkingTimeBetweenDate(memberService.findByMail(mail), startDate, endDate);
				}
			}
			map.put("workHours", workingTimes);
			map.put("SearchworkingTimesList", memberWorkingTimesList);
			map.put("members", memberService.findAllMembers());
			map.put("firstDate", startingDate);
			map.put("secondDate", endingDate);
			map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
			model.addAttribute("LoggedInUser", getPrincipal());			
			return "workHours";
		}
		
		
		/*
		 * This method update an existing workingTimes
		 * @param : id, memberID, startDateTime, endDateTime.
		 */
		@RequestMapping(params = "update", method = RequestMethod.POST)
		public String updateWorkingTimes(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
				Map<String, Object> map, ModelMap model) {

			workingTimesService.editWorkingTimes(workingTimes);
			map.put("workingTimes", workingTimes);
			map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
			model.addAttribute("LoggedInUser", getPrincipal());
			return "workingTimes";
		}

		/*
		 * This method delete an existing workingTimes
		 * @param : id.
		 */
		@RequestMapping(params = "delete", method = RequestMethod.POST)
		public String deleteWorkingTimes(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
				Map<String, Object> map, ModelMap model) {

			workingTimesService.deleteWorkingTimesByID(workingTimes.getId());
			workingTimes = new WorkingTimes();
			map.put("workingTimes", workingTimes);
			map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
			model.addAttribute("LoggedInUser", getPrincipal());
			return "workingTimes";
		}

	

		/*
		 * This method help to get one workingTimes by id
		 * @param : id.
		 */
		@RequestMapping(params = "searchworkingtimes", method = RequestMethod.POST)
		public String searchWorkingTimesByID(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
				@RequestParam("mail") String mail, Map<String, Object> map, ModelMap model) {

			WorkingTimes search = workingTimesService.findWorkingTimesByID(workingTimes.getId());
			workingTimes = search != null ? search : new WorkingTimes();
			map.put("workingTimes", workingTimes);
			map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
			model.addAttribute("LoggedInUser", getPrincipal());
			return "workingTimes";
		}	 
		
		/**
		 * This method returns the principal[user-name] of logged-in user.
		 */
		private String getPrincipal() {
			String memberName = null;
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if (principal instanceof UserDetails) {
				memberName = ((UserDetails) principal).getUsername();
			} else {
				memberName = principal.toString();
			}
			return memberName;
		}	
		
		/*	
		 * @function convert calendar date
		 * @param String oldDateString 
		 * @return newDateString 
		 */
		public String dateFormatConverterFunction(String oldDateString){
			String OLD_FORMAT = "yyyy-MM-dd HH:mm";
			String NEW_FORMAT = "EEE MMM dd yyyy HH:mm";	
			String newDateString = null;		
			Date d;
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
				d = sdf.parse(oldDateString.replace("T"," "));
				sdf.applyPattern(NEW_FORMAT);
				newDateString = sdf.format(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}		
			return newDateString;
		}
		
		public static Date getDateWithoutTime(Date date) {
		    Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		    cal.set(Calendar.HOUR_OF_DAY, 0);
		    cal.set(Calendar.MINUTE, 0);
		    cal.set(Calendar.SECOND, 0);
		    cal.set(Calendar.MILLISECOND, 0);
		    return cal.getTime();
		}

		public static Date getTomorrowDate(Date date) {
		    Calendar cal = Calendar.getInstance();
		    cal.setTime(date);
		    cal.add(Calendar.DATE, 1);
		    return cal.getTime();
		}
		
		public Date dateFormatConverterInDate(String oldDateString){
			Date date = null;
			String FORMAT = "yyyy-MM-dd";
			SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
			try {
				date = formater.parse(oldDateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return date;
		}
		/*
		 * This method help to automatically save times of session owner The method
		 * is now used in member controller to save time when disconnection method
		 * is called. Don't delete event if it is in comment tag.
		 */
		@RequestMapping(value = "/workingTimesAuto")
		public String saveVolunteerWorkingTimes(@ModelAttribute("workingTimesAuto") WorkingTimes workingTimes,
				HttpSession session) {

			if (session != null) {

				Member member = memberService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
				workingTimes.setMember(member);
				workingTimes.setStartDateTime(new Date(session.getCreationTime()));
				workingTimes.setEndDateTime(new Date(session.getLastAccessedTime()));

				workingTimesService.saveWorkingTimes(workingTimes);
			}
			return "workingTimesAuto";
		}

	}
