package com.laderaille.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.laderaille.domain.Event;
import com.laderaille.domain.Member;
import com.laderaille.domain.WorkingTimes;
import com.laderaille.service.EventService;
import com.laderaille.service.MemberService;
import com.laderaille.service.WorkingTimesService;

@Controller
@RequestMapping("/workingTimes")
public class WorkingTimesController {

	@Autowired
	WorkingTimesService workingTimesService;

	@Autowired
	MemberService memberService;
	
	@Autowired
	EventService eventService;
	

	/*
	 * This method put entity workingTimes and list of all workingTimes in a map
	 * before return workingTimes form. It is the default method when this class
	 * is called.
	 */
	@RequestMapping()
	public String setupForm(Map<String, Object> map, ModelMap model) {		
		WorkingTimes workingTimes = new WorkingTimes();
		map.put("members", memberService.findAllMembers());
		map.put("workingTimes", workingTimes);
		map.put("eventList", dailyEvents());
		map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
		model.addAttribute("LoggedInUser", getPrincipal());
		return "workingTimes";
	}
	
	@RequestMapping(params = "showMembers", method = RequestMethod.POST)
	public String showMembers(Map<String, Object> map,@RequestParam("eventId") String eventIdString, ModelMap model) {
		
		int eventId = Integer.parseInt(eventIdString.replaceAll("[^0-9]+", ""));
		WorkingTimes workingTimes = new WorkingTimes();
		
		Set<Member> participants = null;		
		if(eventService.findById(eventId)!=null){			
			participants = eventService.findById(eventId).getMembers();
		}		
		map.put("members", memberService.findAllMembers());
		map.put("workingTimes", workingTimes);
		map.put("memberList", participants);
		map.put("eventIdStringSelected", eventIdString);
		map.put("eventList", dailyEvents());
		map.put("workingTimesList", workingTimesService.getAllWorkingTimes());
		model.addAttribute("LoggedInUser", getPrincipal());
		return "workingTimes";
	}


	/*
	 * This method add a new workingTimes for volunteer.
	 * @param : mail, startDateTime, endDateTime.
	 */	
	@RequestMapping(params = "add", method = RequestMethod.POST)
	public String addWorkingTimes(@ModelAttribute WorkingTimes workingTimes, BindingResult result,
			@RequestParam("startDateTime") String startDateTime, @RequestParam("endDateTime") String endDateTime, 
			@RequestParam("eventId") String eventIdString, Map<String, Object> map, ModelMap model) {	
		int eventId = Integer.parseInt(eventIdString.replaceAll("[^0-9]+", ""));
		boolean savePresence = false ; 
		String sucessMessage = "";
		String errorMessage = "";
		Set<Member> participants = null;
		if(startDateTime != ""&&endDateTime!="")
		{
			String memberMail = workingTimes.getMember().getMail();
			Date firstdate =  dateFormatConverterFunction(startDateTime);
			Date seconddate =  dateFormatConverterFunction(endDateTime);
			
			Member member = memberService.findByMail(memberMail);
			Event event = eventService.findById(eventId);
			if(member!=null && firstdate!=null && seconddate!= null && event!= null)
			{
				workingTimes.setMember(member);
				workingTimes.setStartDateTime(firstdate);
				workingTimes.setEndDateTime(seconddate);
				workingTimesService.saveWorkingTimes(workingTimes);
				savePresence = workingTimesService.updateEventMembers(member, eventId);
				participants = eventService.findById(eventId).getMembers();
			}			
			if(savePresence){
				//TODO redirection if success.
				sucessMessage = member.getFirstName()+" "+member.getLastName()+ " a \u00e9t\u00e9 ajout\u00e9.";
			} else {
				//TODO redirection if not saved.	
				errorMessage = member.getFirstName()+" "+member.getLastName()+ " existe d\u00e9j\u00e0.";
			}	
		}		
		List<Member> memberList = new ArrayList<Member>();
		memberList.addAll(participants);
		map.put("workingTimes", workingTimes);
		map.put("eventList", dailyEvents());
		map.put("memberList", participants);
		map.put("sucessMessage", sucessMessage);
		map.put("errorMessage", errorMessage);
		map.put("eventIdStringSelected", eventIdString);
		map.put("members", memberService.findAllMembers());
		return "workingTimes";
	}

	/*
	 * This method add a new workingTimes for a simple member.
	 * @param : eventId. 
	 */	
	@RequestMapping(params = "checkPresence", method = RequestMethod.POST)
	public String checkWorkingTimes(@ModelAttribute WorkingTimes workingTimes, BindingResult result,			
			@RequestParam("eventId") String eventIdString, Map<String, Object> map, ModelMap model) {
		int eventId = Integer.parseInt(eventIdString.replaceAll("[^0-9]+", ""));
		boolean savePresence = false ; 
		String sucessMessage = "";
		String errorMessage = "";
		Set<Member> participants = null;
		//to show member mail.
		String memberMail = workingTimes.getMember().getMail();	
		
		Member member = memberService.findByMail(memberMail);		
		if(member!=null&&eventService.findById(eventId)!=null)
		{			
			savePresence = workingTimesService.updateEventMembers(member, eventId);
			participants = eventService.findById(eventId).getMembers();
		}
		if(savePresence){
			//TODO redirection if success.
			sucessMessage = member.getFirstName()+" "+member.getLastName()+ " a \u00e9t\u00e9 ajout\u00e9.";
		} else {
			//TODO redirection if not saved.	
			errorMessage = member.getFirstName()+" "+member.getLastName()+ " existe d\u00e9j\u00e0.";
		}	
		List<Member> memberList = new ArrayList<Member>();
		memberList.addAll(participants);
		map.put("workingTimes", workingTimes);
		map.put("memberList", participants);
		map.put("sucessMessage", sucessMessage);
		map.put("errorMessage", errorMessage);
		map.put("eventIdStringSelected", eventIdString);
		map.put("eventList", dailyEvents());
		map.put("members", memberService.findAllMembers());		
		
		return "workingTimes";	
	}
	
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}	
	
	/*	
	 * @function convert calendar date
	 * @param String oldDateString 
	 * @return newDateString 
	 */
	public Date dateFormatConverterFunction(String oldDateString){
		String OLD_FORMAT = "yyyy-MM-dd HH:mm";
		//String NEW_FORMAT = "EEE MMM dd yyyy HH:mm";	
		//String newDateString = null;		
		Date d = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			d = sdf.parse(oldDateString.replace("T"," "));
			//sdf.applyPattern(NEW_FORMAT);
			//newDateString = sdf.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		return d;
	}
	
	public Date dateFormatConverterInDate(String oldDateString){
		Date date = null;
		String FORMAT = "yyyy-MM-dd";
		SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
		try {
			date = formater.parse(oldDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	
	public static Date getDateWithoutTime(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
	    return cal.getTime();
	}

	public static Date getTomorrowDate(Date date) {
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    cal.add(Calendar.DATE, 1);
	    return cal.getTime();
	}	
	
	public List<Event> dailyEvents() {
		Date startDate = getDateWithoutTime(new Date());
		Date endDate = getDateWithoutTime(getTomorrowDate(new Date()));		
		return eventService.findAllDaylyEvents(startDate, endDate);		
	}
}