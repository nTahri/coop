package com.laderaille.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import javax.validation.Valid;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.laderaille.domain.Category;
import com.laderaille.domain.FileBucket;
import com.laderaille.domain.Product;
import com.laderaille.repository.CategoryDao;
import com.laderaille.repository.ProductDao;
import com.laderaille.utils.FileValidator;
import com.laderaille.utils.MultiFileValidator;

@Controller
@RequestMapping("/in")
// @SessionAttributes("roles")
public class FileUploadController {

	// private static String
	// UPLOAD_LOCATION="/Users/nabiltahri/Desktop/config/";
	private static String SHEETNAME = "Products";// "Categories";

	@Autowired FileValidator fileValidator;
	@Autowired MultiFileValidator multiFileValidator;
	@Autowired CategoryDao categoryDao;
	@Autowired ProductDao productDao;

	@InitBinder("fileBucket")
	protected void initBinderFileBucket(WebDataBinder binder) {
		binder.setValidator(fileValidator);
	}

	@RequestMapping(value = "/importexport", method = RequestMethod.GET)
	public String getSingleUploadPage(ModelMap model) {
		FileBucket fileModel = new FileBucket();
		model.addAttribute("fileBucket", fileModel);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "singleFileUploader";
	}

	@RequestMapping(value = "/importexport", method = RequestMethod.POST)
	public String singleFileUpload(@Valid FileBucket fileBucket, BindingResult result, ModelMap model)
			throws IOException {

		if (result.hasErrors()) {
			System.out.println("validation errors");
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("message", result.getAllErrors());
			model.addAttribute("backPage", "/laderaille/in/importexport");
			return "Error";
		} else {
			System.out.println("Fetching file");
			try {
				HSSFWorkbook workbook = new HSSFWorkbook(fileBucket.getFile().getInputStream());
				HSSFSheet categorieSheet = workbook.getSheet(SHEETNAME);
				Iterator<Row> rowIterator = categorieSheet.rowIterator();
				Row row = rowIterator.next();
				while (rowIterator.hasNext()) {
					row = rowIterator.next();
					//1-01 1-01 A. 12 mcx (Midi Express) 6.5 1.0 10.0 STANDARD 1-MIDI EXPRESS
					Product newProd = new Product();
					newProd.setProductCode(row.getCell(0).getStringCellValue());
					newProd.setProductRef(row.getCell(1).getStringCellValue());
					newProd.setProductName(row.getCell(2).getStringCellValue());
					newProd.setProductSellPrice(row.getCell(3).getNumericCellValue());
					newProd.setProductPrice(row.getCell(4).getNumericCellValue());
					newProd.setProductStock(row.getCell(5).getNumericCellValue());
					newProd.setProductTaxCat(row.getCell(6).getStringCellValue());

					newProd.setProductCategory(this.addCategory(row.getCell(7).getStringCellValue()));
					newProd.setProductDescription(row.getCell(8).getStringCellValue());
					newProd.setProductStockMin(row.getCell(9).getNumericCellValue());
					this.addProduct(newProd);

				}
				fileBucket.getFile().getInputStream().close();
				model.addAttribute("fileName", fileBucket.getFile().getOriginalFilename());
				//FileOutputStream out = new FileOutputStream(new File("TODO"));//TODO 
				//workbook.write(out);
				//out.close();
				model.addAttribute("LoggedInUser", getPrincipal());
				model.addAttribute("message", "Successfull upload");
				model.addAttribute("backPage", "/laderaille/in/importexport");
				return "Succes";
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				model.addAttribute("LoggedInUser", getPrincipal());
				model.addAttribute("message", e.getMessage());
				model.addAttribute("backPage", "/laderaille/in/importexport");
				return "Error";
			} catch (IOException e) {
				e.printStackTrace();
				model.addAttribute("LoggedInUser", getPrincipal());
				model.addAttribute("message", e.getMessage());
				model.addAttribute("backPage", "/laderaille/in/importexport");
				return "Error";
			}

		}
	}

	
	/**
	 * Permet de trouver une categorie avec son nom si elle existe sinon on
	 * ajoute une nouvelle
	 * 
	 * @param categoryName
	 * @return retourne le id de la categorie
	 */
	@SuppressWarnings("unused")
	private Category addCategory(String categoryName) {
		Category cat = categoryDao.findByName(categoryName);
		if (cat == null) {
			Category newCat = new Category();
			newCat.setName(categoryName);
			categoryDao.saveCategory(newCat);
			return categoryDao.findByName(categoryName);
		} else {
			return cat;
		}
	}
	/**
	 * Permet d'enregistrer un produit dans la base de donnees
	 * @param product
	 * @return le id du produit
	 */
	@SuppressWarnings("unused")
	private long addProduct(Product product) {
		Product prod = productDao.findProductByName(product.getProductName());
		if (prod == null) {
			productDao.saveProduct(product);
			return productDao.findProductByName(product.getProductName()).getId();
		} else {
			product.setId(prod.getId());
			productDao.updateProduct(product);
			return prod.getId();
		}

	}
	
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

}
