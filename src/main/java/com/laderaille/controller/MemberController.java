package com.laderaille.controller;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.laderaille.domain.Member;
import com.laderaille.domain.MemberProfile;
import com.laderaille.domain.WorkingTimes;
import com.laderaille.service.MemberProfileService;
import com.laderaille.service.MemberService;
import com.laderaille.service.WorkingTimesService;

/**
 * This class contains the methods that create and operate on or return the
 * needed MEMBER components for the program.
 * 
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Soumri.N
 * @author Tahri.N
 * @author Yezli.S
 * 
 * @version 1.0
 */

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class MemberController {

	@Autowired
	MemberService memberService;

	@Autowired
	MemberProfileService memberProfileService;

	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;

	@Autowired
	WorkingTimesService workingTimesService;

	/**
	 * This method initialize the binder that will be used in different other
	 * methods
	 * 
	 * @param dataBinder
	 *            WebDataBinder
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");

		dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {

			@Override
			public void setAsText(String value) {
				try {
					setValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value));
				} catch (ParseException e) {
					setValue(null);
				}
			}
		});

	}

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String index(ModelMap model) {

		// List<Member> members = memberService.findAllMembers();
		// model.addAttribute("members", members);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "index";
	}

	/**
	 * This method will provide the medium to present the list of the MEMEBER
	 * (s) that exist in the database.
	 * 
	 * @param model ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/memberList" }, method = RequestMethod.GET)
	public String listMembers(ModelMap model) {

		List<Member> members = memberService.findAllMembers();
		model.addAttribute("members", members);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "memberList";
	}

	/**
	 * This method will provide the medium to add a new member.
	 * 
	 * @param model ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/addMember" }, method = RequestMethod.GET)
	public String newMember(ModelMap model) {
		Member member = new Member();
		model.addAttribute("member", member);
		model.addAttribute("edit", false);
		// model.addAttribute("actualDate", new Date());
		model.addAttribute("LoggedInUser", getPrincipal());
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * persisting a MEMBER object into the database. It also validates the
	 * member input
	 * 
	 * @param member MEMBER
	 *            object that needs to be persisted in the database
	 * @param result BindingResult
	 * @param model ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/addMember" }, method = RequestMethod.POST)
	public String saveMember(@Valid Member member, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			model.addAttribute("LoggedInUser", getPrincipal());
			return "registration";
		}
		
		member.setJoiningDate(new Date());
		memberService.saveMember(member);

		model.addAttribute("success",
				"Membre " + member.getFirstName() + " " + member.getLastName() + " ajout� avec succ�s");
		model.addAttribute("LoggedInUser", getPrincipal());

		return "registrationSuccess";
	}

	/**
	 * This method will provide the medium to update an existing user.
	 * 
	 * @param id 
	 *           the id of the member that needs to be updated
	 * @param model  ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/modifyMember-{id}" }, method = RequestMethod.GET)
	public String editUser(@PathVariable Integer id, ModelMap model) {
		Member member = memberService.findById(id);

		model.addAttribute("member", member);
		model.addAttribute("actualDate", member.getJoiningDate());
		model.addAttribute("edit", true);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "registration";
	}

	/**
	 * This method will be called on form submission, handling POST request for
	 * updating user in database. It also validates the user input
	 * 
	 * @param member MEMBER
	 *            object that needs to be updated in the database
	 * @param result BindingResult
	 * @param model ModelMap
	 * @param id
	 *            an integer that represents the member id
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/modifyMember-{id}" }, method = RequestMethod.POST)
	public String updateUser(@Valid Member member, BindingResult result, ModelMap model, @PathVariable Integer id) {

		if (result.hasErrors()) {
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("edit", true);
			return "registration";
		}
		member.setId(id);
		System.out.println("=============== " + member.getStatus());
		memberService.updateMember(member);

		model.addAttribute("success",
				"Membre " + member.getFirstName() + " " + member.getLastName() + " modifi� avec succ�s.");
		model.addAttribute("LoggedInUser", getPrincipal());
		return "registrationSuccess";
	}

	/**
	 * This method provides the medium that shows the confirmation of the
	 * removal of a MEMBER from the database.
	 * 
	 * @param model ModelMap
	 * @param id 
	 *           an integer that represents the member id
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/deleteMemberConfirm-{id}" }, method = RequestMethod.GET)
	public String deleteMemberConfirm(@PathVariable Integer id, ModelMap model) {
		Member member = memberService.findById(id);

		model.addAttribute("deleteMessage", "Veuillez confirmer la suppression de " + member.getMail());
		model.addAttribute("member", member);

		return "deleteMember";
	}

	/**
	 * This method will delete an user by it's id value.
	 * 
	 * @param id 
	 *           an integer that represents the member id
	 * 
	 * @return String: representing the new URL of the page
	 */

	@RequestMapping(value = { "/deleteMember-{id}" }, method = RequestMethod.GET)
	public String deleteMember(@PathVariable Integer id) {
		memberService.deleteMemberByID(id);
		return "redirect:/memberList";
	}

	/**
	 * This method will provide UserProfile list to views
	 * 
	 * @return List the list of the members in the database
	 */
	@ModelAttribute("roles")
	public List<MemberProfile> initializeProfiles() {
		return memberProfileService.findAll();
	}

	/**
	 * This method will provide gender list to views
	 * 
	 * 
	 * @return List: the list of the members in the database
	 */
	@ModelAttribute("genders")
	public List<MemberProfile> initializeGenders() {
		return memberProfileService.findAll();
	}

	/**
	 * This method handles Access-Denied redirect.
	 * 
	 * @param model ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
	public String accessDeniedPage(ModelMap model) {
		model.addAttribute("loggedInUser", getPrincipal());
		return "accessDenied";
	}

	/**
	 * This method handles login GET requests. If users is already logged-in and
	 * tries to goto login page again, will be redirected to list page.
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage() {

		if (isCurrentAuthenticationAnonymous()) {

			return "login";
		} else {
			return "redirect:/";
		}
	}

	/**
	 * This method handles logout requests.
	 * 
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @param session HttpSession
	 * 
	 * @return String the URL that needs to be used
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {

			if (session != null) {
				WorkingTimes workingTimes = new WorkingTimes();
				Member member = memberService
						.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
				if (member != null) {
					workingTimes.setMember(member);
					workingTimes.setStartDateTime(new Date(session.getCreationTime()));
					workingTimes.setEndDateTime(new Date(session.getLastAccessedTime()));
					workingTimesService.saveWorkingTimes(workingTimes);
				}

			}

			persistentTokenBasedRememberMeServices.logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
			session.invalidate();
		}
		return "redirect:/login?logout";
	}

	/**
	 * This method is used to get the details of the current user of the program
	 * in order to avoid asking the user for credentials each time he requests
	 * other secured pages.
	 * 
	 * @return String: the member name
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

	/**
	 * This method returns true if users is already authenticated [logged-in],
	 * else false.
	 * 
	 * @return boolean true if authenticated anonymously
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authenticationTrustResolver.isAnonymous(authentication);
	}

}