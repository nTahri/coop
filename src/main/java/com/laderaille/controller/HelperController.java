package com.laderaille.controller;



import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.laderaille.domain.Member;
import com.laderaille.domain.MemberProfile;
import com.laderaille.service.CategoryService;
import com.laderaille.service.ClosingCashService;
import com.laderaille.service.EventService;
import com.laderaille.service.InvoiceLineService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.MemberProfileService;
import com.laderaille.service.MemberService;
import com.laderaille.service.ProductService;

@Controller
@RequestMapping("/clear/all")
public class HelperController {

	@Autowired EventService eventService;
	@Autowired CategoryService categoyService;
	@Autowired ProductService productService;
	@Autowired InvoiceLineService invoiceLineService;
	@Autowired InvoiceService invoiceService;
	@Autowired MemberService memberService;
	@Autowired MemberProfileService memberProfileService;
	@Autowired ClosingCashService closingCashService;
	
	
	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public String getCleanUp(ModelMap model){

		model.addAttribute("LoggedInUser", getPrincipal());
		
		return "CleanUp";
	}
	
	@RequestMapping(value = { "/transactions" }, method = RequestMethod.GET)
	public String deleteInvoices(ModelMap model){

		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("backPage", "/laderaille/invoice/invoiceList");
		
		try{
			invoiceLineService.deleteAllInvoiceLines();
			invoiceService.deleteAllInvoices();
			closingCashService.deleteAllClosingCashs();
			model.addAttribute("message", "Suppresion des factures est reussi");
			return "Succes";
		}catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "Error";
		}
	}
	@RequestMapping(value = { "/members" }, method = RequestMethod.GET)
	public String deleteMembers(ModelMap model){
		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("backPage", "/laderaille/memberList");
		try{
			eventService.deleteAllEvents();
			memberService.deleteAllMembers();
			
			model.addAttribute("message", "Suppresion des membres est reussi");
			return "Succes";
			
		}catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "Error";
		}
	}
	@RequestMapping(value = { "/inventory" }, method = RequestMethod.GET)
	public String deleteProducts(ModelMap model){

		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("backPage", "/laderaille/invoice/inventory");
		
		try{
			productService.deleteAllProducts();
			categoyService.deleteAllCategories();
			model.addAttribute("message", "Suppresion de l'inventaire avec succes");
			return "Succes";
		}catch (Exception e) {
			model.addAttribute("message", e.getMessage());
			return "Error";
		}
	}
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

}
