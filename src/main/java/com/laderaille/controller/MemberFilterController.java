package com.laderaille.controller;

import static org.mockito.Mockito.never;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.laderaille.domain.AgeIntervalEnum;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.Member;
import com.laderaille.domain.MemberFilter;
import com.laderaille.domain.TypeAbonnementEnum;
import com.laderaille.repository.InvoiceDaoImp;
import com.laderaille.service.MemberFilterService;
import com.laderaille.service.MemberService;


@Controller
@RequestMapping("/memberFilter")
public class MemberFilterController {

	@Autowired
	MemberFilterService memberFilterService;

	@Autowired
	MemberService memberService;

	//Default.
	@RequestMapping()
	public String setupForm(@ModelAttribute MemberFilter memberFilter, BindingResult result, Map<String, Object> map) {
		List<MemberFilter> criteriaList = memberFilterService.getAllCriteria();
		List<AgeIntervalEnum> ageIntervalEnumList = new ArrayList<AgeIntervalEnum>(Arrays.asList(AgeIntervalEnum.values()));
		List<TypeAbonnementEnum> typeAbonnementEnumList = new ArrayList<TypeAbonnementEnum>(Arrays.asList(TypeAbonnementEnum.values()));

		map.put("criteriaList", criteriaList);
		map.put("memberFilter", memberFilter);
		map.put("ageIntervalEnumList", ageIntervalEnumList);
		map.put("typeAbonnementEnumList", typeAbonnementEnumList);
		map.put("LoggedInUser", getPrincipal());
		return "memberFilter";		
	}	

	/*
	 * 
	 * 
	 */
	@RequestMapping(params = "search", method = RequestMethod.POST)
	public String filterActions(@ModelAttribute MemberFilter memberFilter, BindingResult result,
			Map<String, Object> map, @RequestParam("startDate") String startDate, 
			@RequestParam("endDate") String endDate) {		

		List<Member> memberList = null;

		System.out.println("SMALL TEST"+memberFilter.getSearchCriteria());

		switch(memberFilter.getSearchCriteria())
		{
		case "Membres qui ont quitt� la Coop.":
			memberList = memberService.findFormerMembers();
			break;
		case "Membres par tranche d'age":
			break;
		case "Membres selon la p�riode d'inscription":
			Date thisStartDate = dateFormatConverterInDate(startDate) ;
			//new Date(this.dateFormatConverterFunction(startDate));	
			@SuppressWarnings("deprecation")
			Date thisEndDate =  dateFormatConverterInDate(endDate);
			//new Date(this.dateFormatConverterFunction(endDate));	
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			memberList = memberService.findAllMemberBetweenJoiningDate(thisStartDate, thisEndDate);
			break;
		case "Membres en fonction de leur implication":
			break;
		case "Membres dont l'abonnement a expir�":
			break;
		case "Membres en fonction du type d'abonnement":
			break;
		case "Tous les membres":
			memberList = memberService.findAllMembers();
			break;
		default: ;
		break;			
		}		
		List<MemberFilter> criteriaList = memberFilterService.getAllCriteria();
		List<AgeIntervalEnum> ageIntervalEnumList = new ArrayList<AgeIntervalEnum>(Arrays.asList(AgeIntervalEnum.values()));
		map.put("ageIntervalEnumList", ageIntervalEnumList);
		map.put("criteriaList", criteriaList);
		map.put("memberFilter", memberFilter);
		map.put("memberList", memberList);
		map.put("LoggedInUser", getPrincipal());
		return "memberFilter";
	}

	@RequestMapping(params = "export", method = RequestMethod.POST)
	public View exportInvoice(@ModelAttribute MemberFilter memberFilter,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {	
		return new AbstractXlsView() {
			Logger log = LoggerFactory.getLogger(InvoiceDaoImp.class);
			@SuppressWarnings("null")
			@Override
			protected void buildExcelDocument(Map<String, Object> model, org.apache.poi.ss.usermodel.Workbook workbook,
					HttpServletRequest request, HttpServletResponse response) throws Exception {
				response.addHeader("Content-Disposition", "attachment; filename=\"MemberList.xls\"");

				// create style for header cells
				CellStyle style = workbook.createCellStyle();
				Font font = workbook.createFont();
				font.setFontName("Arial");
				style.setFillForegroundColor(HSSFColor.BLUE.index);
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
				font.setColor(HSSFColor.WHITE.index);
				style.setFont(font);



				Sheet sheetProduct = workbook.createSheet("memberFilter");
				sheetProduct.setDefaultColumnWidth(30);

				// create header row
				Row headerProduct = sheetProduct.createRow(0);

				headerProduct.createCell(0).setCellValue("ID");
				headerProduct.getCell(0).setCellStyle(style);

				headerProduct.createCell(1).setCellValue("NOM");
				headerProduct.getCell(1).setCellStyle(style);

				headerProduct.createCell(2).setCellValue("PRENOM");
				headerProduct.getCell(2).setCellStyle(style);

				headerProduct.createCell(3).setCellValue("ADRESSE");
				headerProduct.getCell(3).setCellStyle(style);

				headerProduct.createCell(4).setCellValue("COURRIEL");
				headerProduct.getCell(4).setCellStyle(style);

				headerProduct.createCell(5).setCellValue("SEXE");
				headerProduct.getCell(5).setCellStyle(style);

				headerProduct.createCell(6).setCellValue("MOT DE PASSE");
				headerProduct.getCell(6).setCellStyle(style);

				headerProduct.createCell(7).setCellValue("TELEPHONE");
				headerProduct.getCell(7).setCellStyle(style);

				headerProduct.createCell(8).setCellValue("DATE D'ADHeSION");
				headerProduct.getCell(8).setCellStyle(style);

				headerProduct.createCell(9).setCellValue("FACEBOOK");
				headerProduct.getCell(9).setCellStyle(style);

				headerProduct.createCell(10).setCellValue("STATUS");
				headerProduct.getCell(10).setCellStyle(style);

				headerProduct.createCell(11).setCellValue("ID ETUDIANT");
				headerProduct.getCell(11).setCellStyle(style);

				headerProduct.createCell(12).setCellValue("DATE DE DEPART");
				headerProduct.getCell(12).setCellStyle(style);

				headerProduct.createCell(13).setCellValue("DATE DE NAISSANCE");
				headerProduct.getCell(13).setCellStyle(style);
				
				headerProduct.createCell(14).setCellValue("DATE DE D'EXPIRATION");
				headerProduct.getCell(14).setCellStyle(style);

				// create data rows
				int rowCountPrd = 1;
				List<Member> memberList =  memberFilterList(memberFilter, startDate, endDate);
				for (Member member :memberList) {

					Row aRow = sheetProduct.createRow(rowCountPrd++);
					aRow.createCell(0).setCellValue(member.getId());
					if(member.getFirstName()!=null)aRow.createCell(1).setCellValue(member.getFirstName());
					if(member.getLastName()!=null)aRow.createCell(2).setCellValue(member.getLastName());
					if(member.getAddress()!=null)aRow.createCell(3).setCellValue(member.getAddress());
					if(member.getMail()!=null)aRow.createCell(4).setCellValue(member.getMail());		                
					if(member.getSex()!=null)aRow.createCell(5).setCellValue(member.getSex());
					if(member.getPassword()!=null)aRow.createCell(6).setCellValue(member.getPassword());
					if(member.getPhoneNumber()!=null)aRow.createCell(7).setCellValue(member.getPhoneNumber());
					if(member.getJoiningDate()!=null)aRow.createCell(8).setCellValue(member.getJoiningDate());
					if(member.getFacebookName()!=null)aRow.createCell(9).setCellValue(member.getFacebookName());
					aRow.createCell(10).setCellValue(member.getStatus());
					if(member.getStudentID()!=null)aRow.createCell(11).setCellValue(member.getStudentID());
					if(member.getLeftDate()!=null) aRow.createCell(12).setCellValue(member.getLeftDate());
					if(member.getBirthday()!=null) aRow.createCell(13).setCellValue(member.getBirthday());
					if(member.getExpiringDate()!=null) aRow.createCell(14).setCellValue(member.getExpiringDate());

				}
			}
		};
	}
	List<Member> memberFilterList(MemberFilter memberFilter, String startDate, String endDate){
		List<Member> memberList= null;
		switch(memberFilter.getSearchCriteria())
		{
		case "Membres qui ont quitt� la Coop.":
			memberList = memberService.findFormerMembers();
			break;
		case "Membres par tranche d'age":
			break;
		case "Membres selon la p�riode d'inscription":													
			memberList = memberService.findAllMemberBetweenJoiningDate(dateFormatConverterInDate(startDate), dateFormatConverterInDate(endDate));
			break;
		case "Membres en fonction de leur implication":
			break;
		case "Membres dont l'abonnement a expir�":
			break;
		case "Membres en fonction du type d'abonnement":
			break;
		case "Tous les membres":
			System.out.println("/////////////////////////////////////////////////////////////");
			memberList = memberService.findAllMembers();
			break;
		default: ;
		break;			
		}		
		return memberList;	
	}


	/*	
	 * @function convert calendar date
	 * @param String oldDateString 
	 * @return newDateString 
	 */
	public String dateFormatConverterFunction(String oldDateString){		
		String OLD_FORMAT = "yyyy-MM-dd";
		String NEW_FORMAT = "EEE MMM dd yyyy";		
		String newDateString = null;		
		Date d;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}		
		return newDateString;
	}

	public Date dateFormatConverterInDate(String oldDateString){
		Date date = null;
		String FORMAT = "yyyy-MM-dd";
		SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
		try {
			date = formater.parse(oldDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * This method is used to get the details of the current user of the program
	 * in order to avoid asking the user for credentials each time he requests
	 * other secured pages.
	 * 
	 * @return String: the member name
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}
}
