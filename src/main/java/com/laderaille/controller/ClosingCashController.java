package com.laderaille.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.laderaille.domain.ClosingCash;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.Member;
import com.laderaille.repository.InvoiceDaoImp;
import com.laderaille.service.CashClosingRepportService;
import com.laderaille.service.ClosingCashService;
import com.laderaille.service.MemberService;	

@Controller
@RequestMapping("/closingCash")
public class ClosingCashController {
	
	@Autowired
	CashClosingRepportService cashClosingRepportService;
	
	@Autowired
	MemberService memberService;
	
	@Autowired
	ClosingCashService closingCashService;
	
	//Default for open.
	@RequestMapping()
	public String setupForm(Map<String, Object> map, ModelMap model) {
		Date today = Calendar.getInstance().getTime();

		String searchDate = "";
		String memberMail = "";
		double lastTotal = 0;
		
		ClosingCash closingCash = new ClosingCash();
		ClosingCash lastClosingCash = closingCashService.findLastClosingCash(searchTodayDate());

		if(lastClosingCash != null)
		{
			closingCash.setClosingDate(lastClosingCash.getClosingDate());
			closingCash.setMember(lastClosingCash.getMember());		
			lastTotal = lastClosingCash.getTotal();
			memberMail = lastClosingCash.getMember().getMail();
			//to show last closing cash date
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date yesterday = lastClosingCash.getClosingDate(); 			
			searchDate = df.format(yesterday);	
		}
		
		map.put("closingCash", closingCash);
		map.put("yesterday", searchDate);
		map.put("lastMemberclosingCash", memberMail);
		map.put("lastTotal",lastTotal );
		model.addAttribute("LoggedInUser", getPrincipal());
		return "closingCash";		
	}			

	//Default for close.
	@RequestMapping(value = "/cashClosingRepport")
	public String setupForm(ModelMap model, Map<String, Object> map) {	
		
		ClosingCash closingCash = new ClosingCash()	;		
		boolean closingCashExist = false;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();	
		String reportDate = df.format(today);	
		ClosingCash lastClosingCash = closingCashService.findLastClosingCash(searchTodayDate());
		if(lastClosingCash != null)
		{		
			String lastReportDate = df.format(lastClosingCash.getClosingDate());
			if(Objects.equals(lastReportDate, new String(reportDate)))
			{
				closingCash=lastClosingCash;
				closingCashExist = true;
			}
		}
		this.functionCashClosingRepport(reportDate, model);
		map.put("cashClosingRepport", closingCash);
		map.put("closingCashExist", closingCashExist);
		System.out.print("MY TEST : " + searchTodayDate());
		model.addAttribute("LoggedInUser", getPrincipal());
		return "cashClosingRepport" ;
	}	
	
	//Save new Closing.
	@RequestMapping(value = "/cashClosingRepport.do", params = "save", method = RequestMethod.POST)
	public String saveNewCashClosing(ModelMap model, @ModelAttribute("cashClosingRepport") ClosingCash closingCash) {			
			
		Member member = memberService.findByMail(getPrincipal());		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();
		String errorMessage = "";
		boolean dailyClosingCashExist = false;
		String lastReportDate = "";
		ClosingCash lastClosingCash = closingCashService.findLastClosingCash(searchTodayDate());		
		String reportDate = df.format(today);	
		if (lastClosingCash != null) {
			lastReportDate = df.format(lastClosingCash.getClosingDate());
			if (Objects.equals(lastReportDate, new String(reportDate))) {
				dailyClosingCashExist = true ; 							
			}		
		} 
		
		if (member != null) {
			if (!dailyClosingCashExist) {
				closingCash.setClosingDate(today);
				closingCash.setMember(member);
				closingCashService.saveClosingCash(closingCash);
				dailyClosingCashExist = true ; 	
			}				
		}
		if(member == null){
			errorMessage = "Vous n'avez pas les droits requis pour compter la caisse.";
		}
		model.addAttribute("closingCashExist", dailyClosingCashExist);
		model.addAttribute("errorAnonymousUserMessage", errorMessage);
		this.functionCashClosingRepport(reportDate, model);		
		model.addAttribute("LoggedInUser", getPrincipal());
		return "cashClosingRepport";
	}	
		
	//Edit last saving close cash.
	@RequestMapping(params = "update", method = RequestMethod.POST)
	public String actionForm(Map<String, Object> map, @ModelAttribute ClosingCash closingCash, BindingResult result, ModelMap model) {
		double difference = 0;
		String searchDate ="";
		String memberMail="";
		double lastTotal = 0;
		String errorMessage="";
		ClosingCash lastClosingCash = closingCashService.findLastClosingCash(searchTodayDate());
		Member member = memberService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());

		if(lastClosingCash != null)
		{		
			memberMail = lastClosingCash.getMember().getMail();
			lastTotal = lastClosingCash.getTotal();
			difference = closingCash.getTotal() - lastClosingCash.getTotal();			
			lastClosingCash.setFiveCent(closingCash.getFiveCent());
			lastClosingCash.setTenCent(closingCash.getTenCent());
			lastClosingCash.setTwentyFiveCent(closingCash.getTwentyFiveCent());
			lastClosingCash.setOneDollars(closingCash.getOneDollars());
			lastClosingCash.setTwoDollars(closingCash.getTwoDollars());
			lastClosingCash.setFiveDollars(closingCash.getFiveDollars());
			lastClosingCash.setTenDollars(closingCash.getTenDollars());
			lastClosingCash.setTwentyDollars(closingCash.getTwentyDollars());
			lastClosingCash.setFiftyDollars(closingCash.getFiftyDollars());
			lastClosingCash.setHundredDollars(closingCash.getHundredDollars());
			lastClosingCash.setTotal(closingCash.getTotal());
			lastClosingCash.setOpenDifference(difference);
			lastClosingCash.setOpenComments(closingCash.getOpenComments());		
			
			if(member != null){
				lastClosingCash.setOpenCashMember(member);
				closingCashService.editClosingCash(lastClosingCash);
			}			
			
			//to show last closing cash date
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date lastDate = lastClosingCash.getClosingDate(); 			
			searchDate = df.format(lastDate);				
		}		
		if(member == null){
			errorMessage = "Vous n'avez pas les droits requis pour modifier la caisse.";
		}
		
		map.put("closingCash", lastClosingCash);
		map.put("yesterday", searchDate);
		map.put("difference", difference);
		map.put("lastMemberclosingCash", memberMail);
		model.addAttribute("errorAnonymousMessage", errorMessage);
		map.put("lastTotal", lastTotal);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "closingCash";
	}	
	
	//For search openCash
	@RequestMapping(value = "/closingCash.do", params = "search", method = RequestMethod.POST)
	public String findCashClosingRepport(@RequestParam("yesterdayDate") String yesterdayDate, 
			Map<String, Object> map)
	{	
		System.out.println(yesterdayDate);
		ClosingCash closingCash = new ClosingCash();
		Date date =  dateFormatConverterFunction(yesterdayDate);
		closingCash = closingCashService.findClosingCashByDate(date);			
		map.put("closingCash", closingCash);
		map.put("yesterday", yesterdayDate);
		map.put("LoggedInUser", getPrincipal());
		return "closingCash";		
	}
	
	//Default For search cashClosingRepport
	@RequestMapping(value = "/cashStatistics")
	public String statisticsClosingRepport(@ModelAttribute ClosingCash closingCash, BindingResult result, ModelMap model)
	{		
		model.addAttribute("cashStatistics", closingCash);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "cashStatistics";
	}
	
	//For search cashClosingRepport
	@RequestMapping(value = "/cashStatistics.do", params = "search", method = RequestMethod.POST)
	public String actionStatisticsClosingRepport(@ModelAttribute ClosingCash closingCash, BindingResult result, ModelMap model,
			@RequestParam("searchDate") String searchDate)
	{		
		ClosingCash findClosingCash = closingCashService.findClosingCashByDate(dateFormatConverterFunction(searchDate));
		String returnsearchDate = "";
		if(findClosingCash!=null)
		{
			closingCash = findClosingCash;
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Date lastday = findClosingCash.getClosingDate(); 			
			returnsearchDate = df.format(lastday);	
		}
		//this.functionCashClosingRepport(searchDate, model);	
		model.addAttribute("cashStatistics", closingCash);
		model.addAttribute("cashDate", returnsearchDate);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "cashStatistics";
	}
		
	//To search data.
	public ModelMap functionCashClosingRepport(@RequestParam("searchDate") String searchDate, ModelMap model)
	{		
		@SuppressWarnings("deprecation")	
		//Date date =  new Date(this.dateFormatConverterFunction(searchDate));	
		Date date =  this.dateFormatConverterFunction(searchDate);	
		Date today = Calendar.getInstance().getTime();

		//TODO Replace this by product ID of MemberShip.
		String membershipProductID = "2-02";
		
		//Get daily number of new membership and total amount of that. 
		Object numMembership = cashClosingRepportService.findNumberOfMemberShipInDay(date, membershipProductID).getLeft();
		Object amountMembership = cashClosingRepportService.findNumberOfMemberShipInDay(date, membershipProductID).getRight();
		
		//Get daily number of new registration and total amount of that. 
		String registrationProductID = "2-02";
		Object numRegistration = cashClosingRepportService.findNumberOfMemberShipInDay(date, registrationProductID).getLeft();
		Object amountRegistration = cashClosingRepportService.findNumberOfMemberShipInDay(date, registrationProductID).getRight();

		//Get unclosed invoices number and amount.
		Object uncloseInvoiceNumber = cashClosingRepportService.findAllUnclosedInvoiceInDay(date).getLeft();
		Object uncloseInvoiceAmount = cashClosingRepportService.findAllUnclosedInvoiceInDay(date).getRight();
		
		//Get the total closed invoices amount.
		//Object uncloseInvoiceNumber = cashClosingRepportService.findAllUnclosedInvoiceInDay(date).getLeft();
		//Object uncloseInvoiceAmount = cashClosingRepportService.findAllUnclosedInvoiceInDay(date).getRight();
		
		//Get total units of product sells and her amount.
		Object sellsProductsNumber = cashClosingRepportService.findAllInvoiceInDay(date).getLeft();
		Object totalInvoiceAmount = cashClosingRepportService.findAllInvoiceInDay(date).getRight();
		
		//Get total amount for closed invoices.
		Object closedInvoiceNumber = cashClosingRepportService.findInvoicesClosedThisDay(date).getLeft();
		Object closeInvoiceAmount = cashClosingRepportService.findInvoicesClosedThisDay(date).getRight();
		
		//Get amount before open (or last cash)
		double lastClosingCashAmount = 0 ; 
		ClosingCash lastClosingCash = closingCashService.findLastClosingCash(searchTodayDate());
		if(lastClosingCash!=null){
			lastClosingCashAmount = lastClosingCash.getTotal();	
		}
		
		model.addAttribute("numMembership", numMembership);
		model.addAttribute("amountMembership", amountMembership);
		model.addAttribute("uncloseInvoiceNumber", uncloseInvoiceNumber);
		model.addAttribute("uncloseInvoiceAmount", uncloseInvoiceAmount);
		model.addAttribute("numRegistration", numRegistration);
		model.addAttribute("amountRegistration", amountRegistration);
		model.addAttribute("sellsProductsNumber", sellsProductsNumber);
		model.addAttribute("totalInvoiceAmount", totalInvoiceAmount);
		model.addAttribute("closedInvoiceNumber", closedInvoiceNumber);
		model.addAttribute("closeInvoiceAmount", closeInvoiceAmount);
		model.addAttribute("currentDate", searchDate);
		model.addAttribute("lastClosingCashAmount", lastClosingCashAmount);
		model.addAttribute("LoggedInUser", getPrincipal());
		return model;
	}
	
	public Date dateFormatConverterFunction(String oldDateString){
		Date date = null;
		String FORMAT = "yyyy-MM-dd";
		SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
		try {
			date = formater.parse(oldDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public Date searchTodayDate(){
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = Calendar.getInstance().getTime();
		String stringDate = df.format(today);	
		
		return this.dateFormatConverterFunction(stringDate);
	}
	
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}
}




