package com.laderaille.controller;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.laderaille.domain.Event;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.Member;
import com.laderaille.service.EventNameService;
import com.laderaille.service.EventService;
import com.laderaille.service.InvoiceService;
import com.laderaille.service.MemberProfileService;
import com.laderaille.service.MemberService;

/**
 * This class contains the methods that map the EVENT model with all the view(s)
 * and URL(s) that need an event or its details.
 * 
 * <p>
 * This is one of the most important in the program because it allows the user
 * to manipulate the EVENT object which the most import element in the program.
 *
 * @author Langis.G
 * @author Lemogo.J.R
 * @author Tahri.N
 * @author Soumri.N
 * @author Yezli.S
 * @version 1.0
 * 
 * 
 */

@Controller
@RequestMapping("/event")
@SessionAttributes("roles")
public class EventController {

	@Autowired
	EventService eventService;
	@Autowired
	MessageSource messageSource;
	@Autowired
	MemberService memberService;
	@Autowired
	InvoiceService invoiceService;
	@Autowired
	MemberProfileService memberProfileService;
	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	@Autowired
	EventNameService eventNameService;

	/**
	 * This method initialize the WebDataBinder which will be used for
	 * populating command and form object arguments of annotated handler
	 * methods.
	 * 
	 * <p>
	 * This method support all arguments that RequestMapping supports, except
	 * for command/form objects and corresponding validation result objects
	 * 
	 * @param dataBinder
	 *            Special DataBinder for data binding from web request
	 *            parameters to JavaBean objects. Designed for web environments,
	 *            but not dependent on the Servlet API;
	 * 
	 */
	@InitBinder
	public void initBinder(WebDataBinder dataBinder) {
		dataBinder.setDisallowedFields("id");
		dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {

			@Override
			public void setAsText(String value) {
				try {
					setValue(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value));
				} catch (ParseException e) {
					setValue(null);
				}
			}
		});

	}

	/**
	 * This method extracts all the existing events in the database and sends
	 * them to the view to present them to the user
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String representing the new URL of the page
	 */
	@RequestMapping(value = { "/eventList" }, method = RequestMethod.GET)
	public String getEventsList(ModelMap model) {

		List<Event> events = eventService.findAllEvents();
		model.addAttribute("events", events);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "eventList";
	}

	/**
	 * This method allows the user to get all the needed information for a new
	 * EVENT creation. It creates a new EVENT object to be used by the view and
	 * it return the string to be added to the URL.
	 * 
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/addEvent" }, method = RequestMethod.GET)
	public String newEvent(ModelMap model) {
		Event event = new Event();
		model.addAttribute("event", event);

		List<Member> mList = memberService.findAllMembers();
		Comparator<Member> m = (m1, m2) -> m1.getMail().compareTo(m2.getMail());
		mList.sort(m);
		model.addAttribute("eventName", eventNameService.findAllEventsName());
		model.addAttribute("members", mList);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "addEvent";
	}

	/**
	 * This method is used to communicate the provided details by the user to
	 * the EVENT service to save them into the database. If the EVENT is
	 * successfully stored it will return a success message to the user,
	 * otherwise it will return an error message.
	 * 
	 * <p>
	 * This method will throw an exception if any thing goes wrong when
	 * persisting the data.
	 * 
	 * @param event
	 *            The EVENT object that needs to be persisted
	 * @param result
	 *            BindingResult
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/addEvent" }, method = RequestMethod.POST)
	public String saveEvent(@Valid Event event, BindingResult result, ModelMap model) {

		if (result.hasErrors()) {
			model.addAttribute("LoggedInUser", getPrincipal());
			List<Member> mList = memberService.findAllMembers();
			Comparator<Member> m = (m1, m2) -> m1.getMail().compareTo(m2.getMail());
			mList.sort(m);
			model.addAttribute("eventName", eventNameService.findAllEventsName());
			model.addAttribute("members", mList);
			model.addAttribute("message", result.getAllErrors());
			model.addAttribute("backPage", "/laderaille/event/addEvent");
			return "Error";
		}
		try {
			eventService.saveEvent(event);
			model.addAttribute("success",
					"Event " + event.getName() + " on " + event.getStartDate() + " added with success");
			model.addAttribute("LoggedInUser", getPrincipal());
			List<Member> mList = memberService.findAllMembers();
			Comparator<Member> m = (m1, m2) -> m1.getMail().compareTo(m2.getMail());
			mList.sort(m);
			model.addAttribute("members", mList);
			return "redirect:/event/eventList";
		} catch (Exception e) {
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("message", e.getMessage());
			model.addAttribute("backPage", "/laderaille/event/addEvent");
			return "Error";
		}

	}

	/**
	 * This method will provide the medium to update an existing EVENT. It will
	 * copy the details of the EVENT that needs to be updated in a new EVENT
	 * object to be used by the EVENT service. it also provides the list of the
	 * MEMBER (s) and the INVOICE (s) that may be added to the event.
	 * 
	 * 
	 * @param id:
	 *            integer that represents the id of the event that needs to be
	 *            updated.
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 * 
	 */
	@RequestMapping(value = { "/modifyEvent-{id}" }, method = RequestMethod.GET)
	public String editEvent(@PathVariable Integer id, ModelMap model) {
		Event event = eventService.findById(id);
		model.addAttribute("event", event);
		model.addAttribute("edit", true);

		List<Invoice> invoices = invoiceService.findInvoicesByEventId(event.getId());
		model.addAttribute("invoices", invoices);

		List<Member> mList = memberService.findAllMembers();
		Comparator<Member> m = (m1, m2) -> m1.getMail().compareTo(m2.getMail());
		mList.sort(m);
		model.addAttribute("eventName", eventNameService.findAllEventsName());
		model.addAttribute("members", mList);

		model.addAttribute("LoggedInUser", getPrincipal());
		model.addAttribute("events", eventService.findAllEvents());

		if (event.canBeDeleted()) {
			return "addEvent";
		} else {
			return "redirect:/event/eventList";
		}

	}

	/**
	 * This method will provide the medium to persist the updated EVENT. It will
	 * throw an exception if any problem happens when saving the data into the
	 * database.
	 * 
	 * @param id
	 *            that represents the id of the event that needs to be updated
	 * @param event
	 *            the EVENT object to be updated
	 * @param result
	 *            BindingResult
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/modifyEvent-{id}" }, method = RequestMethod.POST)
	public String editEvent(@Valid Event event, BindingResult result, ModelMap model, @PathVariable Integer id) {

		if (result.hasErrors()) {
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("message", result.getAllErrors());
			model.addAttribute("backPage", "/laderaille/event/addEvent");
			return "Error";
		}
		try {

			Event existing_event = eventService.findById(id);

			if (existing_event.canBeDeleted()) {
				event.setId(id);
				eventService.updateEvent(event);
			}
			return "redirect:/event/eventList";
		} catch (Exception e) {
			model.addAttribute("LoggedInUser", getPrincipal());
			model.addAttribute("message", e.getMessage());
			model.addAttribute("backPage", "/laderaille/event/addEvent");
			return "Error";
		}
	}

	/**
	 * This method is used as the medium to get the data of the EVENT to be
	 * deleted from the user.
	 * 	 * 
	 * @param id
	 *            INT that represents the id of the event that needs to be
	 *            updated
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/deleteEvent-{id}" }, method = RequestMethod.GET)
	public String deleteEvent(@PathVariable Integer id, ModelMap model)  {
		Event event = eventService.findById(id);

		List<Member> mList = memberService.findAllMembers();
		Comparator<Member> m = (m1, m2) -> m1.getMail().compareTo(m2.getMail());
		mList.sort(m);

		Date dateNow = new Date();
		Date eventDate = event.getEndDate();
		long diff = eventDate.getTime() - dateNow.getTime();
		long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

		if (days > 0) {
			eventService.deleteEvent(event);
		}
		return "redirect:/event/eventList";
	}

	/**
	 * This method is used to remove the selected EVENT from the database. It
	 * will search the EVENT by its id and provides it to the service to proceed
	 * with the removal.
	 * 
	 * @param id
	 *            INT that represents the id of the event that needs to be
	 *            updated
	 * @param model
	 *            ModelMap
	 * 
	 * @return String: representing the new URL of the page
	 */
	@RequestMapping(value = { "/deleteEventConfirm-{id}" }, method = RequestMethod.GET)
	public String deleteEventConfirm(@PathVariable Integer id, ModelMap model) {
		Event event = eventService.findById(id);

		model.addAttribute("deleteMessage", "Veuillez confirmer la suppression de " + event.getName());
		model.addAttribute("event", event);

		return "deleteEvent";
	}

	/**
	 * This method is used to get the details of the current user of the program
	 * in order to avoid asking the user for credentials each time he requests
	 * other secured pages.
	 * 
	 * @return String: the member name
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}

}
