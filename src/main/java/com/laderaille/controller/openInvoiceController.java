package com.laderaille.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.laderaille.domain.AgeIntervalEnum;
import com.laderaille.domain.Category;
import com.laderaille.domain.Invoice;
import com.laderaille.domain.MemberFilter;
import com.laderaille.domain.Product;
import com.laderaille.domain.TypeAbonnementEnum;
import com.laderaille.repository.CategoryDaoImp;
import com.laderaille.repository.InvoiceDaoImp;
import com.laderaille.service.CategoryService;
import com.laderaille.service.InvoiceService;


@Controller
@RequestMapping("/openInvoiceList")
//@SessionAttributes({ "roles" })
public class openInvoiceController {

	@Autowired
	InvoiceService invoiceService;
	@Autowired
	CategoryService categoryService;

	@RequestMapping()
   	public String setupForm(@ModelAttribute Invoice invoice, BindingResult result, ModelMap model) {
	
		List<Category> categories = categoryService.findAllCategories();
		List<Invoice> invoices = invoiceService.findAllOpenInvoices();

		model.addAttribute("invoices", invoices);

		model.addAttribute("categories", categories);
		model.addAttribute("LoggedInUser", getPrincipal());
		return "openInvoiceList";		
	}	

	/*
	 * 
	 * 
	 */
	@RequestMapping(params = "searchm", method = RequestMethod.POST)
	public String findOpenInvoice(@ModelAttribute Invoice invoice, BindingResult result, Map<String, Object> map,
			@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {		

		System.out.println("hello world");
		List<Invoice> invoices=null;
		//@SuppressWarnings("deprecation")
		//Date thisStartDate =  new Date(startDate);	
		//@SuppressWarnings("deprecation")
		//Date thisEndDate =  new Date(endDate);
		invoices=invoiceService.findOpenInvoiceBetweenDate(dateFormatConverterFunction(startDate),dateFormatConverterFunction(endDate));
		if(startDate.isEmpty() && endDate.isEmpty()) invoices=invoiceService.findAllOpenInvoices();
		map.put("invoices", invoices);
		map.put("startDate",startDate);
		map.put("endDate",endDate);
		return "openInvoiceList";
	}
	@RequestMapping(params = "exporter", method = RequestMethod.POST)
	public View exportInvoice(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate) {	
		    return new AbstractXlsView() {
		    	Logger log = LoggerFactory.getLogger(InvoiceDaoImp.class);
		        @Override
		        protected void buildExcelDocument(Map<String, Object> model, org.apache.poi.ss.usermodel.Workbook workbook,
		    			HttpServletRequest request, HttpServletResponse response) throws Exception {
		        	response.addHeader("Content-Disposition", "attachment; filename=\"OpenInvoices.xls\"");
		            
		        	// create style for header cells
		            CellStyle style = workbook.createCellStyle();
		            Font font = workbook.createFont();
		            font.setFontName("Arial");
		            style.setFillForegroundColor(HSSFColor.BLUE.index);
		            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		            font.setColor(HSSFColor.WHITE.index);
		            style.setFont(font);
		             
		        
		            
		            Sheet sheetProduct = workbook.createSheet("OpenInvoices");
		            sheetProduct.setDefaultColumnWidth(30);
		            
		            // create header row
		            Row headerProduct = sheetProduct.createRow(0);
		            
		            headerProduct.createCell(0).setCellValue("ID");
		            headerProduct.getCell(0).setCellStyle(style);
		            
		            headerProduct.createCell(1).setCellValue("CLIENT");
		            headerProduct.getCell(1).setCellStyle(style);
		             
		            headerProduct.createCell(2).setCellValue("RESPONSABLE");
		            headerProduct.getCell(2).setCellStyle(style);
		            
		            headerProduct.createCell(3).setCellValue("DATE");
		            headerProduct.getCell(3).setCellStyle(style);
		             
		            headerProduct.createCell(4).setCellValue("STATUS");
		            headerProduct.getCell(4).setCellStyle(style);
		            
		            headerProduct.createCell(5).setCellValue("VALEUR");
		            headerProduct.getCell(5).setCellStyle(style);
		             
		            // create data rows
		            int rowCountPrd = 1;
		            List<Invoice> invoice= null;
		            if(!(startDate.isEmpty()&& endDate.isEmpty()))
		            	invoice=invoiceService.findOpenInvoiceBetweenDate(dateFormatConverterFunction(startDate),dateFormatConverterFunction(endDate));
		            else
		            	invoice = invoiceService.findAllOpenInvoices();
		           
		            for (Invoice inv :invoice ) {
		            	
		                Row aRow = sheetProduct.createRow(rowCountPrd++);
		                aRow.createCell(4).setCellValue(inv.getId());
		                aRow.createCell(0).setCellValue(inv.getBuyerName());
		                aRow.createCell(1).setCellValue(inv.getSellerName());
		                aRow.createCell(2).setCellValue(inv.getTransactionDate());
		                aRow.createCell(3).setCellValue(inv.getInvoiceStatus());		                
		                aRow.createCell(5).setCellValue(inv.getTotal());

		            }
		        }
		    };
		}	

	/*	
	 * @function convert calendar date
	 * @param String oldDateString 
	 * @return newDateString 
	 */
	public Date dateFormatConverterFunction(String oldDateString){
		Date date = null;
		String FORMAT = "yyyy-MM-dd";
		SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
		try {
			date = formater.parse(oldDateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
	private String getPrincipal() {
		String memberName = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			memberName = ((UserDetails) principal).getUsername();
		} else {
			memberName = principal.toString();
		}
		return memberName;
	}
}
